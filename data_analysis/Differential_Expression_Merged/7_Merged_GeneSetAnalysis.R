# script created by KSB, 08.08.18

library(edgeR)
library(plyr)
library(NineteenEightyR)
library(RColorBrewer)
library(biomaRt)
library(ggplot2)
library(ggsignif)
library(EGSEA)
library(goseq)
library(ReactomePA)
library(preprocessCore)
library(magrittr)
library(dplyr)

# Set paths:
inputdir = "/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/Output/Malaria/"

# Set output directory and create it if it does not exist:
outputdir <- "/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/Output/Malaria/MergedData/GOandCamera/"

if (file.exists(outputdir) == FALSE){
    dir.create(outputdir)
}

# Load colour schemes:
wes=c("#3B9AB2", "#EBCC2A", "#F21A00", "#00A08A", "#ABDDDE", "#000000", "#FD6467","#5B1A18")
palette(c(wes, brewer.pal(8,"Dark2")))
# set up colour palette for batch
batch.col=electronic_night(n=3)
dev.off()

# load files -------------------------------------------------------------------------------------------------

# load the normalised, merged data
load(paste0(inputdir, "MergedData/merged.read_counts.TMM.filtered.Rda"))
lcpm=cpm(merged, log=T)

# load in the efit object
load(paste0(inputdir, "MergedData/DE_TMM/voomDupEfit_TMM.Rda"))
# load voom object
load(paste0(inputdir, "MergedData/DE_TMM/vDup_TMM.Rda"))

# set up design matrix ----------------------------------------------------------------------------------------

# Set up design matrix
design <- model.matrix(~0 + merged$samples$species + merged$samples$Island + merged$samples$sex + merged$samples$EEF_falciparum + merged$samples$Merozoite_falciparum + merged$samples$oocyst_falciparum + merged$samples$Ring_falciparum + merged$samples$ookoo_falciparum + merged$samples$Trophozoite_falciparum + merged$samples$bbSpz_vivax + merged$samples$Female_vivax + merged$samples$Male_vivax + merged$samples$Merozoite_vivax + merged$samples$oocyst_vivax + merged$samples$ook_vivax  + merged$samples$Ring_vivax + merged$samples$ookoo_vivax + merged$samples$Schizont_vivax + merged$samples$sgSpz_vivax + merged$samples$Trophozoite_vivax + merged$samples$Gran + merged$samples$Bcell + merged$samples$CD4T + merged$samples$CD8T + merged$samples$NK + merged$samples$Mono)

# rename columns to exclude spaces and unrecognised characters
colnames(design)=gsub("\\$", "", colnames(design)) %>% gsub("mergedsamplesspecies", "", .) %>% gsub("mergedsamplesbatch", "", .) %>% gsub("-", "", .) %>% gsub("mergedsamples", "", .) %>% gsub("study", "", .) %>% gsub("West Papua", "Mappi", .) %>% gsub("Island", "", .) %>% gsub("West Papua", "Mappi", .) 
# set up contrast matrix
contr.matrix <- makeContrasts(SickvsHealthy=(vivax + falciparum)/2 - control, FalcvsHealthy=falciparum - control, VivaxvsHealthy=vivax - control, VivaxvsFalciparum=vivax - falciparum, levels=colnames(design))

# Enrichment analysis for Gene Ontology ----------------------------------------------------------------------------------------------------

ensembl.mart.90 <- useMart(biomart='ENSEMBL_MART_ENSEMBL', dataset='hsapiens_gene_ensembl', host = 'www.ensembl.org', ensemblRedirect = FALSE)

# transform ensembl IDs to entrez IDs to be compatible with human c2 dataset (below)
ensembl_genes=rownames(merged)
entrez=getBM(filters= "ensembl_gene_id", attributes= c("ensembl_gene_id", "entrezgene_id", "description"),values= ensembl_genes,mart=ensembl.mart.90)
merged$entrezID=entrez[match(rownames(merged), entrez[,1]), 2]

# gene set testing with Camera
load(url("http://bioinf.wehi.edu.au/software/MSigDB/human_c2_v5p2.rdata")) 
idx <- ids2indices(Hs.c2,id=merged$entrezID) 
for (i in 1:ncol(contr.matrix)){
    camera.matrix=camera(vDup,idx,design,contrast=contr.matrix[,i])
    write.table(camera.matrix, file=paste0(outputdir,"cameraMatrix_",colnames(contr.matrix)[i],".txt"))
}

# gene set testing with goSeq
# for this analysis, we need to use all genes (not just filtered ones). In order to do this, load the unfiltered DGE list:
load(paste0(inputdir, "Indo-Analysis/dataPreprocessing/unfiltered_DGElistObject.Rda"))
dim(y)
# 27413   123

for(pop in 1:ncol(voomDupEfit)){
    topTable <- topTable(voomDupEfit, coef=pop, n=Inf, p.value=0.05, lfc=1)
    gene.vector=as.integer(rownames(y) %in% rownames(topTable))
    names(gene.vector)=rownames(y)

    # set the probability weighting fcuntion, i.e., implement a weight for each gene dependent on its length
    pwf=nullp(gene.vector,"hg19","ensGene")
    # use  the  default  method  to  calculate  the  over  and  under  expressed  GO categories among DE genes
    GO.wall=goseq(pwf,"hg19","ensGene",use_genes_without_cat=TRUE)

    # now let's interpret the results. First we need to apply a multiple hypothesis testing correction set at 5% (BH method)
    enriched.GO=GO.wall[p.adjust(GO.wall$over_represented_pvalue, method="BH")<.05,]
    write.table(enriched.GO, file=paste0(outputdir,"enrichedGOterms_",colnames(voomDupEfit)[pop],".txt"), quote=F, row.names=F, sep="\t")
    # plot - taken from https://bioinformatics-core-shared-training.github.io/cruk-summer-school-2018/RNASeq2018/html/06_Gene_set_testing.nb.html
    pdf(paste0(outputdir,"enrichedGenes_lfc1pval05_",pop,".pdf"))
    goResults = goseq(pwf,"hg19","ensGene", use_genes_without_cat=TRUE, test.cats=c("GO:BP"))
    goResults = as.data.frame(goResults)
    print(goResults %>% top_n(40, wt=-over_represented_pvalue) %>% mutate(hitsPerc=numDEInCat*100/numInCat) %>% 
    ggplot(aes(x=hitsPerc, y=term, colour=over_represented_pvalue, size=numDEInCat)) +
    geom_point() + expand_limits(x=0) + labs(x="Hits (%)", y="GO term", colour="p value", size="Count") + ggtitle(colnames(voomDupEfit)[pop]))
    dev.off()
    if(nrow(enriched.GO)>0){
        zz=file(paste0(outputdir,"topTen_enrichedGOterms_lfc1pval05",pop,".txt"), open="wt")
        sink(zz)
        sink(zz, type = "message")
        # get GO terms for top ten enriched GO terms - write output with the sink() function
        for(go in 1:length(enriched.GO$category)){
            print(GOTERM[[enriched.GO$category[go]]])
        }
    }
    sink(type = "message")
    sink()

    # KEGG pathway analysis
    en2eg=as.list(org.Hs.egENSEMBL2EG)
    # Get the mapping from Entrez 2 KEGG
    eg2kegg=as.list(org.Hs.egPATH)
    # Define a function which gets all unique KEGG IDs
    # associated with a set of Entrez IDs
    grepKEGG=function(id,mapkeys){unique(unlist(mapkeys[id],use.names=FALSE))}
    # Apply this function to every entry in the mapping from
    # ENSEMBL 2 Entrez to combine the two maps
    kegg=lapply(en2eg,grepKEGG,eg2kegg)
    KEGG=goseq(pwf,gene2cat=kegg, use_genes_without_cat=TRUE)
    enriched.GO.kegg=KEGG[p.adjust(KEGG$over_represented_pvalue, method="BH")<.05,]
    write.table(enriched.GO.kegg, file=paste0(outputdir,"enrichedGOkegg_",colnames(voomDupEfit)[pop],".txt"), quote=F, row.names=F, sep="\t")
}
