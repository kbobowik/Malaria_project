# script created by KSB, 08.08.18

# Load dependencies and set input paths --------------------------

library(edgeR)
library(plyr)
library(NineteenEightyR)
library(RColorBrewer)
library(biomaRt)
library(ggpubr)
library(ggplot2)
library(ggsignif)
library(pheatmap)
library(viridis)
library(gplots)
library(circlize)
library(ComplexHeatmap)
library(EnsDb.Hsapiens.v86)
library(ggsci)
library(scales)
library(dendextend)
library(reshape2)
library(variancePartition)
library(doParallel)
library(preprocessCore)

# Set paths:
inputdir = "/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/Output/Malaria/MergedData/"

# Set output directory and create it if it does not exist:
outputdir <- "/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/Output/Malaria/MergedData/VariancePartition/"

if (file.exists(outputdir) == FALSE){
    dir.create(outputdir)
}

# Load colour schemes:
wes=c("#3B9AB2", "#EBCC2A", "#F21A00", "#00A08A", "#ABDDDE", "#000000", "#FD6467","#5B1A18")
palette(c(wes, brewer.pal(8,"Dark2")))
# set up colour palette for batch
batch.col=electronic_night(n=3)
village.col=c("#EBCC2A","chocolate","chocolate","#3B9AB2","#F21A00","chocolate","chocolate","chocolate","#78B7C5","orange","chocolate")

# set up colour palette. The 'NPG' palette (Nature Publising Group) from ggsci will be used for the standard palette
standard_col=c(pal_npg()(10), pal_futurama()(12), pal_tron()(3))
plasmo=brewer.pal(3, "Set2")

dev.off()

# Quantile normalisation ---------------------------------------------

# load in TMM normalised data file
load(paste0(inputdir, "merged.read_counts.TMM.filtered.Rda"))

# calculate log cpm of the TMM normalised data
lcpm=cpm(merged, log=T)

# VariancePartition attributes the fraction of total variation attributable to each aspect of the study design.
# Let's explore how much variation is being contributed by study

# set up parallel processing
cl <- makeCluster(4)
registerDoParallel(cl)

# Set up design matrix
design <- model.matrix(~0 + merged$samples$diseaseStatus + merged$samples$Island + merged$samples$sex + merged$samples$EEF_falciparum + merged$samples$Merozoite_falciparum + merged$samples$oocyst_falciparum + merged$samples$Ring_falciparum + merged$samples$ookoo_falciparum + merged$samples$Trophozoite_falciparum + merged$samples$bbSpz_vivax + merged$samples$Female_vivax + merged$samples$Male_vivax + merged$samples$Merozoite_vivax + merged$samples$oocyst_vivax + merged$samples$ook_vivax  + merged$samples$Ring_vivax + merged$samples$ookoo_vivax + merged$samples$Schizont_vivax + merged$samples$sgSpz_vivax + merged$samples$Trophozoite_vivax + merged$samples$Gran + merged$samples$Bcell + merged$samples$CD4T + merged$samples$CD8T + merged$samples$NK + merged$samples$Mono)

# Estimate precision weights for each gene and sample
v <- voom(merged, design, normalize="quantile")

# Define formula
# Note the syntax used to specify random effects
form <- ~ (1|diseaseStatus) + (1|Island) + (1|sex) EEF_falciparum + Merozoite_falciparum + oocyst_falciparum + Ring_falciparum + ookoo_falciparum + Trophozoite_falciparum + bbSpz_vivax + Female_vivax + Male_vivax + Merozoite_vivax + oocyst_vivax + ook_vivax + Ring_vivax + ookoo_vivax + Schizont_vivax + sgSpz_vivax + Trophozoite_vivax + Gran + Bcell + CD4T + CD8T + NK + Mono

# Fit model and extract results
# 1) fit linear mixed model on gene expression. If categorical variables are specified,
# a linear mixed model is used. If all variables are modeled as fixed effects,
# a linear model is used. Each entry results in a regression model fit on a single gene.
# 2) extract variance fractions from each model fit for each gene, the fraction of variation attributable
# to each variable is returned
# Interpretation: the variance explained by each variables after correcting for all other variables
varPart <- fitExtractVarPartModel(v, form, merged$samples)

# sort variables (i.e. columns) by median fraction of variance explained
vp = sortCols( varPart )

# sort genes based on variance explained by Individual
pdf(paste0(outputdir,"tenMostVariableGenes_VarPart_Island_Quantile.pdf"))
plotPercentBars(varPart[order(varPart$Island, decreasing=TRUE)[1:10],])
dev.off()

pdf(paste0(outputdir,"VarianceExplained_Quantile.pdf"))
plotVarPart( vp )
dev.off()

# let's also look at the most variable genes by disease status
# sort genes based on variance explained by Individual
pdf(paste0(outputdir,"tenMostVariableGenes_VarPart_diseaseStatus_Quantile.pdf"))
plotPercentBars(varPart[order(varPart$diseaseStatus, decreasing=TRUE)[1:10],])
dev.off()

# TMM normalisation ---------------------------------------------

v <- voom(merged, design)
# Define formula
# Note the syntax used to specify random effects
form <- ~ (1|diseaseStatus) + (1|Island) + (1|sex) + EEF_falciparum + Merozoite_falciparum + oocyst_falciparum + Ring_falciparum + ookoo_falciparum + Trophozoite_falciparum + bbSpz_vivax + Female_vivax + Male_vivax + Merozoite_vivax + oocyst_vivax + ook_vivax + Ring_vivax + ookoo_vivax + Schizont_vivax + sgSpz_vivax + Trophozoite_vivax + Gran + Bcell + CD4T + CD8T + NK + Mono

# Fit model and extract results
# 1) fit linear mixed model on gene expression. If categorical variables are specified,
# a linear mixed model is used. If all variables are modeled as fixed effects,
# a linear model is used. Each entry results in a regression model fit on a single gene.
# 2) extract variance fractions from each model fit for each gene, the fraction of variation attributable
# to each variable is returned
# Interpretation: the variance explained by each variables after correcting for all other variables
varPart <- fitExtractVarPartModel(v, form, merged$samples)

# sort variables (i.e. columns) by median fraction of variance explained
vp = sortCols( varPart )

# sort genes based on variance explained by Individual
pdf(paste0(outputdir,"tenMostVariableGenes_VarPart_Island_TMM_NoNorm.pdf"))
plotPercentBars(varPart[order(varPart$Island, decreasing=TRUE)[1:10],])
dev.off()

pdf(paste0(outputdir,"VarianceExplained_TMM_noNorm.pdf"), width = 15, height=10)
plotVarPart(vp, label.angle = 40)
dev.off()

# let's also look at the most variable genes by disease status
# sort genes based on variance explained by Individual
pdf(paste0(outputdir,"tenMostVariableGenes_VarPart_diseaseStatus_NoNorm.pdf"))
plotPercentBars(varPart[order(varPart$diseaseStatus, decreasing=TRUE)[1:10],])
dev.off()

# Simpler comparison of just study and disease status ----------------------------------------

# Set up design matrix
design <- model.matrix(~0 + merged$samples$diseaseStatus + merged$samples$study)
# Estimate precision weights for each gene and sample
v <- voom(merged, design, normalize="quantile")

# First TMM
form <- ~ (1|diseaseStatus) + (1|study)
# Fit model and extract results
varPart <- fitExtractVarPartModel(v, form, merged$samples)
# sort variables (i.e. columns) by median fraction of variance explained
vp = sortCols( varPart )

# get mean and median values
median(vp$study)
# 0.590213
median(vp$diseaseStatus)
# 0.07511686
mean(vp$study)
# 0.5283475
mean(vp$diseaseStatus)
# 0.1255145

pdf(paste0(outputdir,"VarianceExplained_Quantile_simpleModel.pdf"))
plotVarPart(vp, col = c(ggColorHue(4)))
dev.off()

# Estimate precision weights for each gene and sample
v2 <- voom(merged, design)
# Fit model and extract results
varPart2 <- fitExtractVarPartModel(v2, form, merged$samples)
# sort variables (i.e. columns) by median fraction of variance explained
vp2 = sortCols( varPart2 )

# get mean and median values
median(vp2$study)
# 0.5660225
median(vp2$diseaseStatus)
# 0.0737076
mean(vp2$study)
# 0.5113353
mean(vp2$diseaseStatus)
# 0.1254164

pdf(paste0(outputdir,"VarianceExplained_TMM_simpleModel.pdf"))
plotVarPart(vp2, col = c(ggColorHue(4)))
dev.off()

# plot side by side
ggarrange(plotVarPart(vp, col = c(ggColorHue(4)), main="Quantile"), plotVarPart(vp2, col = c(ggColorHue(4))), main="TMM")