# script created by KSB, 08.08.18
# Perform DE analysing relationship between islands

### Last edit: KB 03.04.2019

# Load dependencies and set input paths --------------------------

# Load dependencies:
library(edgeR)
library(plyr)
library(NineteenEightyR)
library(RColorBrewer)
library(biomaRt)
library(ggpubr)
library(ggplot2)
library(ggsignif)
library(pheatmap)
library(viridis)
library(gplots)
library(circlize)
library(ComplexHeatmap)
library(EnsDb.Hsapiens.v86)
library(ggsci)
library(scales)
library(dendextend)
library(reshape2)
library(variancePartition)
library(doParallel)
library(devtools)
library(Biobase)
library(preprocessCore)
library(magrittr)
library(EnhancedVolcano)
library(seqsetvis)

# Set paths:
inputdir = "/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/Output/Malaria/MergedData/"
housekeepingdir="/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/BatchEffects/"

# Set output directory and create it if it does not exist:
outputdir <- "/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/Output/Malaria/MergedData/TMM/separate/"

if (file.exists(outputdir) == FALSE){
    dir.create(outputdir)
}

# Load colour schemes:
wes=c("#3B9AB2", "#EBCC2A", "#F21A00", "#00A08A", "#ABDDDE", "#000000", "#FD6467","#5B1A18")
palette(c(wes, brewer.pal(8,"Dark2")))
# set up colour palette for batch
batch.col=electronic_night(n=3)
village.col=c("#EBCC2A","chocolate","chocolate","#3B9AB2","#F21A00","chocolate","chocolate","chocolate","#78B7C5","orange","chocolate")
study.col=viridis(n=2, alpha=0.8)

# set up colour palette. The 'NPG' palette (Nature Publising Group) from ggsci will be used for the standard palette
standard_col=c(pal_npg()(10), pal_futurama()(12), pal_tron()(3))
plasmo=brewer.pal(3, "Set2")

dev.off()

# load in data files -----------------------------------------------------------------------

# merged data
load(paste0(inputdir, "merged.read_counts.TMM.filtered.Rda"))

# calculate log cpm
lcpm=cpm(merged, log=T)

# DE analysis --------------------------------------------------------------------------------

# Set up design matrix for falciparum
design <- model.matrix(~0 + merged$samples$species + merged$samples$Island + merged$samples$EEF_falciparum + merged$samples$Merozoite_falciparum + merged$samples$oocyst_falciparum + merged$samples$Ring_falciparum + merged$samples$ookoo_falciparum + merged$samples$Trophozoite_falciparum + merged$samples$Gran + merged$samples$Bcell + merged$samples$CD4T + merged$samples$CD8T + merged$samples$NK + merged$samples$Mono + merged$samples$fract.total)
# rename columns to exclude spaces and unrecognised characters
colnames(design)=gsub("\\$", "", colnames(design)) %>% gsub("mergedsamplesspecies", "", .) %>% gsub("mergedsamplesbatch", "", .) %>% gsub("-", "", .) %>% gsub("mergedsamples", "", .) %>% gsub("study", "", .) %>% gsub("West Papua", "Mappi", .) 
# set up contrast matrix
contr.matrix <- makeContrasts(SickvsHealthy=(vivax + falciparum)/2 - healthy, FalcvsHealthy=falciparum - healthy, VivaxvsHealthy=vivax - healthy, levels=colnames(design))

# Using duplicate correlation and blocking -----------------------------------------------------

# First, we need to perform voom normalisation
v <- voom(merged, design, plot=T, normalize="quantile")

# create a new variable for blocking using sample IDs
# define sample names
samplenames <- colnames(merged)
samplenames[which(merged$samples$study=="indo")] <- sub("([A-Z]{3})([0-9]{3})", "\\1-\\2", samplenames[which(merged$samples$study=="indo")])
samplenames <- sapply(strsplit(samplenames, "[_.]"), `[`, 1)

merged$samples$ind <- samplenames

# Estimate the correlation between the replicates.
# Information is borrowed by constraining the within-block corre-lations to be equal between genes and by using empirical Bayes methods to moderate the standarddeviations between genes 
dupcor <- duplicateCorrelation(v, design, block=merged$samples$ind)
# The value dupcor$consensus estimates the average correlation within the blocks and should be positive
dupcor$consensus
#  0.8293714
median(v$weights)
# 5.543071

# run voom a second time with the blocking variable and estimated correlation
# The  vector y$samples$ind indicates the  two  blocks  corresponding  to  biological  replicates
pdf(paste0(outputdir,"Limma_voomDuplicateCorrelation_TMMNormalisation.pdf"), height=8, width=12)
par(mfrow=c(1,2))
vDup <- voom(merged, design, plot=TRUE, block=merged$samples$ind, correlation=dupcor$consensus, normalize="quantile")
dupcor <- duplicateCorrelation(vDup, design, block=merged$samples$ind) # get warning message: Too much damping - convergence tolerance not achievable
dupcor$consensus
# 0.8290492
median(vDup$weights)
# 5.435427

# With duplicate correction and blocking:
# the inter-subject correlation is input into the linear model fit
voomDupVfit <- lmFit(vDup, design, block=merged$samples$ind, correlation=dupcor$consensus)
voomDupVfit <- contrasts.fit(voomDupVfit, contrasts=contr.matrix)
voomDupEfit <- eBayes(voomDupVfit, robust=T)

plotSA(voomDupEfit, main="Mean-variance trend elimination with duplicate correction")
dev.off()

# save voom and efit object
save(vDup, file = paste0(outputdir, "vDup.Rda"))
save(voomDupEfit, file = paste0(outputdir, "voomDupEfit.Rda"))

# explore different pvalue thresholds
dt <- decideTests(voomDupEfit, p.value=0.05, lfc=0)
summary(dt)

#        SickvsHealthy SickvsSickothr HealthyvsSickothr
# Down            1642           3768              4415
# NotSig          8747           4461              2529
# Up              1397           3557              4842

dt <- decideTests(voomDupEfit, p.value=0.01, lfc=0)
summary(dt)

#        SickvsHealthy SickvsSickothr HealthyvsSickothr
# Down             909           3219              4080
# NotSig         10091           5739              3266
# Up               786           2828              4440

dt <- decideTests(voomDupEfit, p.value=0.01, lfc=1)
summary(dt)

#        SickvsHealthy SickvsSickothr HealthyvsSickothr
# Down             151            917              1584
# NotSig         11483          10090              8596
# Up               152            779              1606

# get top genes using toptable
topTable.SickvsHealthy <- topTable(voomDupEfit, coef=1, p.value=0.01, n=Inf, lfc=0, sort.by="p")
write.table(topTable.SickvsHealthy, file=paste0(outputdir,"topTable_mergedSamples_diseaseStatusSpecies_island_dupcor_SickvsHealthy.txt"))
topTable.FalcvsHealthy <- topTable(voomDupEfit, coef=2, p.value=0.01, n=Inf, sort.by="p")
write.table(topTable.FalcvsHealthy, file=paste0(outputdir,"topTable_mergedSamples_diseaseStatusSpecies_island_dupcor_FalcvsHealthy.txt"))
topTable.VivaxvsHealthy <- topTable(voomDupEfit, coef=3, p.value=0.01, n=Inf, sort.by="p")
write.table(topTable.VivaxvsHealthy, file=paste0(outputdir,"topTable_mergedSamples_diseaseStatusSpecies_island_dupcor_VivaxvsHealthy.txt"))

# # Without duplicate correlation -------------------------------------------------

# pdf(paste0(outputdir,"Limma_voom_TMMNormalisation.pdf"), height=8, width=12)
# vfit <- lmFit(v, design)
# vfit <- contrasts.fit(vfit, contrasts=contr.matrix)
# efit <- eBayes(vfit, robust=T)

# plotSA(efit, main="Mean-variance trend elimination without duplicate correction")
# dev.off()

# # get top genes using toptable
# topTable <- topTable(efit, coef=1, p.value=0.01, n=Inf, sort.by="p")

# # Let's check the correlation between those two measurements - sort by gene first, then cor test on adjusted p-value
# blockedVsNoBlock <- join(topTable.SickvsHealthy, topTable, by="ENSEMBL")
# cor(blockedVsNoBlock[,8], blockedVsNoBlock[,16], method="spearman", use="complete")
# # 0.98

# Visual QC of duplicate correlation voom output after fitting linear models --------------------------------------------------------------------------------------

# check to see p-value distribution is normal
# pdf(paste0(outputdir,"PvalueDist_NotAdjusted_dupCor.pdf"), height=15, width=10)
# hist(voomDupEfit$p.value, main=colnames(voomDupEfit), ylim=c(0,max(table(round(voomDupEfit$p.value, 1)))+1000), xlab="p-value")
# }
# dev.off()

# # check p-value distribution for adjusted p-values
# pdf(paste0(outputdir,"PvalueDist_Adjusted_dupCor.pdf"), height=15, width=10)
# topTable <- topTable(voomDupEfit, coef=1, n=Inf)
# histData <- hist(topTable$adj.P.Val, main=colnames(voomDupEfit), xlab="p-value")
# hist(topTable$adj.P.Val, main=colnames(voomDupEfit), ylim=c(0,max(histData$counts)+1000), xlab="p-value")
# dev.off()

# # Verify that control housekeeping genes are not significantly DE. Set up list of housekeeping genes as controls (from Eisenberg and Levanon, 2003)
# housekeeping=read.table(paste0(housekeepingdir,"Housekeeping_ControlGenes.txt"), as.is=T, header=F)
# # if this is broken, use host = "uswest.ensembl.org"
# ensembl.mart.90 <- useMart(biomart='ENSEMBL_MART_ENSEMBL', dataset='hsapiens_gene_ensembl', host = 'www.ensembl.org', ensemblRedirect = FALSE)
# biomart.results.table <- getBM(attributes = c('ensembl_gene_id', 'external_gene_name'), mart = ensembl.mart.90,values=housekeeping, filters="hgnc_symbol")
# hkGenes=as.vector(biomart.results.table[,1])
# hkControls=hkGenes[which(hkGenes %in% rownames(y$counts))]

# # Volcano plot with points of housekeeping genes
# pdf(paste0(outputdir,"VolcanoPlots_dupCorEfit.pdf"), height=15, width=10)
# plot(voomDupEfit$coef, -log10(as.matrix(voomDupEfit$p.value)), pch=20, main=colnames(voomDupEfit), xlab="log2FoldChange", ylab="-log10(pvalue)")
# points(voomDupEfit$coef[which(rownames(voomDupEfit$coef) %in% hkControls)], -log10(as.matrix(voomDupEfit$p.value)[which(rownames(voomDupEfit$coef) %in% hkControls)]) , pch=20, col=4, xlab="log2FoldChange", ylab="-log10(pvalue)")
# legend("topleft", "genes", "hk genes",fill=4)
# abline(v=c(-1,1))
# dev.off()

# PCA visualisation after correction and association with covariates ------------------------------------------------------------

# let's also visualise how our PCAs look after limma correction by using removeBatcheffect. Help on design of removeBatcheffects was given by the lovely John Blischak.
design <- model.matrix(~0 + merged$samples$diseaseStatus)
# rename columns to exclude spaces and unrecognised characters
colnames(design)=gsub("\\$", "", colnames(design))
batch.corrected.lcpm <- removeBatchEffect(lcpm, batch=merged$samples$Island, covariates=cbind(merged$samples$EEF_falciparum,merged$samples$Merozoite_falciparum,merged$samples$oocyst_falciparum,merged$samples$Ring_falciparum,merged$samples$ookoo_falciparum,merged$samples$Trophozoite_falciparum,merged$samples$bbSpz_vivax,merged$samples$Female_vivax,merged$samples$Male_vivax,merged$samples$Merozoite_vivax,merged$samples$oocyst_vivax,merged$samples$ook_vivax,merged$samples$Ring_vivax,merged$samples$ookoo_vivax,merged$samples$Schizont_vivax,merged$samples$sgSpz_vivax,merged$samples$Trophozoite_vivax,merged$samples$Gran,merged$samples$Bcell,merged$samples$CD4T,merged$samples$CD8T,merged$samples$NK,merged$samples$Mono,merged$samples$fract.total), design=design)

# assign covariate names
# subtract variables we don't need
subtract=c("group", "norm.factors")
# get index of unwanted variables
subtract=which(colnames(merged$samples) %in% subtract)
covariate.names = colnames(merged$samples)[-subtract]
for (name in covariate.names){
 assign(name, merged$samples[[paste0(name)]])
}

# library size needs to be broken up into chunks for easier visualisation of trends (for instance in Age, we want to see high age vs low age rather than the effect of every single age variable)
assign("lib.size", cut(as.numeric(as.character(merged$samples$lib.size)), breaks=5))

# assign names to covariate names so you can grab individual elements by name
names(covariate.names)=covariate.names

# assign factor variables
factorVariables=c(colnames(Filter(is.factor,merged$samples))[which(colnames(Filter(is.factor,merged$samples)) %in% covariate.names)], "lib.size")
numericVariables=colnames(Filter(is.numeric,merged$samples))[which(colnames(Filter(is.numeric,merged$samples)) %in% covariate.names)] %>% subset(., !(. %in% factorVariables))

# initial clustering analysis before data normalisation

disease=colnames(merged)[which(diseaseStatus=="sick")]

# PCA plotting function
plot.pca <- function(dataToPca, speciesCol, namesPch, sampleNames){
    pca <- prcomp(t(dataToPca), scale=T, center=T)
    pca.var <- pca$sdev^2/sum(pca$sdev^2)
    for (i in 1:9){
        pca_axis1=i
        pca_axis2=i+1
        plot(pca$x[,pca_axis1], pca$x[,pca_axis2], col=speciesCol, pch=namesPch, cex=1, xlab=paste0("PC", pca_axis1, " (", round(pca.var[pca_axis1]*100, digits=2), "% of variance)"), ylab=paste0("PC", pca_axis2, " (", round(pca.var[pca_axis2]*100, digits=2), "% of variance)", sep=""), main=name)
        #text(pca$x[disease,pca_axis1], pca$x[disease,pca_axis2], labels=disease, pos=3, cex=1)
        #points(pca$x[,pca_axis1][which(allreplicated==T)], pca$x[,pca_axis2][which(allreplicated==T)], col="black", pch=8, cex=2)
        #text(pca$x[,pca_axis1][which(allreplicated==T)], pca$x[,pca_axis2][which(allreplicated==T)], labels=samplenames[which(allreplicated==T)], pos=3)
        legend(legend=unique(sampleNames), pch=16, x="bottomright", col=unique(speciesCol), cex=0.6, title=name, border=F, bty="n")
        legend(legend=unique(as.numeric(merged$samples$study)), "topright", pch=unique(as.numeric(merged$samples$study)) + 14, title="Batch", cex=0.6, border=F, bty="n")
        }

    return(pca)
}

# PCA association function
pc.assoc <- function(pca.data){
    all.pcs <- data.frame()
    for (i in 1:ncol(pca.data$x)){
        all.assoc <- vector()
        for (j in 1:ncol(all.covars.df)){
            test.assoc <- anova(lm(pca.data$x[,i] ~ all.covars.df[,j]))[1,5]
            all.assoc <- c(all.assoc, test.assoc)
        }
        single.pc <- c(i, all.assoc)
        all.pcs <- rbind(all.pcs, single.pc)
    }
    names(all.pcs) <- c("PC", colnames(all.covars.df))

    print ("Here are the relationships between PCs and some possible covariates")
    print (all.pcs)
    return (all.pcs)
}

# get rid of covariates we aren't interested in
covariate.names=covariate.names[grep("lib.size",covariate.names, invert=T)]
# Prepare covariate matrix
all.covars.df <- merged$samples[,covariate.names]

# Plot PCA
for (name in factorVariables){
    pdf(paste0(outputdir,"removeBatchEffect_pcaresults_",name,".pdf"))
    pcaresults <- plot.pca(dataToPca=batch.corrected.lcpm, speciesCol=as.numeric(get(name)),namesPch=as.numeric(merged$samples$study) + 14,sampleNames=get(name))
    dev.off()
}

# plot numeric variables
for (name in numericVariables){
    initial = .bincode(get(name), breaks=seq(min(get(name), na.rm=T), max(get(name), na.rm=T), len = 80),include.lowest = TRUE)
    bloodCol <- colorRampPalette(c("blue", "red"))(79)[initial]
    pdf(paste0(outputdir,"removeBatchEffect_pcaresults_",name,".pdf"))
    pcaresults <- plot.pca(dataToPca=batch.corrected.lcpm, speciesCol=bloodCol,namesPch=as.numeric(merged$samples$study) + 14,sampleNames=get(name))
    #legend(legend=c("High","Low"), pch=16, x="bottomright", col=c(bloodCol[which.max(get(name))], bloodCol[which.min(get(name))]), cex=0.6, title=name, border=F, bty="n")
    #legend(legend=unique(as.numeric(merged$samples$batch)), "topright", pch=unique(as.numeric(merged$samples$batch)) + 14, title="Batch", cex=0.6, border=F, bty="n")
    dev.off()
}

# Get PCA associations
all.pcs <- pc.assoc(pcaresults)
all.pcs$Variance <- pcaresults$sdev^2/sum(pcaresults$sdev^2)

# shut off all devices
graphics.off()

# plot pca covariates association matrix to illustrate any potential confounding and evidence for batches
pdf(paste0(outputdir,"removeBatchEffect_significantCovariates_AnovaHeatmap.pdf"))
pheatmap(log(all.pcs[1:5,covariate.names]), cluster_col=F, col= colorRampPalette(brewer.pal(11, "RdYlBu"))(100), cluster_rows=F, main="Significant Covariates \n Anova")
dev.off()

# Write out the covariates:
write.table(all.pcs, file=paste0(outputdir,"pca_covariates_significanceLevels.txt"), col.names=T, row.names=F, quote=F, sep="\t")

# Summary and visualisation of gene trends ---------------------------------------------------------------------------

# make a heatmap of all top genes in one pdf

# set up annotation
col_fun = colorRamp2(c(-4, 0, 4), c("blue", "white", "red"))
annot_df <- data.frame(DiseaseStatus = as.character(diseaseStatus), Study = as.character(study))
# Create the heatmap annotation
ha <- HeatmapAnnotation(df = annot_df, col = list(DiseaseStatus = c("sick" =  standard_col[1], "healthy" = standard_col[2]), Study = c("indo" =  study.col[1], "yamagishi" = study.col[2])) )

pdf(paste0(outputdir,"Heatmap_topGenes_mergedTMM.pdf"), height=7, width=9)
# get annotation for gene names
edb <- EnsDb.Hsapiens.v86
topTable <- topTable(voomDupEfit, coef=1, p.value=0.01, lfc=1, n=Inf, sort.by="p")
index <- which(vDup$genes$ENSEMBL %in% topTable$ENSEMBL[1:10])
transformedHeatmap=t(scale(t(vDup$E[index,])))
genesymbol <- select(edb, keys=rownames(transformedHeatmap), columns=c("SYMBOL"), keytype="GENEID")$SYMBOL
rownames(transformedHeatmap)=genesymbol
Heatmap(transformedHeatmap, name = colnames(voomDupEfit)[1],top_annotation = ha)
dev.off()

# Summary and visualisation of gene trends ---------------------------------------------------------------------------

dt <- decideTests(voomDupEfit, p.value=0.01, lfc=1)

# Compare groups with venn diagrams
pdf(paste0(outputdir,"vennDiagram_allSigDEGenes_pval01_dupCor.pdf"))
ssvFeatureVenn(dt, group_names = NULL, counts_txt_size = 5,
  counts_as_labels = FALSE, show_outside_count = TRUE,
  line_width = 1, circle_colors = NULL, fill_alpha = 0.3,
  line_alpha = 1, counts_color = NULL, n_points = 200,
  return_data = FALSE)
dev.off()

# # malaria-specic genes
# malaria_genes=dt[which(dt[,1]!=0 & dt[,2]!=0 & dt[,3]==0),]
# write.table(malaria_genes, file=paste0(outputdir,"malariaSpecificGenes.txt"))
# # non-malaria sick genes
# otherSick_genes=dt[which(dt[,2]!=0 & dt[,3]!=0 & dt[,1]==0),]
# write.table(otherSick_genes, file=paste0(outputdir,"nonMalariaGenes_allOtherSick.txt"))
# # all genes expressed in healthy vs sick
# allSickVshealthy_genes=dt[which(dt[,1]!=0 & dt[,3]!=0 & dt[,2]==0),]
# write.table(allSickVshealthy_genes, file=paste0(outputdir,"allgenes_SickVsHealthy.txt"))
# # all common genes
# all.genes=dt[which(dt[,1]!=0 & dt[,2]!=0 & dt[,3]!=0),]
# write.table(all.genes, file=paste0(outputdir,"AllGenesInCommon.txt"))

# Volcano plots of top genes --------------------------------------------------

topTable.SickvsHealthy <- topTable(voomDupEfit, coef=1, p.value=1, n=Inf, lfc=0, sort.by="p")
pdf(paste0(outputdir,"volcanoPlot_topGenes_MalariaVsHealthControls.pdf"))
EnhancedVolcano(topTable.SickvsHealthy, lab = topTable.SickvsHealthy$SYMBOL, pCutoff = 0.01, FCcutoff = 1, x = 'logFC', y = 'adj.P.Val', title = "Malaria vs Healthy Controls")
dev.off()

topTable.FalcvsHealthy <- topTable(voomDupEfit, coef=2, p.value=1, n=Inf, lfc=0, sort.by="p")
pdf(paste0(outputdir,"volcanoPlot_topGenes_FalciparumVsHealthy.pdf"))
EnhancedVolcano(topTable.FalcvsHealthy, lab = topTable.FalcvsHealthy$SYMBOL, pCutoff = 0.01, FCcutoff = 1, x = 'logFC', y = 'adj.P.Val', title = "Falciparum vs Healthy Controls")
dev.off()

topTable.VivaxvsHealthy <- topTable(voomDupEfit, coef=3, p.value=1, n=Inf, lfc=0, sort.by="p")
pdf(paste0(outputdir,"volcanoPlot_topGenes_VivaxvsHealthy.pdf"))
EnhancedVolcano(topTable.VivaxvsHealthy, lab = topTable.VivaxvsHealthy$SYMBOL, pCutoff = 0.01, FCcutoff = 1, x = 'logFC', y = 'adj.P.Val', title = "Vivax vs Healthy Controls")
dev.off()

# plot all three volcano plots together
pdf(paste0(outputdir,"volcanoPlot_All2.pdf"), width=9, height=9)
ggarrange(EnhancedVolcano(topTable.SickvsHealthy, lab = topTable.SickvsHealthy$SYMBOL, xlim=c(-4,6), x = 'logFC', y = 'adj.P.Val', title = "Malaria vs Controls", transcriptLabSize = 5), EnhancedVolcano(topTable.FalcvsHealthy, lab = topTable.FalcvsHealthy$SYMBOL, xlim=c(-4,4), x = 'logFC', y = 'adj.P.Val', title = "Falciparum vs Healthy Controls", transcriptLabSize = 5), EnhancedVolcano(topTable.VivaxvsHealthy, lab = topTable.VivaxvsHealthy$SYMBOL, x = 'logFC', y = 'adj.P.Val', xlim=c(-8,5), title = "Vivax vs Healthy", transcriptLabSize = 5), labels = c("A", "B", "C"), ncol = 2, nrow = 2)
dev.off()

# top ranked genes -----------------------------------------------------------------------------------------

edb <- EnsDb.Hsapiens.v86
topTable <- topTable(voomDupEfit, coef=1, p.value=0.01, lfc=1, n=Inf, sort.by="p")
topGenes <- topTable$SYMBOL[1:10]
topEnsembl <- rownames(topTable)[1:10]

# To visualise distributions, we'll be making violin plots using ggpubr which needs p-value labels. Let's go ahead and make a matrix to input this into ggpubr
# first set up matrix
topGenes.pvalue=matrix(nrow=length(topEnsembl), ncol=ncol(voomDupEfit))
rownames(topGenes.pvalue)=topEnsembl
colnames(topGenes.pvalue)=colnames(voomDupEfit)
for (i in 1:ncol(voomDupEfit)){
    # get significant genes over a logFC of 1 for all Island comparisons
    topTable <- topTable(voomDupEfit, coef=i, n=Inf)
    for(j in topEnsembl){
        # input the adjusted p.value for each gene
        topGenes.pvalue[j,i]=topTable[j,"adj.P.Val"]
    }
}

# make pvalues into scientific notation with max 3 digits
topGenes.pvalue=formatC(topGenes.pvalue, format="e", digits=2, drop0trailing=T)
# convert e notation to base 10 notation
topGenes.pvalue=sub("e", "x10^", topGenes.pvalue)

# reset ensemble row names to gene symbols
rownames(vDup$E)=vDup$genes$SYMBOL

# We can make the violin plots using ggpubr
pdf(paste0(outputdir,"TopGenes_ggboxplot_Island.pdf"), height=8, width=10)
counter=0
for(ensembl in topEnsembl){
    counter=counter+1
    gene.df <- data.frame(vDup$E[which(vDup$genes$ENSEMBL==ensembl),],diseaseStatus)
    colnames(gene.df)=c("CPM", "diseaseStatus")
    annotation_df <- data.frame(start=c("healthy"), end=c("sick"), y=c(max(gene.df[,1]+4)), label=paste("limma p-value =",topGenes.pvalue[ensembl,1],sep=" "))
    print(ggviolin(gene.df, x = "diseaseStatus", y = "CPM", fill="diseaseStatus", add=c("jitter","boxplot"), main=topGenes[counter], palette=1:2, add.params = c(list(fill = "white"), list(width=0.05))) + geom_signif(data=annotation_df,aes(xmin=start, xmax=end, annotations=label, y_position=y),textsize = 5, vjust = -0.2,manual=TRUE) + ylim(NA, max(gene.df[,1])+7))
}
dev.off()

# Genes associated with malaria resistance ----------------------------

# plot the distribution of all genes involved in genetic resistance to malaria
# from paper: https://www.sciencedirect.com/science/article/pii/S0002929707629097
# resistance genes: SLC4A1, G6PD, HP, ICAM1, PECAM1, CR1, FCGR2A, HLA-B, HLA-DR, IFNAR1, IFNG, IFNGR1, IL1A/IL1B, IL10, IL12B, IL4, MBL2, NOS2A, TNF, TNFSF5

# # set all genes implicated in genetic resistance to malaria as a variable
# immune_genes = c("\\bSLC4A1\\b", "\\bG6PD\\b", "\\bHP\\b", "\\bICAM1\\b", "\\bPECAM1\\b", "\\bCR1\\b", "\\bFCGR2A\\b", "\\bHLA-B\\b", "\\bHLA-DR\\b", "\\bIFNAR1\\b", "\\bIFNG\\b", "\\bIFNGR1\\b", "\\bIL1A\\b", "\\bIL1B\\b", "\\bIL10\\b", "\\bIL12B\\b", "\\bIL4\\b", "\\bMBL2\\b", "\\bNOS2A\\b", "\\bTNF\\b", "\\bTNFSF5\\b")
# # see which resistance genes are in the merged DGE list object
# immune_genes = merged$genes$SYMBOL[grep(paste0(immune_genes, collapse="|"), merged$genes$SYMBOL)]
# immune_list=voomDupEfit$coefficients[which(voomDupEfit$genes$SYMBOL %in% immune_genes),]

# pdf(paste0(outputdir,"AllMalariaGenesExpression_Heatmap.pdf"), height=8, width=10)
# Heatmap(immune_list)
# dev.off()

# # Three genes, "HP", "ICAM1", and "IFNG", are found to be DE in sick vs healthy samples and are implicated in resistance to malaria.
# # Let's look at the distribution of these genes
# pdf(paste0(outputdir,"MalariaGenes_standalone.pdf"), height=8, width=10)
# counter=0
# for(gene in c("HP", "ICAM1", "IFNG")){
#     counter=counter+1
#     gene.df <- data.frame(batch.corrected.norm_data[gene,], diseaseStatus)
#     colnames(gene.df)=c("CPM", "diseaseStatus")
#     assign(gene, print(ggviolin(gene.df, x = "diseaseStatus", y = "CPM", add="jitter",shape=study, main=gene, color="diseaseStatus", palette=standard_col[1:3])))
# }
# dev.off()

# pdf(paste0(outputdir,"MalariaGenes_together.pdf"), height=8, width=10)
# ggarrange(HP, ICAM1, IFNG, labels = c("A", "B", "C"), ncol = 2, nrow = 2)
# dev.off()
