# Test out ABIS for deconvoluting blood using RNASeq data from 123 Indonesian samples
# Code developed by Katalina Bobowik, 20.03.2019
# using the shiny app from the paper from Monaco et al, 2019: https://www.cell.com/cell-reports/fulltext/S2211-1247(19)30059-2


# load packages
library(magrittr)
library(edgeR)
library(plyr)
library(RColorBrewer)
library(biomaRt)
library(ggpubr)
library(ggplot2)
library(ggsignif)
library(pheatmap)
library(viridis)
library(gplots)
library(circlize)
library(ComplexHeatmap)
library(EnsDb.Hsapiens.v86)
library(ggsci)
library(scales)
library(dendextend)
library(reshape2)
library(variancePartition)
library(doParallel)
library(preprocessCore)

# Set paths:
inputdir = "/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/Output/Malaria/"
# Set output directory and create it if it does not exist:
outputdir <- "/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/Output/Malaria/MergedData/DataExploration_TMM/"

if (file.exists(outputdir) == FALSE){
    dir.create(outputdir)
}

# Get covariate names for data ---------------------------

# merged data
load(paste0(inputdir, "MergedData/merged.read_counts.TMM.filtered.Rda"))
# assign covariate names
# subtract variables we don't need
subtract=c("group", "norm.factors")
# get index of unwanted variables
subtract=which(colnames(merged$samples) %in% subtract)
covariate.names = colnames(merged$samples)[-subtract]
for (name in covariate.names){
 assign(name, merged$samples[[paste0(name)]])
}

# library size needs to be broken up into chunks for easier visualisation of trends (for instance in Age, we want to see high age vs low age rather than the effect of every single age variable)
assign("lib.size", cut(as.numeric(as.character(merged$samples$lib.size)), breaks=5))

# assign names to covariate names so you can grab individual elements by name
names(covariate.names)=covariate.names

# assign factor variables
factorVariables=c(colnames(Filter(is.factor,merged$samples))[which(colnames(Filter(is.factor,merged$samples)) %in% covariate.names)], "lib.size")
numericVariables=colnames(Filter(is.numeric,merged$samples))[which(colnames(Filter(is.numeric,merged$samples)) %in% covariate.names)] %>% subset(., !(. %in% factorVariables))

# Get batch.corrected.lcpm values -----------------------

# calculate log cpm
lcpm=cpm(merged, log=T)
# let's also visualise how our PCAs look after limma correction by using removeBatcheffect. Help on design of removeBatcheffects was given by the lovely John Blischak.
design <- model.matrix(~0 + merged$samples$species)

# rename columns to exclude spaces and unrecognised characters
colnames(design)=gsub("\\$", "", colnames(design))
colnames(design)=gsub("mergedsamplesspecies","",colnames(design))
batch.corrected.lcpm <- removeBatchEffect(lcpm, batch=merged$samples$Island, covariates=cbind(merged$samples$sex, merged$samples$EEF_falciparum,merged$samples$Merozoite_falciparum,merged$samples$oocyst_falciparum,merged$samples$Ring_falciparum,merged$samples$ookoo_falciparum,merged$samples$Trophozoite_falciparum,merged$samples$bbSpz_vivax,merged$samples$Female_vivax,merged$samples$Male_vivax,merged$samples$Merozoite_vivax,merged$samples$oocyst_vivax,merged$samples$ook_vivax,merged$samples$Ring_vivax,merged$samples$ookoo_vivax,merged$samples$Schizont_vivax,merged$samples$sgSpz_vivax,merged$samples$Trophozoite_vivax,merged$samples$Gran,merged$samples$Bcell,merged$samples$CD4T,merged$samples$CD8T,merged$samples$NK,merged$samples$Mono), design=design)

# load in voom and efit objects
load(paste0(inputdir,"MergedData/DE_TMM/vDup_TMM.Rda"))
load(paste0(inputdir,"MergedData/DE_TMM/voomDupEfit_TMM.Rda"))

# Get topTable values -----------------------

# set top table gene sognificance parameters
topTable.FalcvsHealthy <- topTable(voomDupEfit, coef=2, p.value=0.05, n=Inf, lfc=0, sort.by="logFC")
topTable.VivaxvsHealthy <- topTable(voomDupEfit, coef=3, p.value=0.05, n=Inf, lfc=0, sort.by="logFC")

# Perform statstical analyses and plot --------------------

a=batch.corrected.lcpm[unique(c(rownames(topTable.FalcvsHealthy[grep("RPL|MRPL|RPS",topTable.FalcvsHealthy$SYMBOL),]),rownames(topTable.VivaxvsHealthy[grep("RPL|MRPL|RPS",topTable.VivaxvsHealthy$SYMBOL),]))),]
colnames(a)=paste(diseaseStatus,study,sep="_")
colnames(a)=gsub("yamagishi","36BP",colnames(a)) %>% gsub("indo","100BP",.)
b=melt(a)
colnames(b)=c("ENSEMBL","diseaseStatus","CPM")
b$diseaseStatus <- factor(b$diseaseStatus, levels = c('malaria_36BP','malaria_100BP','control_36BP','control_100BP'),ordered = TRUE)
pdf(paste0(outputdir,"diseaseStatus_readLengthComparison_translationalProteins.pdf"))
ggboxplot(b, "diseaseStatus", "CPM", color="diseaseStatus", palette =c("#DD3D2D", "#F67E4B", "#5AAE61","#1B7837")) 
dev.off()

# Perform ANOVA and TUKEY analysis
summary(aov(b[,3] ~ b$diseaseStatus))
TUKEY <- TukeyHSD(x=aov(b[,3] ~ b$diseaseStatus), 'b$diseaseStatus', conf.level=0.95)
write.table(TUKEY[[1]],file=paste0(outputdir,"TUKEYtest_unscaled_translationalGenes.txt"))
