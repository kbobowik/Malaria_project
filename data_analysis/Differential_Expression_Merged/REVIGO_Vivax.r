

# A plotting R script produced by the REVIGO server at http://revigo.irb.hr/
# If you found REVIGO useful in your work, please cite the following reference:
# Supek F et al. "REVIGO summarizes and visualizes long lists of Gene Ontology
# terms" PLoS ONE 2011. doi:10.1371/journal.pone.0021800


# --------------------------------------------------------------------------
# If you don't have the ggplot2 package installed, uncomment the following line:
# install.packages( "ggplot2" );
library( ggplot2 );
# --------------------------------------------------------------------------
# If you don't have the scales package installed, uncomment the following line:
# install.packages( "scales" );
library( scales );

outputdir <- "/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/Output/Malaria/MergedData/GOandCamera/"

# --------------------------------------------------------------------------
# Here is your data from REVIGO. Scroll down for plot configuration options.

revigo.names <- c("term_ID","description","frequency_%","plot_X","plot_Y","plot_size","log10_p_value","uniqueness","dispensability");
revigo.data <- rbind(c("GO:0002376","immune system process", 0.600, 0.269, 0.679, 4.886,-3.9440,0.993,0.000),
c("GO:0008150","biological_process",100.000,-1.430, 1.796, 7.108,-35.8570,1.000,0.000),
c("GO:0008152","metabolic process",75.387,-1.162, 1.528, 6.986,-26.2907,0.998,0.000),
c("GO:0009058","biosynthetic process",31.611,-0.960, 1.732, 6.608,-19.5670,0.965,0.000),
c("GO:0009987","cellular process",63.780,-1.739, 0.623, 6.913,-36.5986,0.998,0.000),
c("GO:0023052","signaling", 6.765,-2.875,-2.082, 5.939,-6.7959,0.994,0.000),
c("GO:0033036","macromolecule localization", 3.030, 0.214,-6.111, 5.590,-8.0747,0.934,0.000),
c("GO:0033554","cellular response to stress", 2.967,-5.144,-3.651, 5.581,-11.9031,0.836,0.000),
c("GO:0048519","negative regulation of biological process", 1.984,-6.150, 5.765, 5.406,-9.3251,0.752,0.000),
c("GO:0050896","response to stimulus",12.210,-2.525,-1.713, 6.195,-9.3830,0.994,0.000),
c("GO:0051179","localization",18.495, 0.158,-0.484, 6.375,-7.5467,0.995,0.000),
c("GO:0065007","biological regulation",20.498,-1.340, 0.291, 6.420,-20.7905,0.995,0.000),
c("GO:0071840","cellular component organization or biogenesis", 8.568,-2.276,-0.397, 6.041,-7.7399,0.994,0.000),
c("GO:0000956","nuclear-transcribed mRNA catabolic process", 0.137, 4.885, 4.795, 4.244,-5.2692,0.800,0.032),
c("GO:0006915","apoptotic process", 0.406, 1.293,-1.806, 4.717,-8.7773,0.841,0.036),
c("GO:0006457","protein folding", 0.903, 0.153,-0.978, 5.064,-4.2676,0.954,0.040),
c("GO:0006996","organelle organization", 3.595,-0.417,-3.018, 5.664,-6.5622,0.920,0.048),
c("GO:0007159","leukocyte cell-cell adhesion", 0.090,-0.929, 0.176, 4.061,-3.8425,0.949,0.065),
c("GO:0008283","cell proliferation", 0.394,-0.198,-1.343, 4.704,-5.1898,0.962,0.074),
c("GO:0044237","cellular metabolic process",53.061, 4.335,-0.969, 6.833,-28.7721,0.909,0.082),
c("GO:0006807","nitrogen compound metabolic process",38.744,-3.045,-0.419, 6.696,-24.8069,0.964,0.088),
c("GO:0007154","cell communication", 7.219,-0.270, 0.124, 5.967,-6.5884,0.942,0.100),
c("GO:0071704","organic substance metabolic process",58.357, 0.174,-2.492, 6.874,-26.7190,0.961,0.119),
c("GO:0044238","primary metabolic process",53.743,-0.842,-2.819, 6.839,-25.1733,0.961,0.120),
c("GO:0001775","cell activation", 0.171, 1.878,-2.312, 4.341,-3.6724,0.914,0.157),
c("GO:0042773","ATP synthesis coupled electron transport", 0.221, 3.806, 2.705, 4.452,-4.8761,0.785,0.161),
c("GO:0008219","cell death", 0.458, 1.860,-2.773, 4.769,-8.0680,0.907,0.172),
c("GO:0006266","DNA ligation", 0.102, 6.491, 2.255, 4.115,-4.1261,0.865,0.179),
c("GO:0031392","regulation of prostaglandin biosynthetic process", 0.001,-3.591, 6.201, 2.021,-4.8356,0.766,0.180),
c("GO:0044260","cellular macromolecule metabolic process",34.276, 5.192, 5.219, 6.643,-23.9788,0.815,0.191),
c("GO:0006793","phosphorus metabolic process",13.507, 5.796,-0.911, 6.239,-6.8665,0.899,0.194),
c("GO:0044281","small molecule metabolic process",15.138, 1.035,-1.012, 6.288,-3.4334,0.919,0.196),
c("GO:1901360","organic cyclic compound metabolic process",30.324, 3.627, 0.046, 6.590,-16.0250,0.924,0.198),
c("GO:1901576","organic substance biosynthetic process",30.365, 6.533, 3.458, 6.591,-20.0092,0.853,0.198),
c("GO:0007049","cell cycle", 1.885, 1.279,-2.775, 5.384,-4.2020,0.895,0.200),
c("GO:0002181","cytoplasmic translation", 0.064, 3.051, 6.845, 3.915,-4.2411,0.823,0.210),
c("GO:0006729","tetrahydrobiopterin biosynthetic process", 0.032, 4.945, 3.265, 3.613,-3.2838,0.875,0.219),
c("GO:0043170","macromolecule metabolic process",39.491, 5.018,-0.852, 6.705,-21.1226,0.920,0.224),
c("GO:0001817","regulation of cytokine production", 0.108,-5.832, 4.717, 4.141,-3.5911,0.765,0.246),
c("GO:0006892","post-Golgi vesicle-mediated transport", 0.052, 0.001,-6.169, 3.822,-3.1323,0.952,0.249),
c("GO:0006725","cellular aromatic compound metabolic process",29.628, 5.266, 0.527, 6.580,-17.2620,0.884,0.260),
c("GO:0046483","heterocycle metabolic process",29.664, 5.450, 0.504, 6.580,-17.6990,0.884,0.260),
c("GO:0002682","regulation of immune system process", 0.252,-5.851, 4.961, 4.510,-5.4868,0.720,0.267),
c("GO:0016071","mRNA metabolic process", 0.798, 6.183, 5.048, 5.010,-3.8289,0.831,0.270),
c("GO:0042127","regulation of cell proliferation", 0.313,-6.105, 5.240, 4.603,-4.9393,0.749,0.273),
c("GO:0070647","protein modification by small protein conjugation or removal", 0.821, 1.270, 7.224, 5.023,-3.5732,0.837,0.274),
c("GO:2000145","regulation of cell motility", 0.154,-5.681, 3.057, 4.296,-3.2527,0.719,0.276),
c("GO:0034641","cellular nitrogen compound metabolic process",34.137, 5.570, 2.783, 6.641,-20.4802,0.835,0.277),
c("GO:0046165","alcohol biosynthetic process", 0.112, 3.823, 1.085, 4.159,-3.5103,0.891,0.292),
c("GO:0015833","peptide transport", 0.298,-0.091,-5.914, 4.582,-7.0969,0.928,0.295),
c("GO:0008380","RNA splicing", 0.413, 4.360, 5.043, 4.725,-3.7837,0.829,0.299),
c("GO:1990592","protein K69-linked ufmylation", 0.002,-0.499, 4.586, 2.371,-3.7393,0.887,0.301),
c("GO:0043603","cellular amide metabolic process", 6.879, 6.072, 2.517, 5.946,-9.2321,0.863,0.310),
c("GO:0050790","regulation of catalytic activity", 1.575,-5.746, 5.676, 5.306,-5.6882,0.723,0.312),
c("GO:0042592","homeostatic process", 1.661,-5.984, 5.635, 5.329,-3.8249,0.764,0.314),
c("GO:0065009","regulation of molecular function", 1.726,-5.743, 5.397, 5.345,-6.7282,0.765,0.316),
c("GO:0010605","negative regulation of macromolecule metabolic process", 1.169,-3.786, 6.842, 5.176,-8.8356,0.606,0.316),
c("GO:0048522","positive regulation of cellular process", 1.585,-6.251, 5.526, 5.308,-8.3536,0.657,0.328),
c("GO:0043412","macromolecule modification", 9.785, 2.495, 4.529, 6.099,-9.5834,0.888,0.331),
c("GO:0048518","positive regulation of biological process", 1.744,-6.186, 5.697, 5.350,-8.8894,0.755,0.332),
c("GO:0006984","ER-nucleus signaling pathway", 0.013,-6.468, 1.130, 3.215,-3.5664,0.762,0.340),
c("GO:0046907","intracellular transport", 1.564, 0.048,-6.211, 5.302,-6.2218,0.897,0.359),
c("GO:0034088","maintenance of mitotic sister chromatid cohesion", 0.004, 0.962,-3.992, 2.680,-3.3625,0.914,0.360),
c("GO:0071705","nitrogen compound transport", 1.767,-0.111,-6.298, 5.355,-5.9208,0.936,0.365),
c("GO:0051641","cellular localization", 2.041,-0.451,-5.797, 5.418,-6.6198,0.936,0.372),
c("GO:0018198","peptidyl-cysteine modification", 0.060, 0.327, 6.616, 3.888,-3.0211,0.870,0.387),
c("GO:0051174","regulation of phosphorus metabolic process", 0.580,-4.566, 5.734, 4.872,-3.2323,0.688,0.390),
c("GO:0034248","regulation of cellular amide metabolic process", 0.700,-2.843, 6.753, 4.954,-3.3875,0.662,0.405),
c("GO:0019538","protein metabolic process",18.489, 3.204, 5.162, 6.375,-11.8125,0.873,0.408),
c("GO:1901564","organonitrogen compound metabolic process",17.886, 6.366, 1.954, 6.361,-14.4949,0.880,0.415),
c("GO:0010467","gene expression",19.671, 3.083, 5.157, 6.402,-11.8447,0.874,0.417),
c("GO:0071702","organic substance transport", 4.980,-0.059,-6.347, 5.805,-7.1487,0.929,0.423),
c("GO:0032268","regulation of cellular protein metabolic process", 1.473,-2.579, 6.934, 5.277,-6.8097,0.601,0.437),
c("GO:0006357","regulation of transcription from RNA polymerase II promoter", 1.273,-0.300, 6.798, 5.213,-5.0320,0.608,0.442),
c("GO:0044267","cellular protein metabolic process",14.293, 3.451, 6.724, 6.263,-14.0985,0.794,0.446),
c("GO:0010033","response to organic substance", 0.900,-4.989,-4.060, 5.062,-6.7696,0.878,0.449),
c("GO:0051246","regulation of protein metabolic process", 1.551,-3.081, 6.911, 5.299,-6.5214,0.656,0.453),
c("GO:0048583","regulation of response to stimulus", 1.120,-6.609, 2.130, 5.158,-5.2541,0.706,0.461),
c("GO:0006366","transcription from RNA polymerase II promoter", 1.430, 5.482, 5.604, 5.264,-5.1720,0.789,0.465),
c("GO:0071569","protein ufmylation", 0.002,-0.429, 5.065, 2.425,-3.4646,0.889,0.468),
c("GO:0050794","regulation of cellular process",18.840,-5.474, 5.832, 6.383,-19.5157,0.648,0.474),
c("GO:0006139","nucleobase-containing compound metabolic process",26.547, 6.056, 3.378, 6.532,-17.6364,0.796,0.484),
c("GO:0006310","DNA recombination", 1.641, 6.294, 4.985, 5.323,-3.4341,0.823,0.494),
c("GO:0000722","telomere maintenance via recombination", 0.011,-2.258, 5.297, 3.156,-3.4315,0.760,0.495),
c("GO:1901566","organonitrogen compound biosynthetic process",14.064, 6.161, 4.042, 6.256,-8.2027,0.826,0.500),
c("GO:0006414","translational elongation", 0.777, 3.587, 6.356, 4.999,-3.0655,0.778,0.508),
c("GO:0051103","DNA ligation involved in DNA repair", 0.039, 1.590,-1.946, 3.703,-4.2140,0.798,0.518),
c("GO:0034654","nucleobase-containing compound biosynthetic process",14.533, 5.578, 3.817, 6.271,-11.3439,0.767,0.526),
c("GO:0016070","RNA metabolic process",15.951, 5.197, 5.009, 6.311,-13.7447,0.763,0.541),
c("GO:1901362","organic cyclic compound biosynthetic process",17.871, 6.089, 3.506, 6.360,-10.9706,0.834,0.545),
c("GO:0019438","aromatic compound biosynthetic process",16.954, 6.425, 3.745, 6.338,-11.5560,0.808,0.552),
c("GO:0007005","mitochondrion organization", 0.418,-0.474,-3.063, 4.729,-3.2524,0.932,0.553),
c("GO:1901798","positive regulation of signal transduction by p53 class mediator", 0.004,-6.876, 1.002, 2.674,-5.1007,0.716,0.554),
c("GO:0034645","cellular macromolecule biosynthetic process",19.291, 4.365, 5.422, 6.394,-15.5272,0.762,0.555),
c("GO:0018130","heterocycle biosynthetic process",17.388, 6.219, 3.585, 6.348,-11.6556,0.806,0.556),
c("GO:0009057","macromolecule catabolic process", 1.953, 1.537, 5.127, 5.399,-4.1838,0.856,0.557),
c("GO:2000766","negative regulation of cytoplasmic translation", 0.002,-1.742, 7.450, 2.314,-4.1129,0.682,0.558),
c("GO:0009059","macromolecule biosynthetic process",19.548, 5.226, 5.864, 6.399,-15.8794,0.825,0.558),
c("GO:0006950","response to stress", 4.575,-5.180,-3.465, 5.769,-9.4377,0.886,0.559),
c("GO:0042221","response to chemical", 3.071,-5.129,-3.863, 5.595,-7.2840,0.890,0.562),
c("GO:0034976","response to endoplasmic reticulum stress", 0.100,-5.001,-4.408, 4.106,-3.8043,0.876,0.564),
c("GO:0001816","cytokine production", 0.120,-3.549, 1.127, 4.187,-3.3800,0.932,0.579),
c("GO:0044271","cellular nitrogen compound biosynthetic process",22.502, 6.295, 4.080, 6.460,-16.7423,0.794,0.587),
c("GO:0050999","regulation of nitric-oxide synthase activity", 0.008,-5.068, 5.621, 3.017,-4.5622,0.765,0.588),
c("GO:0006468","protein phosphorylation", 4.137, 2.005, 6.403, 5.725,-3.4718,0.769,0.592),
c("GO:0015031","protein transport", 2.251, 0.005,-6.563, 5.461,-6.8996,0.892,0.593),
c("GO:1904667","negative regulation of ubiquitin protein ligase activity", 0.004,-3.572, 6.526, 2.740,-3.1787,0.657,0.595),
c("GO:0034097","response to cytokine", 0.136,-4.953,-3.869, 4.242,-5.8894,0.885,0.597),
c("GO:0090304","nucleic acid metabolic process",21.449, 5.056, 4.595, 6.440,-14.6990,0.768,0.597),
c("GO:0006810","transport",17.616,-0.088,-6.494, 6.354,-5.6840,0.918,0.600),
c("GO:1901698","response to nitrogen compound", 0.178,-4.924,-4.499, 4.359,-4.9318,0.892,0.612),
c("GO:0051341","regulation of oxidoreductase activity", 0.019,-5.180, 6.022, 3.388,-4.6003,0.762,0.627),
c("GO:0035556","intracellular signal transduction", 4.000,-6.409, 2.204, 5.710,-6.5834,0.621,0.630),
c("GO:0080134","regulation of response to stress", 0.337,-6.359, 1.712, 4.636,-4.3010,0.708,0.639),
c("GO:0007166","cell surface receptor signaling pathway", 0.920,-6.429, 2.038, 5.072,-3.1155,0.671,0.641),
c("GO:1901701","cellular response to oxygen-containing compound", 0.345,-5.075,-4.398, 4.647,-4.9830,0.847,0.651),
c("GO:0043604","amide biosynthetic process", 6.374, 5.785, 4.744, 5.913,-8.2612,0.802,0.655),
c("GO:0046146","tetrahydrobiopterin metabolic process", 0.032, 3.705, 1.984, 3.614,-3.2838,0.896,0.656),
c("GO:0044249","cellular biosynthetic process",30.048, 6.538, 3.882, 6.586,-18.5670,0.821,0.658),
c("GO:0045900","negative regulation of translational elongation", 0.004,-1.686, 7.509, 2.666,-3.9527,0.668,0.658),
c("GO:0006796","phosphate-containing compound metabolic process",13.110, 5.140, 0.886, 6.226,-7.0747,0.856,0.664),
c("GO:0002709","regulation of T cell mediated immunity", 0.010,-6.743, 1.161, 3.113,-3.1128,0.710,0.670),
c("GO:1901700","response to oxygen-containing compound", 0.503,-4.979,-4.470, 4.810,-5.1403,0.883,0.675),
c("GO:0006979","response to oxidative stress", 0.575,-5.097,-3.507, 4.868,-4.4377,0.896,0.679),
c("GO:0051716","cellular response to stimulus", 9.561,-5.090,-4.333, 6.089,-11.8962,0.832,0.680),
c("GO:0034086","maintenance of sister chromatid cohesion", 0.004, 0.997,-4.005, 2.758,-3.3625,0.913,0.685),
c("GO:0031331","positive regulation of cellular catabolic process", 0.058,-5.124, 6.308, 3.870,-4.4535,0.645,0.686),
c("GO:0042886","amide transport", 0.337, 0.301,-5.979, 4.636,-7.1124,0.940,0.689),
c("GO:0043933","macromolecular complex subunit organization", 2.371,-0.516,-3.509, 5.483,-3.1030,0.923,0.689),
c("GO:0031323","regulation of cellular metabolic process",11.662,-3.959, 6.553, 6.175,-16.9747,0.593,0.693),
c("GO:0019222","regulation of metabolic process",11.942,-5.805, 5.685, 6.185,-18.5735,0.683,0.698),
c("GO:0009259","ribonucleotide metabolic process", 2.752, 4.896, 2.126, 5.548,-3.0912,0.756,0.698));

one.data <- data.frame(revigo.data);
names(one.data) <- revigo.names;
one.data <- one.data [(one.data$plot_X != "null" & one.data$plot_Y != "null"), ];
one.data$plot_X <- as.numeric( as.character(one.data$plot_X) );
one.data$plot_Y <- as.numeric( as.character(one.data$plot_Y) );
one.data$plot_size <- as.numeric( as.character(one.data$plot_size) );
one.data$log10_p_value <- as.numeric( as.character(one.data$log10_p_value) );
one.data$frequency <- as.numeric( as.character(one.data$frequency) );
one.data$uniqueness <- as.numeric( as.character(one.data$uniqueness) );
one.data$dispensability <- as.numeric( as.character(one.data$dispensability) );
#head(one.data);


# --------------------------------------------------------------------------
# Names of the axes, sizes of the numbers and letters, names of the columns,
# etc. can be changed below

p1 <- ggplot( data = one.data );
p1 <- p1 + geom_point( aes( plot_X, plot_Y, colour = log10_p_value, size = plot_size), alpha = I(0.6) ) + scale_size_area();
p1 <- p1 + scale_colour_gradientn( colours = c("blue", "green", "yellow", "red"), limits = c( min(one.data$log10_p_value), 0) );
p1 <- p1 + geom_point( aes(plot_X, plot_Y, size = plot_size), shape = 21, fill = "transparent", colour = I (alpha ("black", 0.6) )) + scale_size_area();
p1 <- p1 + scale_size( range=c(5, 30)) + theme_bw(); # + scale_fill_gradientn(colours = heat_hcl(7), limits = c(-300, 0) );
ex <- one.data [ one.data$dispensability < 0.40, ]; 
p1 <- p1 + geom_text( data = ex, aes(plot_X, plot_Y, label = description), colour = I(alpha("black", 0.85)), size = 3 );
p1 <- p1 + labs (y = "semantic space x", x = "semantic space y");
p1 <- p1 + theme(legend.key = element_blank()) ;
one.x_range = max(one.data$plot_X) - min(one.data$plot_X);
one.y_range = max(one.data$plot_Y) - min(one.data$plot_Y);
p1 <- p1 + xlim(min(one.data$plot_X)-one.x_range/10,max(one.data$plot_X)+one.x_range/10);
p1 <- p1 + ylim(min(one.data$plot_Y)-one.y_range/10,max(one.data$plot_Y)+one.y_range/10);



# --------------------------------------------------------------------------
# Output the plot to screen

p1;

# Uncomment the line below to also save the plot to a file.
# The file type depends on the extension (default=pdf).

ggsave(paste0(outputdir,"Revigo_Vivax.pdf"));
