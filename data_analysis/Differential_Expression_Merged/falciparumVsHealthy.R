

# script created by KSB, 08.08.18
# Perform DE analysing relationship between islands

### Last edit: KB 03.04.2019

# Load dependencies and set input paths --------------------------

# Load dependencies:
library(edgeR)
library(plyr)
library(NineteenEightyR)
library(RColorBrewer)
library(biomaRt)
library(ggpubr)
library(ggplot2)
library(ggsignif)
library(pheatmap)
library(viridis)
library(gplots)
library(circlize)
library(ComplexHeatmap)
library(EnsDb.Hsapiens.v86)
library(ggsci)
library(scales)
library(dendextend)
library(reshape2)
library(variancePartition)
library(doParallel)
library(devtools)
library(Biobase)
library(preprocessCore)
library(magrittr)
library(EnhancedVolcano)
library(seqsetvis)

# Set paths:
inputdir = "/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/Output/Malaria/MergedData/"
housekeepingdir="/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/BatchEffects/"

# Set output directory and create it if it does not exist:
outputdir <- "/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/Output/Malaria/MergedData/TMMvsQNorm/"

if (file.exists(outputdir) == FALSE){
    dir.create(outputdir)
}

# Load colour schemes:
wes=c("#3B9AB2", "#EBCC2A", "#F21A00", "#00A08A", "#ABDDDE", "#000000", "#FD6467","#5B1A18")
palette(c(wes, brewer.pal(8,"Dark2")))
# set up colour palette for batch
batch.col=electronic_night(n=3)
village.col=c("#EBCC2A","chocolate","chocolate","#3B9AB2","#F21A00","chocolate","chocolate","chocolate","#78B7C5","orange","chocolate")
study.col=viridis(n=2, alpha=0.8)

# set up colour palette. The 'NPG' palette (Nature Publising Group) from ggsci will be used for the standard palette
standard_col=c(pal_npg()(10), pal_futurama()(12), pal_tron()(3))
plasmo=brewer.pal(3, "Set2")

dev.off()

# load in data files -----------------------------------------------------------------------

# merged data
load(paste0(inputdir, "merged.read_counts.TMM.filtered.Rda"))

# calculate log cpm
lcpm=cpm(merged, log=T)

# DE analysis --------------------------------------------------------------------------------

# Set up design matrix
design <- model.matrix(~0 + merged$samples$species + merged$samples$Island + merged$samples$sex + merged$samples$EEF_falciparum + merged$samples$Merozoite_falciparum + merged$samples$oocyst_falciparum + merged$samples$Ring_falciparum + merged$samples$ookoo_falciparum + merged$samples$Trophozoite_falciparum + merged$samples$Gran + merged$samples$Bcell + merged$samples$CD4T + merged$samples$CD8T + merged$samples$NK + merged$samples$Mono)
design <- model.matrix(~0 + merged$samples$species + merged$samples$Island + merged$samples$sex + merged$samples$Gran + merged$samples$Bcell + merged$samples$CD4T + merged$samples$CD8T + merged$samples$NK + merged$samples$Mono)

# rename columns to exclude spaces and unrecognised characters
colnames(design)=gsub("\\$", "", colnames(design)) %>% gsub("mergedsamplesspecies", "", .) %>% gsub("mergedsamplesbatch", "", .) %>% gsub("-", "", .) %>% gsub("mergedsamples", "", .) %>% gsub("study", "", .) %>% gsub("West Papua", "Mappi", .) 
# set up contrast matrix
contr.matrix <- makeContrasts(SickvsHealthy=(vivax + falciparum)/2 - control, FalcvsHealthy=falciparum - control, VivaxvsHealthy=vivax - control, VivaxvsFalciparum=vivax - falciparum, levels=colnames(design))

# Using duplicate correlation and blocking -----------------------------------------------------

# First, we need to perform voom normalisation
v <- voom(merged, design, plot=T, normalize="quantile")

