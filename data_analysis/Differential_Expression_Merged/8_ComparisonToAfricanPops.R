# script created by KSB, 08.08.18

# Load dependencies and set input paths --------------------------

library(ggplot2)
library(dplyr)
library(viridis)
library(hrbrthemes)
library(edgeR)
library(biomaRt)
library(UpSetR)
library(seqsetvis)
library(ComplexHeatmap)
library(RColorBrewer)
library(goseq)
library(schoolmath)
library(ggpubr)
library(magrittr)
library(reshape2)
library(ggrepel)

species.col=c(viridis(5)[1],brewer.pal(3, "Set2")[1],"coral1")

# Set paths:
inputdir = "/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/Output/Malaria/MergedData/"
referenceFiles = "/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/ReferenceFiles/OtherPops_MalariaDEGenes/"
outputdir <- "/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/Output/Malaria/MergedData/PopComparisons/"

# load in falciparum pop dataset from Adrican population
Idaghdour_CasesVsControls=read.table(paste0(referenceFiles,"Idaghdour_HumanDEGenes_CasesVsControls.txt"), sep="\t", header=T)
# load in viviax pop dataset from South American population
Vallejo_naiveVsControls=read.table(paste0(referenceFiles,"Vallejo_MalariaNaive.txt"), na.strings = c("", "NA"), sep="\t", header=T)
Vallejo_exposedVsControls=read.table(paste0(referenceFiles,"Vallejo_MalariaExposed.txt"), na.strings = c("", "NA"), sep="\t", header=T)
# remove rows where there is no gene information
Vallejo_naiveVsControls = Vallejo_naiveVsControls[-which(is.na(Vallejo_naiveVsControls$Genes)),]
Vallejo_exposedVsControls = Vallejo_exposedVsControls[-which(is.na(Vallejo_exposedVsControls$Genes)),]
rownames(Vallejo_naiveVsControls) = Vallejo_naiveVsControls$ensembl_gene_id
rownames(Vallejo_exposedVsControls) = Vallejo_exposedVsControls$ensembl_gene_id
Tran_MalariaExp_FebrileVsHealthy=read.table(paste0(referenceFiles,"malExp_Febrile.txt"), sep="\t", header=T)
rownames(Tran_MalariaExp_FebrileVsHealthy) = Tran_MalariaExp_FebrileVsHealthy$genes
# the Tran et al dataset does not have a filter for FDR-corrected pvalue
Tran_MalariaExp_FebrileVsHealthy = Tran_MalariaExp_FebrileVsHealthy[which(Tran_MalariaExp_FebrileVsHealthy$FDR < 0.05),]

# sapply(strsplit(Vallejo_naiveVsControls$Genes, "[..]"), `[`, 1)

# load in voom and efit objects
load(paste0(inputdir,"DE_TMM/vDup_TMM.Rda"))
load(paste0(inputdir,"DE_TMM/voomDupEfit_TMM.Rda"))

# set top table gene sognificance parameters
topTable.FalcvsHealthy <- topTable(voomDupEfit, coef=2, p.value=0.05, n=Inf, lfc=0, sort.by="logFC")
topTable.VivaxvsHealthy <- topTable(voomDupEfit, coef=3, p.value=0.05, n=Inf, lfc=0, sort.by="logFC")
dt <- decideTests(voomDupEfit, p.value=0.05, lfc=0)

# compare vivax vs falciparum in LFC values
allENSEMBL=data.frame(c(topTable.FalcvsHealthy$ENSEMBL,topTable.VivaxvsHealthy$ENSEMBL))
colnames(allENSEMBL)=c("ENSEMBL")
vivax_df=data.frame(topTable.VivaxvsHealthy$ENSEMBL, topTable.VivaxvsHealthy$logFC)
colnames(vivax_df)=c("ENSEMBL","Vivax")
combined_vvx=merge(allENSEMBL, vivax_df, all = TRUE)
falc_df=data.frame(topTable.FalcvsHealthy$ENSEMBL, topTable.FalcvsHealthy$logFC)
colnames(falc_df)=c("ENSEMBL","Falciparum")
combined_all=merge(combined_vvx, falc_df, all = TRUE)
# combined_all[is.na(combined_all)] <- 0

# plot density of LFC values
meltedLFC=melt(combined_all)
colnames(meltedLFC)=c("ENSEMBL","Species","LogFC")
pdf(paste0(outputdir,"FalciparumAndVivax_Variance.pdf"), width=10)
ggplot(meltedLFC, aes(x=LogFC, fill=Species)) + geom_density(alpha=0.4) + theme_bw(base_size = 14) +
  labs(title = "LFC Distribution of Significant Genes", y="Density" ) +
  annotate("text", x = -3.8, y = 0.65, label = paste0("Vivax Variance = ",round(var(topTable.VivaxvsHealthy$logFC),2)), size = 4) +
  annotate("text", x = -3.5, y = 0.62, label = paste0("Falciparum Variance = ",round(var(topTable.FalcvsHealthy$logFC),2)), size = 4)
dev.off()

# Falciparum (Idaghdour study) --------------------------------------------

# add ensembl IDs for Idaghdour gene names
# if this is broken, use host = "uswest.ensembl.org"/'www.ensembl.org'
ensembl.mart.90 <- useMart(biomart='ENSEMBL_MART_ENSEMBL', dataset='hsapiens_gene_ensembl', host = "http://jan2020.archive.ensembl.org", ensemblRedirect = FALSE)
biomart.results.table <- getBM(attributes = c('ensembl_gene_id', 'external_gene_name'), mart = ensembl.mart.90, values=as.character(Idaghdour_CasesVsControls$Gene), filters="hgnc_symbol")
write.table(biomart.results.table, file=paste0(outputdir,"Idaghdour_Genes.txt"), row.names=F)
# add in this information as another column
Idaghdour_CasesVsControls$ENSEMBL = biomart.results.table$ensembl_gene_id[match(Idaghdour_CasesVsControls$Gene, biomart.results.table$external_gene_name)]
# remove duplicated genes
Idaghdour_CasesVsControls=Idaghdour_CasesVsControls[-which(duplicated(Idaghdour_CasesVsControls$ENSEMBL)),]
Idaghdour_CasesVsControls = Idaghdour_CasesVsControls[-which(is.na(Idaghdour_CasesVsControls$ENSEMBL)),]
rownames(Idaghdour_CasesVsControls) = Idaghdour_CasesVsControls$ENSEMBL
# match significnat gene names with Idaghdour gene names
Idaghdour=match(rownames(dt), Idaghdour_CasesVsControls$ENSEMBL)
# convert this into a 0x1 matrix (in order to be the same as the dt table)
Idaghdour[is.na(Idaghdour)] = 0
Idaghdour[which(Idaghdour>0)] = 1
names(Idaghdour) = rownames(dt)

# add ensembl IDs
TranEF=match(rownames(dt), as.character(Tran_MalariaExp_FebrileVsHealthy$genes))
# convert this into a 0x1 matrix (in order to be the same as the dt table)
TranEF[is.na(TranEF)] = 0
TranEF[which(TranEF>0)] = 1
names(TranEF) = rownames(dt)

# repurpose dt
dt = dt[,c(2,3)]
dt = as.data.frame(dt)
dt$Benin = unname(Idaghdour)
dt$Mali = unname(TranEF)
colnames(dt)[1] = "Indonesia"

pdf(paste0(outputdir,"VennFalciparum.pdf"))
ssvFeatureVenn(dt[,c(1,3,4)], group_names = NULL, counts_txt_size = 5,
  counts_as_labels = FALSE, show_outside_count = TRUE,
  line_width = 1, circle_colors = c(species.col[1],"blue","cadetblue3"), fill_alpha = 0.3,
  line_alpha = 1, counts_color = NULL, n_points = 200,
  return_data = FALSE)
dev.off()

# compare to just exposed individuals
pdf(paste0(outputdir,"VennFalciparum_exposed.pdf"))
ssvFeatureVenn(dt[,c(1,4)], group_names = NULL, counts_txt_size = 5,
  counts_as_labels = FALSE, show_outside_count = TRUE,
  line_width = 1, circle_colors = c(species.col[1],"blue"), fill_alpha = 0.3,
  line_alpha = 1, counts_color = NULL, n_points = 200,
  return_data = FALSE) + labs(title = "Falciparum")
dev.off()

# compute the hypergeometric p-value, as read in https://seqqc.wordpress.com/2019/07/25/how-to-use-phyper-in-r/
# phyper(Overlap-1, group2, Total-group2, group1,lower.tail= FALSE)
# comparison to malaria-exposed adults
phyper(128-1, 2569, 9166, 551, lower.tail= FALSE)
# 0.2327322
# comparison to P. falciparum-infected children
phyper(116-1, 2173, 9562, 551, lower.tail= FALSE)
#0.08741089

pdf(paste0(outputdir,"UpsetPlotFalciparum.pdf"))
upset(abs(dt), sets = c("Indonesia", "Benin", "Mali"), sets.bar.color = "#56B4E9", nintersects=100, keep.order=TRUE)
dev.off()

shared_genes_ENSEMBL=rownames(dt[which(dt[,1]!=0 & dt[,3]!=0 & dt[,4]!=0),])
# merged data
load(paste0(inputdir, "merged.read_counts.TMM.filtered.Rda"))
shared_genes_Gene = merged[shared_genes_ENSEMBL,]$genes$SYMBOL
write.table(data.frame(shared_genes_ENSEMBL, shared_genes_Gene), file=paste0(outputdir,"sharedGenesAll.txt"), row.names=F)
shared_genes_indo_ENSEMBL = rownames(dt[which(dt[,1]!=0 & dt[,4]!=0),])
shared_genes_indo_Gene = merged[shared_genes_indo_ENSEMBL,]$genes$SYMBOL
write.table(data.frame(shared_genes_indo_ENSEMBL, shared_genes_indo_Gene), file=paste0(outputdir,"sharedGenesIndo.txt"), row.names=F)
indo_unique_genes_ENSEMBL = rownames(dt[which(dt[,1]!=0 & dt[,3]==0 & dt[,4]==0),])
indo_unique_genes_Gene = merged[indo_unique_genes_ENSEMBL,]$genes$SYMBOL
write.table(data.frame(indo_unique_genes_ENSEMBL, indo_unique_genes_Gene), file=paste0(outputdir,"uniqueIndoGenes.txt"), row.names=F)
all_common_genes_ENSEMBL = rownames(dt[which(dt[,1]!=0 & dt[,2]!=0 & dt[,3]!=0 & dt[,4]!=0),])
all_common_genes_Gene = merged[all_common_genes_ENSEMBL,]$genes$SYMBOL
write.table(data.frame(all_common_genes_ENSEMBL, all_common_genes_Gene), file=paste0(outputdir,"AllCommonGenesAllGroups.txt"), row.names=F)

# Heatmap of all African populations
# plot(topTable.FalcvsHealthy[rownames(dt[which(dt[,2]!=0 & dt[,4]!=0),]),"logFC"],Idaghdour_CasesVsControls[which(Idaghdour_CasesVsControls$Gene %in% merged[rownames(dt[which(dt[,2]!=0 & dt[,4]!=0),]),]$genes$SYMBOL), 4])
x=data.frame(topTable.FalcvsHealthy[rownames(dt[which(dt[,1]!=0 & dt[,3]!=0 & dt[,4]!=0),]),"logFC"],Idaghdour_CasesVsControls[which(Idaghdour_CasesVsControls$Gene %in% merged[rownames(dt[which(dt[,1]!=0 & dt[,3]!=0 & dt[,4]!=0),]),]$genes$SYMBOL), 4],Tran_MalariaExp_FebrileVsHealthy[rownames(dt[which(dt[,1]!=0 & dt[,3]!=0 & dt[,4]!=0),]),"logFC"])
colnames(x) = c("Indonesia","Benin","Mali")
rownames(x) = merged[rownames(dt[which(dt[,1]!=0 & dt[,3]!=0 & dt[,4]!=0),]),]$genes$SYMBOL
pdf(paste0(outputdir,"HeatmapFalciparum_AllComparisonPops.pdf"))
Heatmap(as.matrix(x))
dev.off()

# comparison of just malaria naive
x=data.frame(topTable.FalcvsHealthy[rownames(dt[which(dt[,1]!=0 & dt[,3]!=0),]),"logFC"],Idaghdour_CasesVsControls[rownames(dt[which(dt[,1]!=0 & dt[,3]!=0),]),4])
colnames(x) = c("Indonesia","Benin")
rownames(x) = merged[rownames(dt[which(dt[,1]!=0 & dt[,3]!=0),]),]$genes$SYMBOL

# Correlation plot
#title <- expression(paste("Indonesia vs. Benin",italic("P. Falciparum"), " Enriched Genes"))
outliers=vector()
for(lfc in 1:nrow(x)){
  rowMagnitude = is.positive(x[lfc,])
  if(rowMagnitude[1]!=rowMagnitude[2]){
    outliers=c(outliers,rownames(x)[lfc])
  }
}
shared = vector()
for(lfc in 1:nrow(x)){
  rowMagnitude = is.positive(x[lfc,])
  if(rowMagnitude[1]==rowMagnitude[2]){
    shared=c(shared,rownames(x)[lfc])
  }
}
topFalcGenes = topTable.FalcvsHealthy$SYMBOL[1:100]
pdf(paste0(outputdir,"CorPlot_BeninVIndonesia.pdf"))
ggscatter(x, x = "Indonesia", y = "Benin",fullrange = TRUE, size=2, shape=19,col="mediumblue")+
geom_smooth(method = "lm", se = FALSE) +
theme(axis.ticks.x=element_blank(),panel.background=element_rect(colour="#d2d3d5",fill="#FFFFFF"), panel.grid.major = element_line(size = 0.5, linetype = 'solid',colour = "#ececed"),panel.grid.minor = element_line(size = 0.25, linetype = 'solid',colour = "#ececed"), plot.title = element_text(size = 30)) +
stat_cor(label.x = -3.2, label.y = 3.2) +
geom_text_repel(aes(label=ifelse(rownames(x) %in% topFalcGenes,as.character(rownames(x)),'')),hjust=0,vjust=0,nudge_x = -0.2, nudge_y = 0.05, size=6) + 
geom_abline(intercept = 0, slope = 1) + geom_abline(intercept = 0, slope = 0) + geom_vline(xintercept = 0) + ggtitle("Indonesia vs. Benin") +
xlab("Indonesia (LFC)") + ylab("Benin (LFC)")
dev.off()

# comparison of just malaria exposed
x=data.frame(topTable.FalcvsHealthy[rownames(dt[which(dt[,1]!=0 & dt[,4]!=0),]),"logFC"],Tran_MalariaExp_FebrileVsHealthy[rownames(dt[which(dt[,1]!=0 & dt[,4]!=0),]),"logFC"])
colnames(x) = c("Indonesia","Mali")
rownames(x) = merged[rownames(dt[which(dt[,1]!=0 & dt[,4]!=0),]),]$genes$SYMBOL
pdf(paste0(outputdir,"HeatmapFalciparum_onlyExposesComparison.pdf"), height =12, width =9)
Heatmap(x, row_names_gp = gpar(fontsize = 7), name = "row Z-Score")
dev.off()

# Correlation plot
title <- expression(paste("Indonesia vs. Mali ",italic("P. Falciparum"), " Enriched Genes"))
outliers=vector()
for(lfc in 1:nrow(x)){
  rowMagnitude = is.positive(x[lfc,])
  if(rowMagnitude[1]!=rowMagnitude[2]){
    outliers=c(outliers,rownames(x)[lfc])
  }
}
shared = vector()
for(lfc in 1:nrow(x)){
  rowMagnitude = is.positive(x[lfc,])
  if(rowMagnitude[1]==rowMagnitude[2]){
    shared=c(shared,rownames(x)[lfc])
  }
}
topFalcGenes = topTable.FalcvsHealthy$SYMBOL[1:100]
pdf(paste0(outputdir,"CorPlot_MaliVIndonesia.pdf"))
ggscatter(x, x = "Indonesia", y = "Mali",fullrange = TRUE, size=2, shape=19,col="mediumblue")+
geom_smooth(method = "lm", se = FALSE) +
theme(axis.ticks.x=element_blank(),panel.background=element_rect(colour="#d2d3d5",fill="#FFFFFF"), panel.grid.major = element_line(size = 0.5, linetype = 'solid',colour = "#ececed"),panel.grid.minor = element_line(size = 0.25, linetype = 'solid',colour = "#ececed"), plot.title = element_text(size = 30)) +
stat_cor(label.x = -3, label.y = 2.6) +
geom_text_repel(aes(label=ifelse(rownames(x) %in% topFalcGenes,as.character(rownames(x)),'')),hjust=0,vjust=0,nudge_x = -0.2, nudge_y = 0.05, size=6) + 
geom_abline(intercept = 0, slope = 1) + geom_abline(intercept = 0, slope = 0) + geom_vline(xintercept = 0) + ggtitle(title) +
xlab("Indonesia (LFC)") + ylab("Mali (LFC)")
dev.off()

# Vivax (Vallejo study)  --------------------------------------------

dt <- decideTests(voomDupEfit, p.value=0.05, lfc=0)
dt = as.data.frame(dt)

# add ensembl IDs for Vallejo gene names
VallejoExposed=match(rownames(dt), as.character(Vallejo_exposedVsControls$ensembl_gene_id))
# convert this into a 0x1 matrix (in order to be the same as the dt table)
VallejoExposed[is.na(VallejoExposed)] = 0
VallejoExposed[which(VallejoExposed>0)] = 1
names(VallejoExposed) = rownames(dt)

# add ensembl IDs for Vallejo gene names
VallejoNaive=match(rownames(dt), as.character(Vallejo_naiveVsControls$ensembl_gene_id))
# convert this into a 0x1 matrix (in order to be the same as the dt table)
VallejoNaive[is.na(VallejoNaive)] = 0
VallejoNaive[which(VallejoNaive>0)] = 1
names(VallejoNaive) = rownames(dt)

# repurpose dt
dt$Colombia_Buenaventura = unname(VallejoExposed)
dt$Colombia_Cali = unname(VallejoNaive)
dt = dt[,c("VivaxvsHealthy","Colombia_Buenaventura","Colombia_Cali")]
colnames(dt)[1]="Indonesia"

pdf(paste0(outputdir,"UpsetPlotVivax.pdf"))
upset(abs(dt), sets = c("Indonesia","Colombia_Buenaventura", "Colombia_Cali"), sets.bar.color = "#56B4E9", nintersects=100, keep.order=TRUE)
dev.off()

pdf(paste0(outputdir,"VennVivax.pdf"))
ssvFeatureVenn(dt[,c(1,2,3)], group_names = NULL, counts_txt_size = 5,
  counts_as_labels = FALSE, show_outside_count = TRUE,
  line_width = 1, circle_colors = c(species.col[3],"coral4", "violetred2"), fill_alpha = 0.3,
  line_alpha = 1, counts_color = NULL, n_points = 200,
  return_data = FALSE)
dev.off()

pdf(paste0(outputdir,"VennVivax_TwoComparisons.pdf"))
ssvFeatureVenn(dt[,c(1,3)], group_names = NULL, counts_txt_size = 5,
  counts_as_labels = FALSE, show_outside_count = TRUE,
  line_width = 1, circle_colors = c(species.col[3],"blue"), fill_alpha = 0.3,
  line_alpha = 1, counts_color = NULL, n_points = 200,
  return_data = FALSE) + labs(title = "Vivax")
dev.off()

# compute the hypergeometric p-value, as read in https://seqqc.wordpress.com/2019/07/25/how-to-use-phyper-in-r/
# naive individuals
phyper(76-1, 919, 11735-919, 1078,lower.tail= FALSE)
# [1] 0.8561943
# exposed individuals
phyper(76-1, 919, 11735-919, 1078,lower.tail= FALSE)
# 0.7306612

shared_genes_ensembl=rownames(dt[which(dt[,1]!=0 & dt[,2]!=0 & dt[,3]!=0),])
shared_genes_genes = merged[shared_genes_ensembl,]$genes$SYMBOL
write.table(data.frame(shared_genes_ensembl, shared_genes_genes), file=paste0(outputdir,"sharedGenesVivax.txt"), row.names=F)
indo_unique_genes_ensembl = rownames(dt[which(dt[,1]!=0 & dt[,2]==0 & dt[,3]==0),])
indo_unique_genes_genes = merged[indo_unique_genes_ensembl,]$genes$SYMBOL
write.table(data.frame(indo_unique_genes_ensembl, indo_unique_genes_genes), file=paste0(outputdir,"uniqueIndoGenes_vivax.txt"), row.names=F)
shared_genes_naive_ensembl=rownames(dt[which(dt[,1]!=0 & dt[,3]!=0),])
shared_genes_naive_genes = merged[shared_genes_naive_ensembl,]$genes$SYMBOL
write.table(data.frame(shared_genes_naive_ensembl, shared_genes_naive_genes), file=paste0(outputdir,"sharedGenesNaiveVivax.txt"), row.names=F)

# Heatmap for all 
x=data.frame(topTable.VivaxvsHealthy[rownames(dt[which(dt[,1]!=0 & dt[,2]!=0 & dt[,3]!=0),]),"logFC"],Vallejo_exposedVsControls[rownames(dt[which(dt[,1]!=0 & dt[,2]!=0 & dt[,3]!=0),]),"logFC"],Vallejo_naiveVsControls[rownames(dt[which(dt[,1]!=0 & dt[,2]!=0 & dt[,3]!=0),]),"logFC"])
colnames(x) = c("Indonesia","Colombia_Exposed","Colombia_Naive")
rownames(x) = merged[rownames(dt[which(dt[,1]!=0 & dt[,2]!=0 & dt[,3]!=0),]),]$genes$SYMBOL
pdf(paste0(outputdir,"HeatmapVivax.pdf"))
Heatmap(x)
dev.off()

# just naive ----------------------------------

# heatmap
x=data.frame(topTable.VivaxvsHealthy[rownames(dt[which(dt[,1]!=0 & dt[,3]!=0),]),"logFC"],Vallejo_naiveVsControls[rownames(dt[which(dt[,1]!=0 & dt[,3]!=0),]),"logFC"])
colnames(x) = c("Indonesia","Colombia_Cali")
rownames(x) = merged[rownames(dt[which(dt[,1]!=0 & dt[,3]!=0),]),]$genes$SYMBOL
pdf(paste0(outputdir,"HeatmapVivax_Naive.pdf"),height =12, width =9)
Heatmap(x,row_names_gp = gpar(fontsize = 7), name = "row Z-Score")
dev.off()

# Correlation plot
title <- expression(paste("Indonesia vs. Colombia (Cali) ",italic("P. vivax"), " Enriched Genes"))
outliers = vector()
for(lfc in 1:nrow(x)){
  rowMagnitude = is.positive(x[lfc,])
  if(rowMagnitude[1]!=rowMagnitude[2]){
    outliers=c(outliers,rownames(x)[lfc])
  }
}
shared = vector()
for(lfc in 1:nrow(x)){
  rowMagnitude = is.positive(x[lfc,])
  if(rowMagnitude[1]==rowMagnitude[2]){
    shared=c(shared,rownames(x)[lfc])
  }
}
topVivaxGenes = topTable.VivaxvsHealthy$SYMBOL[1:350]
pdf(paste0(outputdir,"CorPlot_Colombia_CaliVIndonesia.pdf"))
ggscatter(x, x = "Indonesia", y = "Colombia_Cali",fullrange = TRUE, size=2, shape=19,col="tomato2")+
geom_smooth(method = "lm", se = FALSE, color="tomato2") +
theme(axis.ticks.x=element_blank(),panel.background=element_rect(colour="#d2d3d5",fill="#FFFFFF"), panel.grid.major = element_line(size = 0.5, linetype = 'solid',colour = "#ececed"),panel.grid.minor = element_line(size = 0.25, linetype = 'solid',colour = "#ececed"), plot.title = element_text(size = 30)) +
stat_cor(label.x = -3, label.y = 2.6) +
geom_text_repel(aes(label=ifelse(rownames(x) %in% topVivaxGenes,as.character(rownames(x)),'')),hjust=0,vjust=0,nudge_x = -0.2, nudge_y = 0.05, size=6) +
geom_abline(intercept = 0, slope = 1) + geom_abline(intercept = 0, slope = 0) + geom_vline(xintercept = 0) + 
ggtitle(title) + xlab("Indonesia (LFC)") + ylab("Colombia (LFC)")
dev.off()

# just exposed ----------------------------
x=data.frame(topTable.VivaxvsHealthy[rownames(dt[which(dt[,1]!=0 & dt[,2]!=0),]),"logFC"],Vallejo_exposedVsControls[rownames(dt[which(dt[,1]!=0 & dt[,2]!=0),]),"logFC"])
colnames(x) = c("Indonesia","Colombia_Buenaventura")
rownames(x) = merged[rownames(dt[which(dt[,1]!=0 & dt[,2]!=0),]),]$genes$SYMBOL
pdf(paste0(outputdir,"HeatmapVivax_Exposed.pdf"),height =12, width =9)
Heatmap(x,row_names_gp = gpar(fontsize = 7), name = "row Z-Score")
dev.off()

# Correlation plot
title <- expression(paste("Indonesia vs. Colombia (Buenaventura) ",italic("P. vivax"), " Enriched Genes"))
outliers = vector()
for(lfc in 1:nrow(x)){
  rowMagnitude = is.positive(x[lfc,])
  if(rowMagnitude[1]!=rowMagnitude[2]){
    outliers=c(outliers,rownames(x)[lfc])
  }
}
shared = vector()
for(lfc in 1:nrow(x)){
  rowMagnitude = is.positive(x[lfc,])
  if(rowMagnitude[1]==rowMagnitude[2]){
    shared=c(shared,rownames(x)[lfc])
  }
}
topVivaxGenes = topTable.VivaxvsHealthy$SYMBOL[1:200]
pdf(paste0(outputdir,"CorPlot_Colombia_BuenaventuraVIndonesia.pdf"))
ggscatter(x, x = "Indonesia", y = "Colombia_Buenaventura",fullrange = TRUE, size=2, shape=19,col="tomato2")+
geom_smooth(method = "lm", se = FALSE, color="tomato2") +
theme(axis.ticks.x=element_blank(),panel.background=element_rect(colour="#d2d3d5",fill="#FFFFFF"), panel.grid.major = element_line(size = 0.5, linetype = 'solid',colour = "#ececed"),panel.grid.minor = element_line(size = 0.25, linetype = 'solid',colour = "#ececed"), plot.title = element_text(size = 30)) +
stat_cor(label.x = -3, label.y = 2.6) +
geom_text_repel(aes(label=ifelse(rownames(x) %in% topVivaxGenes,as.character(rownames(x)),'')),hjust=0,vjust=0,nudge_x = -0.2, nudge_y = 0.05, size=6) +
geom_abline(intercept = 0, slope = 1) + geom_abline(intercept = 0, slope = 0) + geom_vline(xintercept = 0) + 
ggtitle(title) + xlab("Indonesia (LFC)") + ylab("Colombia (LFC)")
dev.off()

# all populations and species ---------------------------------

dt <- decideTests(voomDupEfit, p.value=0.05, lfc=0)
dt = as.data.frame(dt)
dt = dt[,c(2,3)]
dt = as.data.frame(dt)
colnames(dt)[1] = "Falc_Indo"
colnames(dt)[2] = "Vivax_Indo"
dt$Falc_Mali = unname(TranEF)
dt$Falc_Benin = unname(Idaghdour)
dt$Vivax_Naive = unname(VallejoNaive)
dt$Vivax_Exposed = unname(VallejoExposed)
# merge African and SA populations, then turn any duplicated genes (2) into single genes (1)
dt$Falc_Afr = rowSums(cbind(dt$Falc_Mali,dt$Falc_Benin))
dt$Falc_Afr = as.numeric(gsub(2,1,dt$Falc_Afr))
dt$Vivax_SA = rowSums(cbind(dt$Vivax_Naive,dt$Vivax_Exposed))
dt$Vivax_SA = as.numeric(gsub(2,1,dt$Vivax_SA))
# get rid of rows we don't want
dt = dt[,c("Falc_Indo","Vivax_Indo","Falc_Afr","Vivax_SA")]

pdf(paste0(outputdir,"upsetR_AllPops.pdf"), width=15)
#upset(abs(dt), sets = c("Falc_Indo", "Vivax_Indo", "Falc_Afr", "Vivax_SA"), sets.bar.color = "#56B4E9", nintersects=100, keep.order=TRUE) 
upset(abs(dt), sets = c("Vivax_SA","Vivax_Indo", "Falc_Afr", "Falc_Indo"), sets.bar.color = c("red",species.col[3],"mediumblue",species.col[1]), nintersects=100, keep.order=TRUE, point.size = 3, line.size = 0.7, text.scale = 2.2) 
dev.off()

# shared_genes_ensembl=rownames(dt[which(dt[,1]!=0 & dt[,2]!=0 & dt[,3]!=0 & dt[,4]!=0),])
# shared_genes_genes = merged[shared_genes_ensembl,]$genes$SYMBOL
# x=data.frame(topTable.FalcvsHealthy[rownames(dt[which(dt[,1]!=0 & dt[,2]!=0 & dt[,3]!=0 & dt[,4]!=0),]),"logFC"],topTable.VivaxvsHealthy[rownames(dt[which(dt[,1]!=0 & dt[,2]!=0 & dt[,3]!=0 & dt[,4]!=0),]),"logFC"],Tran_MalariaExp_FebrileVsHealthy[rownames(dt[which(dt[,1]!=0 & dt[,2]!=0 & dt[,3]!=0 & dt[,4]!=0),]),"logFC"],Vallejo_naiveVsControls[rownames(dt[which(dt[,1]!=0 & dt[,2]!=0 & dt[,3]!=0 & dt[,4]!=0),]),"logFC"])
# colnames(x) = c("Indonesia_Falciparum","Indonesia_Vivax","Mali","Colombia")
# rownames(x) = merged[rownames(dt[dt[,1]!=0 & dt[,2]!=0 & dt[,3]!=0 & dt[,4]!=0,]),]$genes$SYMBOL
# pdf(paste0(outputdir,"HeatmapAllPopulations.pdf"),height =12, width =9)
# Heatmap(x, name = "row Z-Score")
# row_names_gp = gpar(fontsize = 7), 
# dev.off()

# get unique genes and analyse which pathways these are involved in
unique_genes_ensembl_falc=topTable.FalcvsHealthy[rownames(dt[which(dt[,1]!=0 & dt[,2]==0 & dt[,3]==0 & dt[,4]==0),]),]
unique_genes_ensembl_falc_includingVivaxGenes=topTable.FalcvsHealthy[c(rownames(dt[which(dt[,1]!=0 & dt[,2]==0 & dt[,3]==0 & dt[,4]==0),]),rownames(dt[which(dt[,1]!=0 & dt[,2]!=0 & dt[,3]==0 & dt[,4]==0),])),]
write.table(unique_genes_ensembl_falc, file=paste0(outputdir,"uniqueFalciparumGenes.txt"), sep="\t")
write.table(unique_genes_ensembl_falc_includingVivaxGenes, file=paste0(outputdir,"uniqueFalciparumGenes_IncludingVivax.txt"), sep="\t")
unique_genes_ensembl_vivax=topTable.VivaxvsHealthy[rownames(dt[which(dt[,1]==0 & dt[,2]!=0 & dt[,3]==0 & dt[,4]==0),]),]
unique_genes_ensembl_vivax_includingFalcGenes=topTable.VivaxvsHealthy[c(rownames(dt[which(dt[,1]==0 & dt[,2]!=0 & dt[,3]==0 & dt[,4]==0),]),rownames(dt[which(dt[,1]!=0 & dt[,2]!=0 & dt[,3]==0 & dt[,4]==0),])),]
write.table(unique_genes_ensembl_vivax, file=paste0(outputdir,"uniqueVivaxGenes.txt"), sep="\t", quote=F,col.names=F, row.names=F)
write.table(unique_genes_ensembl_vivax_includingFalcGenes, file=paste0(outputdir,"uniqueVivaxGenes_IncludingFalc.txt"), sep="\t", quote=F, col.names=F, row.names=F)
shared_unique_genes = rownames(dt[which(dt[,1]!=0 & dt[,2]!=0 & dt[,3]==0 & dt[,4]==0),])
write.table(shared_unique_genes, file=paste0(outputdir,"sharedUNiqueGenes_BothSpecies.txt"), sep="\t", quote=F, col.names=F, row.names=F)

# plot density of shared genes compared to unique genes -------------------------

# for falciparum
shared_FalcIndo_FalcAFR=topTable.FalcvsHealthy[rownames(dt[which(dt[,1]!=0 & dt[,2]==0 & dt[,3]!=0 & dt[,4]==0),]),]$ENSEMBL
shared_FalcIndo_VivaxSA=topTable.FalcvsHealthy[rownames(dt[which(dt[,1]!=0 & dt[,2]==0 & dt[,3]==0 & dt[,4]!=0),]),]$ENSEMBL
shared_falcVivaxIndo_FalcAfr=topTable.FalcvsHealthy[rownames(dt[which(dt[,1]!=0 & dt[,2]!=0 & dt[,3]!=0 & dt[,4]==0),]),]$ENSEMBL
shared_falcIndo_FalcAfr_VivaxSA=topTable.FalcvsHealthy[rownames(dt[which(dt[,1]!=0 & dt[,2]==0 & dt[,3]!=0 & dt[,4]!=0),]),]$ENSEMBL
shared_falcVivaxIndo_VivaxSA=topTable.FalcvsHealthy[rownames(dt[which(dt[,1]!=0 & dt[,2]!=0 & dt[,3]==0 & dt[,4]!=0),]),]$ENSEMBL
all_Shared=topTable.FalcvsHealthy[rownames(dt[which(dt[,1]!=0 & dt[,2]!=0 & dt[,3]!=0 & dt[,4]!=0),]),]$ENSEMBL
sharedFalciparumGenes=unique(c(shared_FalcIndo_FalcAFR,shared_FalcIndo_VivaxSA,shared_falcVivaxIndo_FalcAfr,shared_falcIndo_FalcAfr_VivaxSA,shared_falcVivaxIndo_VivaxSA,all_Shared))

# plot
a=topTable.FalcvsHealthy[sharedFalciparumGenes,c("ENSEMBL","logFC")]
a$geneStatus="falciparum_shared"
b=topTable.FalcvsHealthy[unique_genes_ensembl_falc$ENSEMBL,c("ENSEMBL","logFC")]
b$geneStatus="falciparum_unique"
c=rbind(a,b)
pdf(paste0(outputdir,"LFC_DistributionsFalciparum.pdf"))
ggplot(c, aes(x=logFC, fill=geneStatus)) + geom_density(alpha=0.4) + scale_fill_manual(values=c("mediumblue",species.col[1])) + theme_classic() + ggtitle("P. falciparum LogFC Distribution")
dev.off()

# for vivax
shared_VivaxIndo_FalcAfr=topTable.VivaxvsHealthy[rownames(dt[which(dt[,1]==0 & dt[,2]!=0 & dt[,3]!=0 & dt[,4]==0),]),]$ENSEMBL
shared_VivaxIndo_VivaxSA=topTable.VivaxvsHealthy[rownames(dt[which(dt[,1]==0 & dt[,2]!=0 & dt[,3]==0 & dt[,4]!=0),]),]$ENSEMBL
shared_VivaxIndo_falcAfr_VivaxSA=topTable.VivaxvsHealthy[rownames(dt[which(dt[,1]==0 & dt[,2]!=0 & dt[,3]!=0 & dt[,4]!=0),]),]$ENSEMBL
shared_falcVivaxIndo_FalcAfr=topTable.VivaxvsHealthy[rownames(dt[which(dt[,1]!=0 & dt[,2]!=0 & dt[,3]!=0 & dt[,4]==0),]),]$ENSEMBL
shared_falcVivaxIndo_VivaxSA=topTable.VivaxvsHealthy[rownames(dt[which(dt[,1]!=0 & dt[,2]!=0 & dt[,3]==0 & dt[,4]!=0),]),]$ENSEMBL
all_Shared=topTable.VivaxvsHealthy[rownames(dt[which(dt[,1]!=0 & dt[,2]!=0 & dt[,3]!=0 & dt[,4]!=0),]),]$ENSEMBL
sharedVivaxGenes=unique(c(shared_VivaxIndo_FalcAfr,shared_VivaxIndo_VivaxSA,shared_VivaxIndo_falcAfr_VivaxSA,shared_falcVivaxIndo_FalcAfr,shared_falcVivaxIndo_VivaxSA,all_Shared))

# plot
a=topTable.VivaxvsHealthy[sharedVivaxGenes,c("ENSEMBL","logFC")]
a$geneStatus="vivax_shared"
b=topTable.VivaxvsHealthy[unique_genes_ensembl_vivax$ENSEMBL,c("ENSEMBL","logFC")]
b$geneStatus="vivax_unique"
c=rbind(a,b)
pdf(paste0(outputdir,"LFC_DistributionsVivax.pdf"))
ggplot(c, aes(x=logFC, fill=geneStatus)) + geom_density(alpha=0.4) + scale_fill_manual(values=c("red",species.col[3])) + theme_classic() + ggtitle("P. vivax LogFC Distribution")
dev.off()

# Run GoSeq on unique genes using ClueGO and plot ---------------------------------------------------------

# this is created from the file 'unique_genes_ensembl_falc_includingVivaxGenes'
UniqueFalciparumGoResults = read.delim("/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/Output/Malaria/MergedData/ClueGOEnrichmentVivaxFalciparum/ClueGOResults_UniqueFalciparumGenes/ClueGOResults_UniqueFalciparumGenesNodeAttributeTable.txt")

# histogram of GO terms
# for falciparum
pdf(paste0(outputdir,"enrichedGOterms_falciparum_ClueGO.pdf"), height=7, width=12)
ggplot(UniqueFalciparumGoResults[1:20,],  # your table. you just need the GO enriched terms and relative pvalues
       aes(x=reorder(GOTerm,-log10(Term.PValue.Corrected.with.Benjamini.Hochberg)),# this reorder the go terms in a descending order based on their pvalue 
              y=-log10(Term.PValue.Corrected.with.Benjamini.Hochberg))) + # this colors the bars based on my categorical variable (population, e.g. denisova, neandertal and other fancy people)
  geom_bar(stat = 'identity',position = 'dodge',fill=species.col[1])+   # stack the bars next to each other, you can play with the distance between them
  xlab(" ") + # i refuse to explain this 
  ylab("-log10 adjusted pvalue") + # and this
  theme_bw(base_size=14) + # customize theme of the plot
  theme(
    legend.position='none',
    legend.background=element_rect(),
    axis.text.x=element_text(angle=0, size=14, face="bold"),
    axis.text.y=element_text(angle=0, size=14, face="bold"),
    axis.title=element_text(size=10, face="bold"),
    legend.key=element_blank(),    
    legend.key.size=unit(1, "cm"),      
    legend.text=element_text(size=10),  
    title=element_text(size=10)) +
  coord_flip() 
dev.off()

# this is created from the file 'unique_genes_ensembl_vivax_includingFalcGenes'
UniqueVivaxGoResults = read.delim("/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/Output/Malaria/MergedData/ClueGOEnrichmentVivaxFalciparum/ClueGOResults_UniqueVivaxGenes/ClueGOResults_UniqueVivaxGenesNodeAttributeTable.txt")
UniqueVivaxGoResults$GOTerm = as.character(UniqueVivaxGoResults$GOTerm)
UniqueVivaxGoResults$GOTerm[1] = "regulation of alternative mRNA splicing"

# for Vivax
pdf(paste0(outputdir,"enrichedGOterms_Vivax_ClueGO.pdf"), height=7, width=12)
ggplot(UniqueVivaxGoResults[1:20,],  # your table. you just need the GO enriched terms and relative pvalues
       aes(x=reorder(GOTerm,-log10(Term.PValue.Corrected.with.Benjamini.Hochberg)),# this reorder the go terms in a descending order based on their pvalue 
              y=-log10(Term.PValue.Corrected.with.Benjamini.Hochberg))) + # this colors the bars based on my categorical variable (population, e.g. denisova, neandertal and other fancy people)
  geom_bar(stat = 'identity',position = 'dodge',fill=species.col[3])+   # stack the bars next to each other, you can play with the distance between them
  xlab(" ") + # i refuse to explain this 
  ylab("-log10 adjusted pvalue") + # and this
  theme_bw(base_size=14) + # customize theme of the plot
  theme(
    legend.position='none',
    legend.background=element_rect(),
    axis.text.x=element_text(angle=0, size=14, face="bold"),
    axis.text.y=element_text(angle=0, size=14, face="bold"),
    axis.title=element_text(size=10, face="bold"),
    legend.key=element_blank(),    
    legend.key.size=unit(1, "cm"),      
    legend.text=element_text(size=10),  
    title=element_text(size=10)) +
  coord_flip() 
dev.off()
