# script created by KSB, 08.08.18

# Load dependencies and set input paths --------------------------

library(ggplot2)
library(dplyr)
library(viridis)
library(hrbrthemes)
library(magrittr)
library(ggpubr)
library(ggrepel)

# Set paths:
inputdir = "/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/Output/Malaria/MergedData/Reactome/"

# Set output directory and create it if it does not exist:
outputdir <- "/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/Output/Malaria/MergedData/Reactome/"

# Falciparum --------------------------------------------

reactome_falciparum = read.csv(paste0(inputdir,"topTable_Falciparum_Reactome.csv"))
reactome_falciparum = reactome_falciparum %>% filter(Entities.FDR<=0.05) %>% dplyr::select(c(Pathway.identifier,Pathway.name,X.Entities.found,X.Entities.total,Entities.FDR))
# change column names
colnames(reactome_falciparum) = c("pathwaysID", "pathwayName", "nGenes", "totalGenes", "FDR")
reactome_falciparum$ratio = reactome_falciparum$nGenes/reactome_falciparum$totalGenes
reactome_falciparum = reactome_falciparum[1:20,]
reactome_falciparum <- reactome_falciparum[order(reactome_falciparum$ratio),]
pdf(paste0(outputdir,"FalciparumGenes_Reactome.pdf"), width=18)
# plasma_pal <- c(viridis::viridis(n = 4),"#F8766D", "red","maroon","orange","#00A5FF","black")
p.adjust <- reactome_falciparum$FDR
b=ggplot(reactome_falciparum, aes(x=reorder(pathwayName, sort(as.numeric(pathwayName))), y=ratio, size = nGenes)) + 
geom_point(aes(size=nGenes, color=p.adjust)) +
geom_segment(aes(x=pathwayName,xend=pathwayName, 
                   y=min(ratio), 
                   yend=max(ratio)),
  linetype="dashed", size=0.1) + labs(title="Falciparum Pathways") + 
  scale_colour_gradient2(low = "red", mid = "blue", high = "blue", midpoint = max(p.adjust), breaks = c(min(p.adjust),max(p.adjust)), limits=c(0,max(p.adjust)), labels=c(round(min(p.adjust),6),round(max(p.adjust),4))) + 
  scale_size_continuous(range = c(4,12)) + theme_bw() +
  coord_flip() + xlab("") + ylab("Gene Ratio") + theme(text = element_text(size=20), axis.text.y=element_text(colour="black"),axis.text.x=element_text(colour="black"))
b + guides(colour = guide_colorbar(order = 1))
dev.off()


# Shared genes (Vivax and Falciparum) --------------------------------------------

reactome_shared = read.csv(paste0(inputdir,"SharedGenes_FalciparumVivax.csv"))
reactome_shared = reactome_shared %>% filter(Entities.FDR<=0.05) %>% dplyr::select(c(Pathway.identifier,Pathway.name,X.Entities.found,X.Entities.total,Entities.FDR))
# change column names
colnames(reactome_shared) = c("pathwaysID", "pathwayName", "nGenes", "totalGenes", "FDR")
reactome_shared$ratio = reactome_shared$nGenes/reactome_shared$totalGenes
# read in pathways table, made with Ruby script
topLevelPathway=read.table(paste0(inputdir,"shared_VivaxFalciparumGenes_TopLevelPathways.txt"), sep="\t", strip.white=T)
colnames(topLevelPathway) = c("Reactome_ID","Pathway")
topLevelPathway$Pathway=gsub("Metabolism of proteins", "Protein/RNA Metabolism", topLevelPathway[,"Pathway"]) %>% gsub("Metabolism of RNA", "Protein/RNA Metabolism", .) %>% 
gsub("Disease", "Disease/Response to stimuli", .) %>% gsub("\\bCellular responses to external stimuli\\b", "Disease/Response to stimuli", .) %>% 
gsub("\\bDNA Replication\\b", "Transcription/DNA Replication", .) %>% 
gsub("Gene expression \\(Transcription\\)", "Transcription/DNA Replication", .)
reactome_shared$category = as.character(topLevelPathway$Pathway)
reactome_shared = reactome_shared[1:20,]
reactome_shared <- reactome_shared[order(reactome_shared$ratio),]
pdf(paste0(outputdir,"SharedGenes_FalciparumVivax.pdf"), width=15)
# plasma_pal <- c(viridis::viridis(n = 4),"#F8766D", "red","maroon","orange","#00A5FF","black")
p.adjust <- reactome_shared$FDR
b=ggplot(reactome_shared, aes(x=reorder(pathwayName, sort(as.numeric(pathwayName))), y=ratio, size = nGenes)) + 
geom_point(aes(size=nGenes, color=p.adjust)) +
geom_segment(aes(x=pathwayName,xend=pathwayName, 
                   y=min(ratio), 
                   yend=max(ratio)), 
  linetype="dashed", size=0.1) + labs(title="Shared Pathways") + 
  scale_colour_gradient2(low = "red", mid = "blue", high = "blue", midpoint = max(p.adjust), breaks = c(min(p.adjust),max(p.adjust)), limits=c(0,max(p.adjust)), labels=c(round(min(p.adjust),6),round(max(p.adjust),4))) + 
  scale_size_continuous(range = c(4,12)) + theme_bw() +
  coord_flip() + xlab("") + ylab("Gene Ratio") + theme(text = element_text(size=20), axis.text.y=element_text(colour="black"),axis.text.x=element_text(colour="black"))
  b + guides(colour = guide_colorbar(order = 1))
dev.off()

# Unique genes to P. falciparum -------------------------

reactome_falciparum_unique = read.csv(paste0(inputdir,"topTable_Falciparum_Reactome_Unique.csv"))
reactome_falciparum_unique= reactome_falciparum_unique %>% filter(Entities.FDR<=0.05) %>% dplyr::select(c(Pathway.identifier,Pathway.name,X.Entities.found,X.Entities.total,Entities.FDR))
# change column names
colnames(reactome_falciparum_unique) = c("pathwaysID", "pathwayName", "nGenes", "totalGenes", "FDR")
reactome_falciparum_unique$ratio = reactome_falciparum_unique$nGenes/reactome_falciparum_unique$totalGenes
reactome_falciparum_unique <- reactome_falciparum_unique[order(reactome_falciparum_unique$ratio),]
reactome_falciparum_unique$pathwayName = as.character(reactome_falciparum_unique$pathwayName)
# electrom transport is put in there twoce so we'll remove it
reactome_falciparum_unique = reactome_falciparum_unique[-6,]
reactome_falciparum_unique$pathwayName = as.factor(reactome_falciparum_unique$pathwayName)

pdf(paste0(outputdir,"FalciparumGenes_Reactome_Unique.pdf"), width=8)
# plasma_pal <- c(viridis::viridis(n = 4),"#F8766D", "red","maroon","orange","#00A5FF","black")
p.adjust <- reactome_falciparum_unique$FDR
b=ggplot(reactome_falciparum_unique, aes(x=reorder(pathwayName, sort(as.numeric(pathwayName))), y=ratio, size = nGenes)) + 
geom_point(aes(size=nGenes, color=p.adjust)) +
geom_segment(aes(x=pathwayName,xend=pathwayName, 
                   y=min(ratio), 
                   yend=max(ratio)),
  linetype="dashed", size=0.1) + labs(title="Falciparum Pathways") + 
  scale_colour_gradient2(low = "red", mid = "blue", high = "blue", midpoint = max(p.adjust), breaks = c(min(p.adjust),max(p.adjust)), limits=c(0,max(p.adjust)), labels=c(round(min(p.adjust),6),round(max(p.adjust),4))) + 
  scale_size_continuous(range = c(4,12)) + theme_bw() +
  coord_flip() + xlab("") + ylab("Gene Ratio") + theme(text = element_text(size=20), axis.text.y=element_text(colour="black"),axis.text.x=element_text(colour="black"))
b + guides(colour = guide_colorbar(order = 1))
dev.off()

# Unique genes to P. vivax -------------------------

reactome_vivax_unique = read.csv(paste0(inputdir,"topTable_vivax_Reactome_Unique.csv"))
reactome_vivax_unique= reactome_vivax_unique %>% filter(Entities.FDR<=0.05) %>% dplyr::select(c(Pathway.identifier,Pathway.name,X.Entities.found,X.Entities.total,Entities.FDR))
# change column names
colnames(reactome_vivax_unique) = c("pathwaysID", "pathwayName", "nGenes", "totalGenes", "FDR")
reactome_vivax_unique$ratio = reactome_vivax_unique$nGenes/reactome_vivax_unique$totalGenes
reactome_vivax_unique <- reactome_vivax_unique[order(reactome_vivax_unique$ratio),]
#reactome_vivax_unique$ratio = reactome_vivax_unique$ratio*100

pdf(paste0(outputdir,"vivaxGenes_Reactome_Unique.pdf"), width=10)
# plasma_pal <- c(viridis::viridis(n = 4),"#F8766D", "red","maroon","orange","#00A5FF","black")
p.adjust <- reactome_vivax_unique$FDR
b=ggplot(reactome_vivax_unique, aes(x=reorder(pathwayName, sort(as.numeric(pathwayName))), y=ratio, size = nGenes)) + 
geom_point(aes(size=nGenes, color=p.adjust)) +
geom_segment(aes(x=pathwayName,xend=pathwayName, 
                   y=min(ratio), 
                   yend=max(ratio)),
  linetype="dashed", size=0.1) + labs(title="Vivax Pathways") + 
  scale_colour_gradient2(low = "red", mid = "blue", high = "blue", midpoint = max(p.adjust), breaks = c(min(p.adjust),max(p.adjust)), limits=c(0,max(p.adjust)), labels=c(round(min(p.adjust),6),round(max(p.adjust),6))) + 
  scale_size_continuous(range = c(4,12)) + theme_bw() +
  coord_flip() + xlab("") + ylab("Gene Ratio") + theme(text = element_text(size=20), axis.text.y=element_text(colour="black"),axis.text.x=element_text(colour="black"))
b + guides(colour = guide_colorbar(order = 1))
dev.off()
