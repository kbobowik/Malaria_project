# script created by KSB
# Perform DE analysis of healthy vs sick samples

# Load dependencies and set input paths --------------------------

# Load dependencies:
library(edgeR)
library(plyr)
library(NineteenEightyR)
library(RColorBrewer)
library(biomaRt)
library(ggpubr)
library(ggplot2)
library(ggsignif)
library(pheatmap)
library(viridis)
library(gplots)
library(circlize)
library(ComplexHeatmap)
library(EnsDb.Hsapiens.v86)
library(ggsci)
library(scales)
library(dendextend)
library(reshape2)
library(variancePartition)
library(doParallel)
library(devtools)
library(Biobase)
library(preprocessCore)
library(magrittr)
library(EnhancedVolcano)
library(seqsetvis)

# Set paths:
inputdir = "/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/Output/Malaria/MergedData/"
housekeepingdir="/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/BatchEffects/"

# Set output directory and create it if it does not exist:
outputdir <- "/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/Output/Malaria/MergedData/"

if (file.exists(outputdir) == FALSE){
    dir.create(outputdir)
}

# Load colour schemes:
wes=c("#3B9AB2", "#EBCC2A", "#F21A00", "#00A08A", "#ABDDDE", "#000000", "#FD6467","#5B1A18")
palette(c(wes, brewer.pal(8,"Dark2")))
# set up colour palette for batch
batch.col=electronic_night(n=3)
village.col=c("#EBCC2A","chocolate","chocolate","#3B9AB2","#F21A00","chocolate","chocolate","chocolate","#78B7C5","orange","chocolate")

# set up colour palette. The 'NPG' palette (Nature Publising Group) from ggsci will be used for the standard palette
standard_col=c(pal_npg()(10), pal_futurama()(12), pal_tron()(3))
plasmo=brewer.pal(3, "Set2")

dev.off()

# load in data files and normalise data -----------------------------------------------------------------------

# merged data
load(paste0(inputdir, "merged.read_counts.TMM.filtered.Rda"))
lcpm=cpm(merged, log=T)
norm_data = normalize.quantiles(as.matrix(lcpm))
rownames(norm_data)=merged$genes$SYMBOL
colnames(norm_data)=colnames(lcpm)

# DE analysis Species --------------------------------------------------------------------------------

# Set up design matrix
design <- model.matrix(~0 + merged$samples$diseaseStatus.species + merged$samples$Island + merged$samples$EEF_falciparum + merged$samples$Merozoite_falciparum + merged$samples$Ring_falciparum + merged$samples$Trophozoite_falciparum + merged$samples$Gran + merged$samples$Bcell + merged$samples$CD4T + merged$samples$CD8T + merged$samples$NK + merged$samples$Mono + merged$samples$fract.unmapped.reads)

# rename columns to exclude spaces and unrecognised characters
colnames(design)=gsub("\\$", "", colnames(design)) %>% gsub("mergedsamplesdiseaseStatus.species", "", .) %>% gsub("mergedsamplesstudy", "", .) %>% gsub("-", "", .) %>% gsub("mergedsamples", "", .) %>% gsub("Island", "", .) %>% gsub("West Papua", "Mappi", .) 

# set up contrast matrix
contr.matrix <- makeContrasts(SickvsHealthy=(vivax + falciparum)/2 - healthy, SickvsSickothr=(vivax + falciparum)/2 - other.sick, HealthyvsSickothr=healthy - other.sick, levels=colnames(design))

# Using duplicate correlation and blocking -----------------------------------------------------

# define sample names
samplenames <- colnames(merged)
samplenames[which(merged$samples$study=="indo")] <- sub("([A-Z]{3})([0-9]{3})", "\\1-\\2", samplenames[which(merged$samples$study=="indo")])
samplenames <- sapply(strsplit(samplenames, "[_.]"), `[`, 1)

# create a new variable for blocking using sample IDs
merged$samples$ind <- samplenames

# Estimate the correlation between the replicates.
# Information is borrowed by constraining the within-block corre-lations to be equal between genes and by using empirical Bayes methods to moderate the standarddeviations between genes 
dupcor <- duplicateCorrelation(norm_data, design, block=merged$samples$ind)
# The value dupcor$consensus estimates the average correlation within the blocks and should be positive
dupcor$consensus
# 0.8429341

# With duplicate correction and blocking:
# the inter-subject correlation is input into the linear model fit
lfit <- lmFit(norm_data, design, block=merged$samples$ind, correlation=dupcor$consensus)
lfit <- contrasts.fit(lfit, contrasts=contr.matrix)
efit <- eBayes(lfit, robust=T)

pdf(paste0(outputdir,"MeanVarianceTrend_MergedData_diseaseStatusSpecies_Island.pdf"))
plotSA(efit, main="Mean-variance trend elimination with duplicate correction")
dev.off()

# save efit object
save(efit, file = paste0(outputdir, "efit_diseaseStatusSpecies_Island_dupcor.Rda"))

# explore different pvalue thresholds
dt <- decideTests(efit, p.value=0.05, lfc=0)
summary(dt)

#             SickvsHealthy SickvsSickothr HealthyvsSickothr
# Down            1479           3701              4416
# NotSig          8811           4478              2527
# Up              1496           3607              4843

dt <- decideTests(efit, p.value=0.01, lfc=0)
summary(dt)

#             SickvsHealthy SickvsSickothr HealthyvsSickothr
# Down             779           3133              4060
# NotSig         10163           5705              3299
# Up               844           2948              4427

# get top genes using toptable
topTable.SickvsHealthy <- topTable(efit, coef=1, p.value=0.01, n=Inf, sort.by="p")
write.table(topTable.SickvsHealthy, file=paste0(outputdir,"topTable_mergedSamples_diseaseStatusSpecies_island_allSickSamples_dupcor_SickvsHealthy.txt"))
topTable.SickvsSickothr <- topTable(efit, coef=2, p.value=0.01, n=Inf, sort.by="p")
write.table(topTable.SickvsSickothr, file=paste0(outputdir,"topTable_mergedSamples_diseaseStatusSpecies_island_allSickSamples_dupcor_SickvsSickothr.txt"))
topTable.HealthyvsSickothr <- topTable(efit, coef=3, p.value=0.01, n=Inf, sort.by="p")
write.table(topTable.HealthyvsSickothr, file=paste0(outputdir,"topTable_mergedSamples_diseaseStatusSpecies_island_allSickSamples_dupcor_HealthyvsSickothr.txt"))

# QC --------------------------------------------------------------------------------------

# check to see p-value distribution is normal
pdf(paste0(outputdir,"PvalueDist_NotAdjusted_dupCor_diseaseStatusSpecies_Island.pdf"), height=15, width=10)
par(mfrow=c(3,1))
for (i in 1:ncol(efit)){
    hist(efit$p.value[,i], main=colnames(efit)[i], ylim=c(0,max(table(round(efit$p.value[,i], 1)))+1000), xlab="p-value")
}
dev.off()

# check p-value distribution for adjusted p-values
pdf(paste0(outputdir,"PvalueDist_Adjusted_dupCor_diseaseStatusSpecies_Island.pdf"), height=15, width=10)
par(mfrow=c(3,1))
for (i in 1:ncol(efit)){
    topTable <- topTable(efit, coef=i, n=Inf)
    histData <- hist(topTable$adj.P.Val, main=colnames(efit)[i], xlab="p-value")
    hist(topTable$adj.P.Val, main=colnames(efit)[i], ylim=c(0,max(histData$counts)+1000), xlab="p-value")
}
dev.off()

# Verify that control housekeeping genes are not significantly DE. Set up list of housekeeping genes as controls (from Eisenberg and Levanon, 2003)
housekeeping=read.table(paste0(housekeepingdir,"Housekeeping_ControlGenes.txt"), as.is=T, header=F)
housekeeping=housekeeping$V1

# Volcano plot with points of housekeeping genes
pdf(paste0(outputdir,"VolcanoPlots_dupCorEfit_diseaseStatusSpecies_Island.pdf"), height=15, width=10)
par(mfrow=c(3,1))
for (i in 1:ncol(efit)){
    plot(efit$coef[,i], -log10(as.matrix(efit$p.value)[,i]), pch=20, main=colnames(efit)[i], xlab="log2FoldChange", ylab="-log10(pvalue)")
    points(efit$coef[,i][which(names(efit$coef[,i]) %in% housekeeping)], -log10(as.matrix(efit$p.value)[,i][which(names(efit$coef[,i]) %in% housekeeping)]) , pch=20, col=1, xlab="log2FoldChange", ylab="-log10(pvalue)")
    legend("topleft", "genes", "hk genes",fill=4)
    abline(v=c(-1,1))
}
dev.off()

# PCA visualisation after correction and association with covariates ------------------------------------------------------------

# let's also visualise how our PCAs look after limma correction by using removeBatcheffect. Help on design of removeBatcheffects was given by the lovely John Blischak.
design <- model.matrix(~0 + merged$samples$diseaseStatus.species)
# rename columns to exclude spaces and unrecognised characters
colnames(design)=gsub("\\$", "", colnames(design)) %>% gsub("mergedsamplesspecies", "", .)
batch.corrected.norm_data <- removeBatchEffect(norm_data, batch=merged$samples$Island, covariates=cbind(merged$samples$EEF_falciparum, merged$samples$Merozoite_falciparum, merged$samples$Ring_falciparum, merged$samples$Trophozoite_falciparum, merged$samples$Gran, merged$samples$Bcell, merged$samples$CD4T, merged$samples$CD8T, merged$samples$NK, merged$samples$Mono, merged$samples$fract.unmapped.reads), design=design)

# assign covariate names
# subtract variables we don't need
subtract=c("group", "norm.factors", "ind")
# get index of unwanted variables
subtract=which(colnames(merged$samples) %in% subtract)
covariate.names = colnames(merged$samples)[-subtract]
for (name in covariate.names){
 assign(name, merged$samples[[paste0(name)]])
}

# library size needs to be broken up into chunks for easier visualisation of trends (for instance in Age, we want to see high age vs low age rather than the effect of every single age variable)
assign("lib.size", cut(as.numeric(as.character(merged$samples$lib.size)), breaks=5))

# assign names to covariate names so you can grab individual elements by name
names(covariate.names)=covariate.names

# assign factor variables
factorVariables=c(colnames(Filter(is.factor,merged$samples))[which(colnames(Filter(is.factor,merged$samples)) %in% covariate.names)], "lib.size")
numericVariables=colnames(Filter(is.numeric,merged$samples))[which(colnames(Filter(is.numeric,merged$samples)) %in% covariate.names)] %>% subset(., !(. %in% factorVariables))

# PCA plotting function
plot.pca <- function(dataToPca, speciesCol, namesPch, sampleNames){
    pca <- prcomp(t(dataToPca), scale=T, center=T)
    pca.var <- pca$sdev^2/sum(pca$sdev^2)
    for (i in 1:9){
        pca_axis1=i
        pca_axis2=i+1
        plot(pca$x[,pca_axis1], pca$x[,pca_axis2], col=speciesCol, pch=namesPch, cex=1, xlab=paste0("PC", pca_axis1, " (", round(pca.var[pca_axis1]*100, digits=2), "% of variance)"), ylab=paste0("PC", pca_axis2, " (", round(pca.var[pca_axis2]*100, digits=2), "% of variance)", sep=""), main=name)
        legend(legend=unique(sampleNames), pch=16, x="bottomright", col=unique(speciesCol), cex=1, title=name, border=F, bty="n")
        legend(legend=unique(as.numeric(merged$samples$study)), "topright", pch=unique(as.numeric(merged$samples$study)) + 14, title="Batch", cex=1, border=F, bty="n")
        }

    return(pca)
}

plot.pca.numeric <- function(dataToPca, speciesCol, namesPch, sampleNames){
    pca <- prcomp(t(dataToPca), scale=T, center=T)
    pca.var <- pca$sdev^2/sum(pca$sdev^2)
    for (i in 1:9){
        pca_axis1=i
        pca_axis2=i+1
        plot(pca$x[,pca_axis1], pca$x[,pca_axis2], col=speciesCol, pch=namesPch, cex=1, xlab=paste0("PC", pca_axis1, " (", round(pca.var[pca_axis1]*100, digits=2), "% of variance)"), ylab=paste0("PC", pca_axis2, " (", round(pca.var[pca_axis2]*100, digits=2), "% of variance)", sep=""), main=name)
        legend(legend=c("high","low"), pch=16, x="bottomright", col=c(speciesCol[which.max(sampleNames)],speciesCol[which.min(sampleNames)]), cex=1, title=name, border=F, bty="n")
        legend(legend=unique(as.numeric(merged$samples$study)), "topright", pch=unique(as.numeric(merged$samples$study)) + 14, title="Batch", cex=1, border=F, bty="n")
        }

    return(pca)
}

# PCA association function
pc.assoc <- function(pca.data){
    all.pcs <- data.frame()
    for (i in 1:ncol(pca.data$x)){
        all.assoc <- vector()
        for (j in 1:ncol(all.covars.df)){
            test.assoc <- anova(lm(pca.data$x[,i] ~ all.covars.df[,j]))[1,5]
            all.assoc <- c(all.assoc, test.assoc)
        }
        single.pc <- c(i, all.assoc)
        all.pcs <- rbind(all.pcs, single.pc)
    }
    names(all.pcs) <- c("PC", colnames(all.covars.df))

    print ("Here are the relationships between PCs and some possible covariates")
    print (all.pcs)
    return (all.pcs)
}

# get rid of covariates we aren't interested in
covariate.names=covariate.names[grep("lib.size",covariate.names, invert=T)]
# Prepare covariate matrix
all.covars.df <- merged$samples[,covariate.names]

# Plot PCA
for (name in factorVariables){
    pdf(paste0(outputdir,"removeBatchEffect_pcaresults_diseaseStatusSpecies_Island_",name,".pdf"))
    pcaresults <- plot.pca(dataToPca=batch.corrected.norm_data, speciesCol=as.numeric(get(name)),namesPch=as.numeric(merged$samples$study) + 14,sampleNames=get(name))
    dev.off()
}

# plot numeric variables
for (name in numericVariables){
    initial = .bincode(get(name), breaks=seq(min(get(name), na.rm=T), max(get(name), na.rm=T), len = 80),include.lowest = TRUE)
    bloodCol <- colorRampPalette(c("blue", "red"))(79)[initial]
    pdf(paste0(outputdir,"removeBatchEffect_pcaresults_diseaseStatusSpecies_Island_",name,".pdf"))
    pcaresults <- plot.pca(dataToPca=batch.corrected.norm_data, speciesCol=bloodCol,namesPch=as.numeric(merged$samples$study) + 14,sampleNames=get(name))
    #legend(legend=c("High","Low"), pch=16, x="bottomright", col=c(bloodCol[which.max(get(name))], bloodCol[which.min(get(name))]), cex=0.6, title=name, border=F, bty="n")
    #legend(legend=unique(as.numeric(merged$samples$batch)), "topright", pch=unique(as.numeric(merged$samples$batch)) + 14, title="Batch", cex=0.6, border=F, bty="n")
    dev.off()
}

# Get PCA associations
all.pcs <- pc.assoc(pcaresults)
all.pcs$Variance <- pcaresults$sdev^2/sum(pcaresults$sdev^2)

# plot pca covariates association matrix to illustrate any potential confounding and evidence for batches
pdf(paste0(outputdir,"removeBatchEffect_significantCovariates_AnovaHeatmap_diseaseStatusSpecies_Island.pdf"))
pheatmap(log(all.pcs[1:5,covariate.names]), cluster_col=F, col= colorRampPalette(brewer.pal(11, "RdYlBu"))(100), cluster_rows=F, main="Significant Covariates \n Anova")
dev.off()

# Write out the covariates:
write.table(all.pcs, file=paste0(outputdir,"pca_covariates_significanceLevels_diseaseStatusSpecies_Island.txt"), col.names=T, row.names=F, quote=F, sep="\t")

# Summary and visualisation of gene trends ---------------------------------------------------------------------------

dt <- decideTests(efit, p.value=0.01, lfc=0)

# Compare groups with venn diagrams
pdf(paste0(outputdir,"vennDiagram_allSigDEGenes_pval01_dupCor.pdf"))
ssvFeatureVenn(dt, group_names = NULL, counts_txt_size = 5,
  counts_as_labels = FALSE, show_outside_count = TRUE,
  line_width = 1, circle_colors = NULL, fill_alpha = 0.3,
  line_alpha = 1, counts_color = NULL, n_points = 200,
  return_data = FALSE)
dev.off()

# malaria-specic genes
malaria_genes=dt[which(dt[,1]!=0 & dt[,2]!=0 & dt[,3]==0),]
write.table(malaria_genes, file=paste0(outputdir,"malariaSpecificGenes.txt"))
# non-malaria sick genes
otherSick_genes=dt[which(dt[,2]!=0 & dt[,3]!=0 & dt[,1]==0),]
write.table(otherSick_genes, file=paste0(outputdir,"nonMalariaGenes_allOtherSick.txt"))
# all genes expressed in healthy vs sick
allSickVshealthy_genes=dt[which(dt[,1]!=0 & dt[,3]!=0 & dt[,2]==0),]
write.table(allSickVshealthy_genes, file=paste0(outputdir,"allgenes_SickVsHealthy.txt"))
# all common genes
all.genes=dt[which(dt[,1]!=0 & dt[,2]!=0 & dt[,3]!=0),]
write.table(all.genes, file=paste0(outputdir,"AllGenesInCommon.txt"))

# Volcano plots of top genes --------------------------------------------------

topTable.SickvsHealthy <- topTable(efit, coef=1, p.value=1, n=Inf, sort.by="p")
pdf(paste0(outputdir,"volcanoPlot_topGenes_MalariaVsHealthControls.pdf"))
EnhancedVolcano(topTable.SickvsHealthy, lab = topTable.SickvsHealthy$ID, x = 'logFC', y = 'adj.P.Val', title = "Malaria vs Healthy Controls")
dev.off()

topTable.SickvsSickothr <- topTable(efit, coef=2, p.value=1, n=Inf, sort.by="p")
pdf(paste0(outputdir,"volcanoPlot_topGenes_SickvsSickothr.pdf"))
EnhancedVolcano(topTable.SickvsSickothr, lab = topTable.SickvsSickothr$ID, x = 'logFC', y = 'adj.P.Val', title = "Malaria vs Patients with Other Diseases")
dev.off()

topTable.HealthyvsSickothr <- topTable(efit, coef=3, p.value=1, n=Inf, sort.by="p")
pdf(paste0(outputdir,"volcanoPlot_topGenes_HealthyvsSickothr.pdf"))
EnhancedVolcano(topTable.HealthyvsSickothr, lab = topTable.HealthyvsSickothr$ID, x = 'logFC', y = 'adj.P.Val', title = "Healthy Controls vs Patients with Other Diseases")
dev.off()

# plot all three volcano plots together
pdf(paste0(outputdir,"volcanoPlot_All2.pdf"), width=9, height=9)
ggarrange(EnhancedVolcano(topTable.SickvsHealthy, lab = topTable.SickvsHealthy$ID, xlim=c(-4,6), x = 'logFC', y = 'adj.P.Val', title = "Malaria vs Controls", transcriptLabSize = 5), EnhancedVolcano(topTable.SickvsSickothr, lab = topTable.SickvsSickothr$ID, xlim=c(-4,4), x = 'logFC', y = 'adj.P.Val', title = "Malaria vs Other Sick", transcriptLabSize = 5), EnhancedVolcano(topTable.HealthyvsSickothr, lab = topTable.HealthyvsSickothr$ID, x = 'logFC', y = 'adj.P.Val', xlim=c(-8,5), title = "Controls vs Other Sick", transcriptLabSize = 5), labels = c("A", "B", "C"), ncol = 2, nrow = 2)
dev.off()

# top ranked genes -----------------------------------------------------------------------------------------

# # Let's see how the expression levels of all of the significantly DE genes in population comparisons with Mappi are distributed within each island. First, assign our top genes and ensembl IDs to variables
edb <- EnsDb.Hsapiens.v86
topTable <- topTable(efit, coef=1, p.value=0.01, lfc=0, n=Inf, sort.by="p")
topGenes <- topTable$ID[1:10]

# To visualise distributions, we'll be making violin plots using ggpubr which needs p-value labels. Let's go ahead and make a matrix to input this into ggpubr
# first set up matrix
topGenes.pvalue=matrix(nrow=length(topGenes), ncol=ncol(efit))
rownames(topGenes.pvalue)=topGenes
colnames(topGenes.pvalue)=colnames(efit)
# get significant genes over a logFC of 1 for all Island comparisons
topTable <- topTable(efit, coef=1, n=Inf, lfc=0)
for(j in topGenes){
        # input the adjusted p.value for each gene
        topGenes.pvalue[j,]=topTable[grep(paste0("\\",j,"\\b"), topTable$ID),"adj.P.Val"]
}

# make pvalues into scientific notation with max 3 digits
topGenes.pvalue=formatC(topGenes.pvalue, format="e", digits=2, drop0trailing=T)
# convert e notation to base 10 notation
topGenes.pvalue=sub("e", "x10^", topGenes.pvalue)

# We can make the violin plots using ggpubr
pdf(paste0(outputdir,"TopGenes_ggboxplot_Island.pdf"), height=8, width=10)
counter=0
for(gene in topGenes){
    counter=counter+1
    # gene.df <- data.frame(norm_data[gene,], diseaseStatus)
    gene.df <- data.frame(batch.corrected.norm_data[gene,], diseaseStatus)
    colnames(gene.df)=c("CPM", "diseaseStatus")
    #annotation_df <- data.frame(start=c("malaria"), end=c("control"), y=c(max(gene.df[,1]+5),max(gene.df[,1]+5)), label=paste("limma p-value =",topGenes.pvalue[ensembl,],sep=" "))
    #print(ggviolin(gene.df, x = "diseaseStatus", y = "CPM", fill="diseaseStatus", add=c("jitter","boxplot"), main=topGenes[counter], palette=plasmo[1:2], add.params = c(list(fill = "white"), list(width=0.05))) + textsize = 5, vjust = -0.2,manual=TRUE) + ylim(NA, max(gene.df[,1])+7))
    #print(ggviolin(gene.df, x = "diseaseStatus", y = "CPM", fill="diseaseStatus", add="jitter","boxplot"), shape=study, main=topGenes[counter], palette=standard_col[1:3], add.params = c(list(fill = "white"),list(width=0.05))))
    print(ggviolin(gene.df, x = "diseaseStatus", y = "CPM", add="jitter",shape=study, main=topGenes[counter], color="diseaseStatus", palette=standard_col[1:3]))
}
dev.off()

# heatmap of top genes
col_fun = colorRamp2(c(-4, 0, 4), c("blue", "white", "red"))
df1=data.frame(DiseaseStatus = as.character(diseaseStatus))
ha1 = HeatmapAnnotation(df = df1, col = list(DiseaseStatus = c("sick" =  standard_col[1], "healthy" = standard_col[2], "other.sick" = standard_col[3])))
topTable.SickvsHealthy <- topTable(efit, coef=1, p.value=1, n=Inf, sort.by="p")
index <- which(rownames(batch.corrected.norm_data) %in% topTable.SickvsHealthy$ID[1:10])
transformedHeatmap=t(scale(t(batch.corrected.norm_data[index,])))
pdf(paste0(outputdir,"topTenGens_removeBatcheffectCorrected_Heatmap.pdf"), height=8, width=10)
draw(Heatmap(transformedHeatmap, col=col_fun, column_title = colnames(efit)[1], top_annotation = ha1, show_row_names = T, show_heatmap_legend = T, show_column_names = F, name = "Z-Score"),show_annotation_legend = T,newpage=F)
dev.off()

# Genes associated with malaria resistance ----------------------------

# plot the distribution of all genes involved in genetic resistance to malaria
# from paper: https://www.sciencedirect.com/science/article/pii/S0002929707629097
# resistance genes: SLC4A1, G6PD, HP, ICAM1, PECAM1, CR1, FCGR2A, HLA-B, HLA-DR, IFNAR1, IFNG, IFNGR1, IL1A/IL1B, IL10, IL12B, IL4, MBL2, NOS2A, TNF, TNFSF5

# set all genes implicated in genetic resistance to malaria as a variable
immune_genes = c("\\bSLC4A1\\b", "\\bG6PD\\b", "\\bHP\\b", "\\bICAM1\\b", "\\bPECAM1\\b", "\\bCR1\\b", "\\bFCGR2A\\b", "\\bHLA-B\\b", "\\bHLA-DR\\b", "\\bIFNAR1\\b", "\\bIFNG\\b", "\\bIFNGR1\\b", "\\bIL1A\\b", "\\bIL1B\\b", "\\bIL10\\b", "\\bIL12B\\b", "\\bIL4\\b", "\\bMBL2\\b", "\\bNOS2A\\b", "\\bTNF\\b", "\\bTNFSF5\\b")
# see which resistance genes are in the merged DGE list object
immune_genes = merged$genes$SYMBOL[grep(paste0(immune_genes, collapse="|"), merged$genes$SYMBOL)]
immune_list=efit$coefficients[which(rownames(efit) %in% immune_genes),]

pdf(paste0(outputdir,"AllMalariaGenesExpression_Heatmap.pdf"), height=8, width=10)
Heatmap(immune_list)
dev.off()

# Three genes, "HP", "ICAM1", and "IFNG", are found to be DE in sick vs healthy samples and are implicated in resistance to malaria.
# Let's look at the distribution of these genes
pdf(paste0(outputdir,"MalariaGenes_standalone.pdf"), height=8, width=10)
counter=0
for(gene in c("HP", "ICAM1", "IFNG")){
    counter=counter+1
    gene.df <- data.frame(batch.corrected.norm_data[gene,], diseaseStatus)
    colnames(gene.df)=c("CPM", "diseaseStatus")
    assign(gene, print(ggviolin(gene.df, x = "diseaseStatus", y = "CPM", add="jitter",shape=study, main=gene, color="diseaseStatus", palette=standard_col[1:3])))
}
dev.off()

pdf(paste0(outputdir,"MalariaGenes_together.pdf"), height=8, width=10)
ggarrange(HP, ICAM1, IFNG, labels = c("A", "B", "C"), ncol = 2, nrow = 2)
dev.off()
