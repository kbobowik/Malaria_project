# script created by KSB, 08.08.18
# Perform DE analysing relationship between islands

### Last edit: KB 03.04.2019

# Load dependencies and set input paths --------------------------

# Load dependencies:
library(edgeR)
library(plyr)
library(NineteenEightyR)
library(RColorBrewer)
library(biomaRt)
library(ggpubr)
library(ggplot2)
library(ggsignif)
library(pheatmap)
library(viridis)
library(gplots)
library(circlize)
library(ComplexHeatmap)
library(EnsDb.Hsapiens.v86)
library(ggsci)
library(scales)
library(dendextend)
library(reshape2)
library(variancePartition)
library(doParallel)
library(devtools)
library(Biobase)
library(preprocessCore)
library(magrittr)
library(EnhancedVolcano)
library(seqsetvis)

# Set paths:
inputdir = "/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/Output/Malaria/MergedData/"
housekeepingdir="/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/BatchEffects/"

# Set output directory and create it if it does not exist:
outputdir <- "/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/Output/Malaria/MergedData/TMMvsQNorm/"

if (file.exists(outputdir) == FALSE){
    dir.create(outputdir)
}

# Load colour schemes:
wes=c("#3B9AB2", "#EBCC2A", "#F21A00", "#00A08A", "#ABDDDE", "#000000", "#FD6467","#5B1A18")
palette(c(wes, brewer.pal(8,"Dark2")))
# set up colour palette for batch
batch.col=electronic_night(n=3)
village.col=c("#EBCC2A","chocolate","chocolate","#3B9AB2","#F21A00","chocolate","chocolate","chocolate","#78B7C5","orange","chocolate")
study.col=viridis(n=2, alpha=0.8)

# set up colour palette. The 'NPG' palette (Nature Publising Group) from ggsci will be used for the standard palette
standard_col=c(pal_npg()(10), pal_futurama()(12), pal_tron()(3))
plasmo=brewer.pal(3, "Set2")

dev.off()

# load in data files -----------------------------------------------------------------------

# merged data
load(paste0(inputdir, "merged.read_counts.TMM.filtered.Rda"))

# calculate log cpm
lcpm=cpm(merged, log=T)

# DE analysis --------------------------------------------------------------------------------

# Set up design matrix
design <- model.matrix(~0 + merged$samples$species + merged$samples$Island + merged$samples$sex + merged$samples$EEF_falciparum + merged$samples$Merozoite_falciparum + merged$samples$oocyst_falciparum + merged$samples$Ring_falciparum + merged$samples$ookoo_falciparum + merged$samples$Trophozoite_falciparum + merged$samples$bbSpz_vivax + merged$samples$Female_vivax + merged$samples$Male_vivax + merged$samples$Merozoite_vivax + merged$samples$oocyst_vivax + merged$samples$ook_vivax  + merged$samples$Ring_vivax + merged$samples$ookoo_vivax + merged$samples$Schizont_vivax + merged$samples$sgSpz_vivax + merged$samples$Trophozoite_vivax + merged$samples$Gran + merged$samples$Bcell + merged$samples$CD4T + merged$samples$CD8T + merged$samples$NK + merged$samples$Mono)

# rename columns to exclude spaces and unrecognised characters
colnames(design)=gsub("\\$", "", colnames(design)) %>% gsub("mergedsamplesspecies", "", .) %>% gsub("mergedsamplesbatch", "", .) %>% gsub("-", "", .) %>% gsub("mergedsamples", "", .) %>% gsub("study", "", .) %>% gsub("West Papua", "Mappi", .) 
# set up contrast matrix
contr.matrix <- makeContrasts(SickvsHealthy=(vivax + falciparum)/2 - control, FalcvsHealthy=falciparum - control, VivaxvsHealthy=vivax - control, VivaxvsFalciparum=vivax - falciparum, levels=colnames(design))

# Using duplicate correlation and blocking -----------------------------------------------------

# First, we need to perform voom normalisation
v <- voom(merged, design, plot=T, normalize="quantile")

# create a new variable for blocking using sample IDs
# define sample names
samplenames <- colnames(merged)
samplenames[which(merged$samples$study=="indo")] <- sub("([A-Z]{3})([0-9]{3})", "\\1-\\2", samplenames[which(merged$samples$study=="indo")])
samplenames <- sapply(strsplit(samplenames, "[_.]"), `[`, 1)

merged$samples$ind <- samplenames

# Estimate the correlation between the replicates.
# Information is borrowed by constraining the within-block corre-lations to be equal between genes and by using empirical Bayes methods to moderate the standarddeviations between genes 
dupcor <- duplicateCorrelation(v, design, block=merged$samples$ind)
# The value dupcor$consensus estimates the average correlation within the blocks and should be positive
dupcor$consensus
#  0.7999426
median(v$weights)
# 6.225499

# run voom a second time with the blocking variable and estimated correlation
# The  vector y$samples$ind indicates the  two  blocks  corresponding  to  biological  replicates
pdf(paste0(outputdir,"Limma_voomDuplicateCorrelation_QuantileNormalisation.pdf"), height=8, width=12)
par(mfrow=c(1,2))
vDup <- voom(merged, design, plot=TRUE, block=merged$samples$ind, correlation=dupcor$consensus, normalize="quantile")
dupcor <- duplicateCorrelation(vDup, design, block=merged$samples$ind) # get warning message: Too much damping - convergence tolerance not achievable
dupcor$consensus
# 0.7996647
median(vDup$weights)
# 6.099316

# With duplicate correction and blocking:
# the inter-subject correlation is input into the linear model fit
voomDupVfit <- lmFit(vDup, design, block=merged$samples$ind, correlation=dupcor$consensus)
voomDupVfit <- contrasts.fit(voomDupVfit, contrasts=contr.matrix)
voomDupEfit <- eBayes(voomDupVfit, robust=T)

plotSA(voomDupEfit, main="Mean-variance trend elimination with duplicate correction")
dev.off()

# save voom and efit object
save(vDup, file = paste0(outputdir, "vDup_QuantileNorm.Rda"))
save(voomDupEfit, file = paste0(outputdir, "voomDupEfit_QuantileNorm.Rda"))

# explore different pvalue thresholds
dt <- decideTests(voomDupEfit, p.value=0.05, lfc=0)
summary(dt)

#        SickvsHealthy FalcvsHealthy VivaxvsHealthy VivaxvsFalciparum
# Down             990           301            596                 0
# NotSig          9556         10993          10288             11727
# Up              1189           441            851                 8

dt <- decideTests(voomDupEfit, p.value=0.01, lfc=0)
summary(dt)

#        SickvsHealthy FalcvsHealthy VivaxvsHealthy VivaxvsFalciparum
# Down             337            74            161                 0
# NotSig         10851         11514          11327             11735
# Up               547           147            247                 0

dt <- decideTests(voomDupEfit, p.value=0.05, lfc=1)
summary(dt)
#        SickvsHealthy FalcvsHealthy VivaxvsHealthy VivaxvsFalciparum
# Down             265           171            306                 0
# NotSig         11160         11349          10979             11729
# Up               310           215            450                 6

dt <- decideTests(voomDupEfit, p.value=0.01, lfc=1)
summary(dt)

#        SickvsHealthy FalcvsHealthy VivaxvsHealthy VivaxvsFalciparum
# Down             151            62            118                 0
# NotSig         11350         11561          11416             11735
# Up               234           112            201                 0

# get top genes using toptable
topTable.SickvsHealthy <- topTable(voomDupEfit, coef=1, p.value=0.05, n=Inf, lfc=0, sort.by="logFC")
# write.table(topTable.SickvsHealthy, file=paste0(outputdir,"topTable_mergedSamples_diseaseStatusSpecies_island_dupcor_SickvsHealthy.txt"))
topTable.FalcvsHealthy <- topTable(voomDupEfit, coef=2, p.value=0.05, n=Inf, lfc=0, sort.by="p")
# write.table(topTable.FalcvsHealthy, file=paste0(outputdir,"topTable_mergedSamples_diseaseStatusSpecies_island_dupcor_FalcvsHealthy.txt"))
topTable.VivaxvsHealthy <- topTable(voomDupEfit, coef=3, p.value=0.05, n=Inf, lfc=0, sort.by="p")
# write.table(topTable.VivaxvsHealthy, file=paste0(outputdir,"topTable_mergedSamples_diseaseStatusSpecies_island_dupcor_VivaxvsHealthy.txt"))
topTable.VivaxvsFalciparum <- topTable(voomDupEfit, coef=4, p.value=0.05, n=Inf, lfc=0, sort.by="p")

# # Trying without normalisation -------------------------------------------------

# pdf(paste0(outputdir,"Limma_voom_TMMNormalisation.pdf"), height=8, width=12)
v_noNorm <- voom(merged, design, plot=T, normalize="none")
dupcor_noNorm <- duplicateCorrelation(v_noNorm, design, block=merged$samples$ind)
vDup_noNorm <- voom(merged, design, plot=TRUE, normalize="none", block=merged$samples$ind, correlation=dupcor$consensus)
dupcor_noNorm <- duplicateCorrelation(vDup_noNorm, design, block=merged$samples$ind) # get warning message: Too much damping - convergence tolerance not achievable

# With duplicate correction and blocking:
# the inter-subject correlation is input into the linear model fit
voomDupVfit_noNorm <- lmFit(vDup_noNorm, design, block=merged$samples$ind, correlation=dupcor$consensus)
voomDupVfit_noNorm <- contrasts.fit(voomDupVfit_noNorm, contrasts=contr.matrix)
voomDupEfit_noNorm <- eBayes(voomDupVfit_noNorm, robust=T)

# save voom and efit object
save(vDup_noNorm, file = paste0(outputdir, "vDup_TMMnoNorm.Rda"))
save(voomDupEfit_noNorm, file = paste0(outputdir, "voomDupEfit_TMMnoNorm.Rda"))

# explore different pvalue thresholds
dt <- decideTests(voomDupEfit_noNorm, p.value=0.05, lfc=0)
summary(dt)

#        SickvsHealthy FalcvsHealthy VivaxvsHealthy VivaxvsFalciparum
# Down             590           179            347                 0
# NotSig          9982         11154          10646             11729
# Up              1163           402            742                 6

dt <- decideTests(voomDupEfit_noNorm, p.value=0.01, lfc=0)
summary(dt)

#        SickvsHealthy FalcvsHealthy VivaxvsHealthy VivaxvsFalciparum
# Down             167            39             66                 0
# NotSig         11109         11585          11533             11734
# Up               459           111            136                 1

dt <- decideTests(voomDupEfit_noNorm, p.value=0.05, lfc=1)
summary(dt)
#        SickvsHealthy FalcvsHealthy VivaxvsHealthy VivaxvsFalciparum
# Down             175           120            191                 0
# NotSig         11174         11388          11039             11730
# Up               386           227            505                 5

dt <- decideTests(voomDupEfit_noNorm, p.value=0.01, lfc=1)
summary(dt)

#        SickvsHealthy FalcvsHealthy VivaxvsHealthy VivaxvsFalciparum
# Down              92            34             54                 0
# NotSig         11381         11612          11553             11734
# Up               262            89            128                 1

# get top genes using toptable
topTable.SickvsHealthy_noNorm <- topTable(voomDupEfit_noNorm, coef=1, p.value=0.05, n=Inf, sort.by="p")
topTable.FalcvsHealthy_noNorm <- topTable(voomDupEfit_noNorm, coef=2, p.value=0.05, n=Inf, lfc=0, sort.by="p")
topTable.VivaxvsHealthy_noNorm <- topTable(voomDupEfit_noNorm, coef=3, p.value=0.05, n=Inf, lfc=0, sort.by="p")
topTable.VivaxvsFalciparum_noNorm <- topTable(voomDupEfit_noNorm, coef=4, p.value=0.05, n=Inf, lfc=0, sort.by="p")

# # Let's check the correlation between those two measurements - sort by gene first, then cor test on adjusted p-value
normVsNoNorm.SickvsHealthy <- join(topTable.SickvsHealthy, topTable.SickvsHealthy_noNorm, by="ENSEMBL")
colnames(normVsNoNorm.SickvsHealthy)[which(duplicated(colnames(normVsNoNorm.SickvsHealthy)))] = paste(colnames(normVsNoNorm.SickvsHealthy)[which(duplicated(colnames(normVsNoNorm.SickvsHealthy)))], "noNorm", sep="_")
cor(normVsNoNorm.SickvsHealthy[,"adj.P.Val"], normVsNoNorm.SickvsHealthy[,"adj.P.Val_noNorm"], method="spearman", use="complete")
# 0.8065753
cor(normVsNoNorm.SickvsHealthy[,"logFC"], normVsNoNorm.SickvsHealthy[,"logFC_noNorm"], method="spearman", use="complete")
# 0.9843219
pdf(paste0(outputdir,"normVsNoNorm.SickvsHealthy.pdf"))
plot(normVsNoNorm.SickvsHealthy[,"logFC"], normVsNoNorm.SickvsHealthy[,"logFC_noNorm"], main=paste("normVsNoNorm.SickvsHealthy", round(cor(normVsNoNorm.SickvsHealthy[,"logFC"], normVsNoNorm.SickvsHealthy[,"logFC_noNorm"], method="spearman", use="complete"),2), sep="\n"))
dev.off()

normVsNoNorm.FalcvsHealthy <- join(topTable.FalcvsHealthy, topTable.FalcvsHealthy_noNorm, by="ENSEMBL")
colnames(normVsNoNorm.FalcvsHealthy)[which(duplicated(colnames(normVsNoNorm.FalcvsHealthy)))] = paste(colnames(normVsNoNorm.FalcvsHealthy)[which(duplicated(colnames(normVsNoNorm.FalcvsHealthy)))], "noNorm", sep="_")
cor(normVsNoNorm.FalcvsHealthy[,"adj.P.Val"], normVsNoNorm.FalcvsHealthy[,"adj.P.Val_noNorm"], method="spearman", use="complete")
# 0.8220955
cor(normVsNoNorm.FalcvsHealthy[,"logFC"], normVsNoNorm.FalcvsHealthy[,"logFC_noNorm"], method="spearman", use="complete")
# 0.9910118
pdf(paste0(outputdir,"normVsNoNorm.FalcvsHealthy.pdf"))
plot(normVsNoNorm.FalcvsHealthy[,"logFC"], normVsNoNorm.FalcvsHealthy[,"logFC_noNorm"], main=paste("normVsNoNorm.FalcvsHealthy",round(cor(normVsNoNorm.FalcvsHealthy[,"logFC"], normVsNoNorm.FalcvsHealthy[,"logFC_noNorm"], method="spearman", use="complete"),2), sep="\n"))
dev.off()

normVsNoNorm.VivaxvsHealthy <- join(topTable.VivaxvsHealthy, topTable.VivaxvsHealthy_noNorm, by="ENSEMBL")
colnames(normVsNoNorm.VivaxvsHealthy)[which(duplicated(colnames(normVsNoNorm.VivaxvsHealthy)))] = paste(colnames(normVsNoNorm.VivaxvsHealthy)[which(duplicated(colnames(normVsNoNorm.VivaxvsHealthy)))], "noNorm", sep="_")
cor(normVsNoNorm.VivaxvsHealthy[,"adj.P.Val"], normVsNoNorm.VivaxvsHealthy[,"adj.P.Val_noNorm"], method="spearman", use="complete")
# 0.8013681
cor(normVsNoNorm.VivaxvsHealthy[,"logFC"], normVsNoNorm.VivaxvsHealthy[,"logFC_noNorm"], method="spearman", use="complete")
# 0.9852843
pdf(paste0(outputdir,"normVsNoNorm.VivaxvsHealthy.pdf"))
plot(normVsNoNorm.VivaxvsHealthy[,"logFC"], normVsNoNorm.VivaxvsHealthy[,"logFC_noNorm"], main=paste("normVsNoNorm.VivaxvsHealthy",round(cor(normVsNoNorm.VivaxvsHealthy[,"logFC"], normVsNoNorm.VivaxvsHealthy[,"logFC_noNorm"], method="spearman", use="complete"),2), sep="\n"))
dev.off()

normVsNoNorm.VivaxvsFalciparum <- join(topTable.VivaxvsFalciparum, topTable.VivaxvsFalciparum_noNorm, by="ENSEMBL")
colnames(normVsNoNorm.VivaxvsFalciparum)[which(duplicated(colnames(normVsNoNorm.VivaxvsFalciparum)))] = paste(colnames(normVsNoNorm.VivaxvsFalciparum)[which(duplicated(colnames(normVsNoNorm.VivaxvsFalciparum)))], "noNorm", sep="_")
cor(normVsNoNorm.VivaxvsFalciparum[,"logFC"], normVsNoNorm.VivaxvsFalciparum[,"logFC_noNorm"], method="spearman", use="complete")
# 0.7714286
pdf(paste0(outputdir,"normVsNoNorm.VivaxvsHealthy.pdf"))
plot(normVsNoNorm.VivaxvsFalciparum[,"logFC"], normVsNoNorm.VivaxvsFalciparum[,"logFC_noNorm"], main=paste("normVsNoNorm.VivaxvsFalciparum",round(cor(normVsNoNorm.VivaxvsFalciparum[,"logFC"], normVsNoNorm.VivaxvsFalciparum[,"logFC_noNorm"], method="spearman", use="complete"),2), sep="\n"))
dev.off()