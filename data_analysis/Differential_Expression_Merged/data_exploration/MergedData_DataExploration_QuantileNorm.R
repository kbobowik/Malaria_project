# script created by KSB

# Load dependencies and set input paths --------------------------

library(edgeR)
library(plyr)
library(RColorBrewer)
library(biomaRt)
library(ggpubr)
library(ggplot2)
library(ggsignif)
library(pheatmap)
library(viridis)
library(gplots)
library(circlize)
library(ComplexHeatmap)
library(EnsDb.Hsapiens.v86)
library(ggsci)
library(scales)
library(dendextend)
library(reshape2)
library(variancePartition)
library(doParallel)
library(preprocessCore)

# Set paths:
inputdir = "/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/Output/Malaria/"

# Set output directory and create it if it does not exist:
outputdir <- "/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/Output/Malaria/MergedData/"

if (file.exists(outputdir) == FALSE){
    dir.create(outputdir)
}

# colour schemes:
study.col=viridis(n=2, alpha=0.8)
# set up colour palette. The 'NPG' palette (Nature Publising Group) from ggsci will be used for the standard palette
standard_col=c(pal_npg()(10), pal_futurama()(12), pal_tron()(3))
plasmo=brewer.pal(3, "Set2")

# load in data files -----------------------------------------------------------------------

# merged data
load(paste0(inputdir, "MergedData/merged.read_counts.TMM.filtered.Rda"))

# load in plasmo files
load(paste0(inputdir,"Plasmodium/Indo/PlasmodiumCounts_unfiltered_Indo_fractMappedReads.Rds"))
load(paste0(inputdir,"Plasmodium/Yamagishi/PlasmodiumCounts_unfiltered_Yamagishi_fractUnmappedReads.Rds"))

# look at ratio of plasmodium to human counts ----------------------------------------------
plasmoCounts=rbind(pfpx$samples[,c("samples","lib.size")],pfpx.yam$samples[,c("samples","lib.size")])
plasmoCounts=plasmoCounts$lib.size[match(rownames(merged$samples),rownames(plasmoCounts))]
plasmoData=plasmoCounts
humanData=merged$samples$lib.size
data=data.frame(plasmoData,humanData)
data=t(data)
colnames(data)=colnames(merged)
rownames(data)=c("plasmo","human")
plasmoData=melt(data)
colnames(plasmoData)=c("species","samples","counts")
pdf(paste0(outputdir,"humanAndPlasmodiumLibrarySizes.pdf"), height=8, width=15)
ggplot(plasmoData, aes(fill=forcats::fct_rev(species), y=counts, x=samples)) + scale_fill_manual(values = plasmo[c(2,1)]) + geom_bar(position="stack", stat="identity") +
theme_bw() + theme(panel.border = element_blank(), panel.grid.major = element_blank(), panel.grid.minor = element_blank(), axis.text.x = element_text(angle=45), legend.title=element_blank()) + 
scale_x_discrete(labels=merged$samples$study) + ggtitle("Human and Plasmodium Library Size")
dev.off()

# plot proportion of cell types for each group
cellType.df=melt(merged$samples[,c("Gran","Bcell","CD4T","CD8T","NK","Mono","diseaseStatus")])
colnames(cellType.df)[c(2,3)]=c("Leukocytes","Percentage")
# ggbarplot(cellType.df, x = "variable", y = "value", color = "diseaseStatus", add = "mean_se", position = position_dodge(0.8))
pdf(paste0(outputdir,"ProportionLeukocytes_DiseaseStatus.pdf"), height=8, width=15)
ggboxplot(cellType.df, x = "Leukocytes", y = "Percentage",color = "diseaseStatus")
dev.off()

# look at plasmodium stages in barplot  ----------------------------------------------

index.falciparum=which(merged$samples$diseaseStatus.species=="falciparum")
falciparum=t(data.frame(merged$samples[,grep("falciparum", colnames(merged$samples))]))[,index.falciparum]
falciparum = melt(falciparum)
colnames(falciparum)=c("stage","samples","proportion")
labels=which(merged$samples$diseaseStatus.species=="falciparum") %>% merged$samples[,grep("study", colnames(merged$samples))][.]
pdf(paste0(outputdir,"PlasmodiumStageEstimate_Falciparum.pdf"), height=8, width=15)
ggplot(falciparum, aes(fill=stage, y=proportion, x=samples)) + geom_bar(position="stack", stat="identity") + 
scale_fill_manual(values = colorRampPalette(brewer.pal(12, "Paired"))(12)) + ggtitle("Falciparum") + 
theme_bw() + theme(panel.border = element_blank(), panel.grid.major = element_blank(), panel.grid.minor = element_blank(), axis.text.x = element_text(angle=45)) + scale_x_discrete(labels=labels)
dev.off()

index.vivax=which(merged$samples$diseaseStatus.species=="vivax")
vivax=t(data.frame(merged$samples[,grep("vivax", colnames(merged$samples))]))[,index.vivax]
# melt into ggplot form
vivax = melt(vivax)
colnames(vivax)=c("stage","samples","proportion")
labels=which(merged$samples$diseaseStatus.species=="vivax") %>% merged$samples[,grep("study", colnames(merged$samples))][.]
pdf(paste0(outputdir,"PlasmodiumStageEstimate_Vivax.pdf"), height=8, width=15)
ggplot(vivax, aes(fill=stage, y=proportion, x=samples)) + geom_bar(position="stack", stat="identity") + 
scale_fill_manual(values = colorRampPalette(brewer.pal(12, "Paired"))(12)) + ggtitle("Vivax") + 
theme_bw() + theme(panel.border = element_blank(), panel.grid.major = element_blank(), panel.grid.minor = element_blank(), axis.text.x = element_text(angle=45)) + scale_x_discrete(labels=labels)
dev.off()

# assign covariates --------------------------------------------

# subtract variables we don't need
subtract=c("group", "norm.factors")
# get index of unwanted variables
subtract=which(colnames(merged$samples) %in% subtract)
covariate.names = colnames(merged$samples)[-subtract]
for (name in covariate.names){
 assign(name, merged$samples[[paste0(name)]])
}

# library size needs to be broken up into chunks for easier visualisation of trends (for instance in Age, we want to see high age vs low age rather than the effect of every single age variable)
assign("lib.size", cut(as.numeric(as.character(merged$samples$lib.size)), breaks=5))

# assign names to covariate names so you can grab individual elements by name
names(covariate.names)=covariate.names

# assign factor variables
factorVariables=c(colnames(Filter(is.factor,merged$samples))[which(colnames(Filter(is.factor,merged$samples)) %in% covariate.names)], "lib.size")
numericVariables=colnames(Filter(is.numeric,merged$samples))[which(colnames(Filter(is.numeric,merged$samples)) %in% covariate.names)] %>% subset(., !(. %in% factorVariables))

# Explore quantile normalisation of the data -----------------------------------------------------------------------

# script adapted from Jeff Leek on the following qunatile normalisation help page: http://jtleek.com/genstats/inst/doc/02_05_normalization.html
# get the log2 counts per million of all samples and plot the distribution
lcpm=cpm(merged, log=T)
colramp = colorRampPalette(c(3,"white",2))(ncol(merged))

pdf(paste0(outputdir,"normaliseQuantiles.pdf"), width=10)
cdev.off()

---
rank = apply(lcpm, 2, function(y) rank(y, ties.method = "average") / (length(y) + 1))
norm_data = qnorm(rank)
rownames(norm_data)=merged$genes$SYMBOL
colnames(norm_data)=colnames(lcpm)
plot(density(norm_data[,1]),col=colramp[1],lwd=3,ylab="log-CPM", main="TMM + Quantile Normalisation")
for(i in 2:ncol(merged)){lines(density(norm_data[,i]),lwd=3,col=colramp[i])}

# qnorm((rank(lcpm,na.last="keep")-0.5)/sum(!is.na(lcpm)))
---

# PCA --------------------

# PCA plotting function
plot.pca <- function(dataToPca, speciesCol, namesPch, sampleNames){
    pca <- prcomp(t(dataToPca), scale=T, center=T)
    pca.var <- pca$sdev^2/sum(pca$sdev^2)
    for (i in 1:9){
        pca_axis1=i
        pca_axis2=i+1
        plot(pca$x[,pca_axis1], pca$x[,pca_axis2], col=speciesCol, pch=namesPch, cex=2, xlab=paste0("PC", pca_axis1, " (", round(pca.var[pca_axis1]*100, digits=2), "% of variance)"), ylab=paste0("PC", pca_axis2, " (", round(pca.var[pca_axis2]*100, digits=2), "% of variance)", sep=""), main=name)
        legend(legend=unique(sampleNames), pch=16, x="bottomleft", col=unique(speciesCol), cex=1, title=name, border=F, bty="n")
        legend(legend=unique(as.numeric(merged$samples$study)), "topleft", pch=unique(as.numeric(merged$samples$study)) + 14, title="Study", cex=1, border=F, bty="n")
        }

    return(pca)
}

plot.pca.numeric <- function(dataToPca, speciesCol, namesPch, sampleNames){
    pca <- prcomp(t(dataToPca), scale=T, center=T)
    pca.var <- pca$sdev^2/sum(pca$sdev^2)
    for (i in 1:9){
        pca_axis1=i
        pca_axis2=i+1
        plot(pca$x[,pca_axis1], pca$x[,pca_axis2], col=speciesCol, pch=namesPch, cex=2, xlab=paste0("PC", pca_axis1, " (", round(pca.var[pca_axis1]*100, digits=2), "% of variance)"), ylab=paste0("PC", pca_axis2, " (", round(pca.var[pca_axis2]*100, digits=2), "% of variance)", sep=""), main=name)
        legend(legend=c("high","low"), pch=16, x="bottomleft", col=c(speciesCol[which.max(sampleNames)],speciesCol[which.min(sampleNames)]), cex=1, title=name, border=F, bty="n")
        legend(legend=unique(as.numeric(merged$samples$study)), "topleft", pch=unique(as.numeric(merged$samples$study)) + 14, title="Study", cex=1, border=F, bty="n")
        }

    return(pca)
}

# PCA association function
pc.assoc <- function(pca.data){
    all.pcs <- data.frame()
    for (i in 1:ncol(pca.data$x)){
        all.assoc <- vector()
        for (j in 1:ncol(all.covars.df)){
            test.assoc <- anova(lm(pca.data$x[,i] ~ all.covars.df[,j]))[1,5]
            all.assoc <- c(all.assoc, test.assoc)
        }
        single.pc <- c(i, all.assoc)
        all.pcs <- rbind(all.pcs, single.pc)
    }
    names(all.pcs) <- c("PC", colnames(all.covars.df))

    print ("Here are the relationships between PCs and some possible covariates")
    print (all.pcs)
    return (all.pcs)
}

# get rid of covariates we aren't interested in
covariate.names=covariate.names[grep("lib.size",covariate.names, invert=T)]
# Prepare covariate matrix
all.covars.df <- merged$samples[,covariate.names]

# Plot PCA
for (name in factorVariables){
    pdf(paste0(outputdir,"pcaresults_QuantileNorm",name,".pdf"))
    if (name=="species"){
        col=plasmo
    }
    if (name=="study"){
        col=study.col
    } 
    if (name != "species" & name != "study"){
        col=standard_col
    }
    pcaresults <- plot.pca(dataToPca=norm_data, speciesCol=col[as.numeric(get(name))],namesPch=as.numeric(merged$samples$study) + 14,sampleNames=get(name))
    dev.off()
}

# plot numeric variables
for (name in numericVariables){
    initial = .bincode(get(name), breaks=seq(min(get(name), na.rm=T), max(get(name), na.rm=T), len = 80),include.lowest = TRUE)
    bloodCol <- colorRampPalette(c("blue", "red"))(79)[initial]
    pdf(paste0(outputdir,"pcaresults__QuantileNorm",name,".pdf"))
    pcaresults <- plot.pca.numeric(dataToPca=norm_data, speciesCol=bloodCol,namesPch=as.numeric(merged$samples$study) + 14,sampleNames=get(name))
    dev.off()
}

# Get PCA associations
all.pcs <- pc.assoc(pcaresults)
all.pcs$Variance <- pcaresults$sdev^2/sum(pcaresults$sdev^2)

graphics.off()

# plot pca covariates association matrix to illustrate any potential confounding and evidence for batches
pdf(paste0(outputdir,"significantCovariates_AnovaHeatmap_QuantileNorm.pdf"))
pheatmap(log(all.pcs[1:5,covariate.names]), cluster_col=F, col= colorRampPalette(brewer.pal(11, "RdYlBu"))(100), cluster_rows=F, main="Significant Covariates \n Anova")
dev.off()

# Write out the covariates:
write.table(all.pcs, file=paste0(outputdir,"pca_covariates_significanceLevels_QuantileNorm.txt"), col.names=T, row.names=F, quote=F, sep="\t")

# look for sample outliers from PCA
pca.outliers.final=matrix(nrow=0, ncol=3)

for (i in 1:ncol(pcaresults$x)){
    pca.dim=c()
    outlier.sample=c()
    outlier.zscore=c()
    zscore=scale(pcaresults$x[,i])
    outliers=which(abs(zscore) >= 3)
    if (length(outliers) > 0){
        pca.dim=c(pca.dim, i)
        outlier.sample=c(outlier.sample, names(pcaresults$x[,i][outliers]))
        outlier.zscore=c(outlier.zscore, zscore[outliers])
        pca.outliers=matrix(c(rep(pca.dim,length(outlier.sample)), outlier.sample, outlier.zscore), nrow=length(outlier.sample), ncol=3)
        pca.outliers.final=rbind(pca.outliers.final, pca.outliers)
    }
}

# Dissimilarity matrix with euclidean distances
pdf(paste0(outputdir,"SampleDistances_Euclidean_QuantileNorm.pdf"), height=8, width=15)
par(mar=c(6.1,4.1,4.1,2.1))
eucl.distance <- dist(t(norm_data), method = "euclidean")
eucl.cluster <- hclust(eucl.distance, method = "complete")
dend.eucl=as.dendrogram(eucl.cluster)
for(name in numericVariables){
    initial = .bincode(get(name), breaks=seq(min(get(name), na.rm=T), max(get(name), na.rm=T), len = 80),include.lowest = TRUE)
    bloodCol <- colorRampPalette(c("blue", "red"))(79)[initial]
    labels_colors(dend.eucl)=bloodCol[order.dendrogram(dend.eucl)]
    plot(dend.eucl, main=name)
}
for(name in factorVariables){
    labels_colors(dend.eucl)=standard_col[as.numeric(get(name))][order.dendrogram(dend.eucl)]
    plot(dend.eucl, main=name)
}
dev.off()

# Heatmap of quantile normalised lcpm distances
# set up annotation
df1=data.frame(diseaseStatus = as.character(diseaseStatus))
df2=data.frame(study = as.character(study))
ha1 = HeatmapAnnotation(df = df1, col = list(diseaseStatus = c("sick" =  standard_col[1], "healthy" = standard_col[2], "other.sick"=standard_col[3])))
ha2 = rowAnnotation(df = df2, col= list(study=c("indo" = study.col[1], "yamagishi" = study.col[2])))

# plot
pdf(paste0(outputdir,"lcpmCorrelationHeatmaps_QuantileNorm.pdf"), height=10, width=15)
Heatmap(cor(norm_data,method="pearson"), col=magma(100), column_title = "Pearson Correlation \n log2-CPM", name="Corr Coeff", top_annotation = ha1, show_row_names = FALSE, column_names_gp=gpar(fontsize = 8)) + ha2
dev.off()
