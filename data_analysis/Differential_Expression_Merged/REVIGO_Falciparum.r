

# A plotting R script produced by the REVIGO server at http://revigo.irb.hr/
# If you found REVIGO useful in your work, please cite the following reference:
# Supek F et al. "REVIGO summarizes and visualizes long lists of Gene Ontology
# terms" PLoS ONE 2011. doi:10.1371/journal.pone.0021800

outputdir <- "/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/Output/Malaria/MergedData/GOandCamera/"

# --------------------------------------------------------------------------
# If you don't have the ggplot2 package installed, uncomment the following line:
# install.packages( "ggplot2" );
library( ggplot2 );
# --------------------------------------------------------------------------
# If you don't have the scales package installed, uncomment the following line:
# install.packages( "scales" );
library( scales );


# --------------------------------------------------------------------------
# Here is your data from REVIGO. Scroll down for plot configuration options.

revigo.names <- c("term_ID","description","frequency_%","plot_X","plot_Y","plot_size","log10_p_value","uniqueness","dispensability");
revigo.data <- rbind(c("GO:0002376","immune system process", 0.600, 0.400,-0.076, 4.886,-6.0794,0.992,0.000),
c("GO:0006614","SRP-dependent cotranslational protein targeting to membrane", 0.138, 0.167, 5.602, 4.249,-12.2765,0.735,0.000),
c("GO:0006950","response to stress", 4.575, 5.855, 1.981, 5.769,-7.7747,0.875,0.000),
c("GO:0008150","biological_process",100.000, 1.396,-0.572, 7.108,-24.0283,1.000,0.000),
c("GO:0008152","metabolic process",75.387, 1.145,-0.468, 6.986,-15.5143,0.998,0.000),
c("GO:0009987","cellular process",63.780, 0.657,-0.177, 6.913,-24.6478,0.997,0.000),
c("GO:0010166","wax metabolic process", 0.002, 1.377, 2.720, 2.328,-3.3189,0.983,0.000),
c("GO:0023052","signaling", 6.765, 1.495, 2.924, 5.939,-6.9136,0.993,0.000),
c("GO:0048519","negative regulation of biological process", 1.984, 6.064,-6.192, 5.406,-6.7399,0.808,0.000),
c("GO:0050896","response to stimulus",12.210, 1.204, 2.206, 6.195,-8.7799,0.993,0.000),
c("GO:0051179","localization",18.495, 0.062, 0.349, 6.375,-6.5272,0.994,0.000),
c("GO:0065007","biological regulation",20.498, 0.100, 0.006, 6.420,-10.6840,0.994,0.000),
c("GO:0071840","cellular component organization or biogenesis", 8.568, 1.526, 3.133, 6.041,-7.1694,0.993,0.000),
c("GO:0006807","nitrogen compound metabolic process",38.744, 1.159, 2.023, 6.696,-15.5229,0.962,0.012),
c("GO:0006413","translational initiation", 0.518,-3.989,-6.312, 4.823,-9.6799,0.798,0.027),
c("GO:0007154","cell communication", 7.219,-0.595, 0.357, 5.967,-6.7471,0.938,0.041),
c("GO:0017144","drug metabolic process", 0.058, 0.957,-0.207, 3.868,-3.7909,0.941,0.047),
c("GO:0060670","branching involved in labyrinthine layer morphogenesis", 0.002, 2.038,-1.929, 2.435,-3.3766,0.944,0.047),
c("GO:0006091","generation of precursor metabolites and energy", 1.940,-1.678,-0.062, 5.396,-3.3588,0.918,0.065),
c("GO:0009058","biosynthetic process",31.611, 1.099, 1.429, 6.608,-6.8508,0.964,0.066),
c("GO:0008283","cell proliferation", 0.394,-0.247, 0.011, 4.704,-4.8861,0.952,0.067),
c("GO:1901360","organic cyclic compound metabolic process",30.324,-2.316,-2.466, 6.590,-13.2774,0.922,0.069),
c("GO:0071704","organic substance metabolic process",58.357,-0.111, 1.494, 6.874,-15.0851,0.959,0.094),
c("GO:0010025","wax biosynthetic process", 0.002,-2.299,-0.181, 2.314,-3.3189,0.970,0.097),
c("GO:0044237","cellular metabolic process",53.061,-4.492, 1.244, 6.833,-14.5544,0.904,0.119),
c("GO:0000956","nuclear-transcribed mRNA catabolic process", 0.137,-5.532,-4.483, 4.244,-9.5331,0.814,0.119),
c("GO:0044238","primary metabolic process",53.743, 0.491, 2.223, 6.839,-14.3615,0.960,0.120),
c("GO:0008219","cell death", 0.458, 0.306, 1.342, 4.769,-5.1475,0.891,0.156),
c("GO:0034641","cellular nitrogen compound metabolic process",34.137,-6.157,-2.188, 6.641,-15.2741,0.828,0.191),
c("GO:0009449","gamma-aminobutyric acid biosynthetic process", 0.000,-2.699,-0.556, 1.799,-3.3319,0.884,0.193),
c("GO:0006793","phosphorus metabolic process",13.507,-5.677, 0.717, 6.239,-7.2967,0.894,0.193),
c("GO:0007049","cell cycle", 1.885, 1.084, 1.238, 5.384,-3.2817,0.875,0.200),
c("GO:0043170","macromolecule metabolic process",39.491,-3.028,-3.500, 6.705,-12.2984,0.917,0.211),
c("GO:0043687","post-translational protein modification", 0.028,-0.069,-6.665, 3.562,-3.7808,0.881,0.248),
c("GO:0042773","ATP synthesis coupled electron transport", 0.221,-4.023,-2.263, 4.452,-8.9914,0.713,0.248),
c("GO:0006725","cellular aromatic compound metabolic process",29.628,-5.617,-0.396, 6.580,-13.2899,0.878,0.260),
c("GO:0046483","heterocycle metabolic process",29.664,-5.822,-0.307, 6.580,-13.1475,0.878,0.260),
c("GO:0051641","cellular localization", 2.041, 0.377, 4.557, 5.418,-3.5297,0.929,0.263),
c("GO:0051338","regulation of transferase activity", 0.368, 5.398,-5.933, 4.674,-4.8894,0.815,0.265),
c("GO:0002682","regulation of immune system process", 0.252, 5.460,-5.433, 4.510,-4.8827,0.822,0.267),
c("GO:0016071","mRNA metabolic process", 0.798,-5.802,-5.654, 5.010,-6.0915,0.832,0.270),
c("GO:0042127","regulation of cell proliferation", 0.313, 5.783,-5.610, 4.603,-4.4584,0.785,0.273),
c("GO:0042981","regulation of apoptotic process", 0.313, 5.443,-4.804, 4.604,-6.4365,0.708,0.273),
c("GO:0044260","cellular macromolecule metabolic process",34.276,-5.172,-5.882, 6.643,-10.8013,0.818,0.277),
c("GO:0008380","RNA splicing", 0.413,-4.795,-5.091, 4.725,-3.6272,0.830,0.299),
c("GO:0043603","cellular amide metabolic process", 6.879,-6.261,-2.028, 5.946,-7.9469,0.857,0.310),
c("GO:0065009","regulation of molecular function", 1.726, 5.603,-5.705, 5.345,-5.0600,0.818,0.316),
c("GO:0010605","negative regulation of macromolecule metabolic process", 1.169, 3.659,-7.315, 5.176,-6.8761,0.716,0.316),
c("GO:0072521","purine-containing compound metabolic process", 2.673,-5.939,-2.159, 5.535,-4.9747,0.832,0.318),
c("GO:0043412","macromolecule modification", 9.785,-2.140,-6.492, 6.099,-5.0899,0.895,0.331),
c("GO:0048518","positive regulation of biological process", 1.744, 6.040,-6.136, 5.350,-6.4089,0.810,0.332),
c("GO:0071705","nitrogen compound transport", 1.767, 1.086, 5.300, 5.355,-3.2717,0.928,0.347),
c("GO:0055086","nucleobase-containing small molecule metabolic process", 4.917,-6.688,-1.322, 5.800,-4.0841,0.759,0.351),
c("GO:0006396","RNA processing", 3.210,-5.005,-5.617, 5.615,-5.6198,0.796,0.373),
c("GO:0006954","inflammatory response", 0.110, 5.630, 2.074, 4.151,-6.2541,0.897,0.373),
c("GO:0046677","response to antibiotic", 0.128, 5.888, 2.246, 4.215,-4.5243,0.897,0.379),
c("GO:0018108","peptidyl-tyrosine phosphorylation", 0.188,-0.860,-6.473, 4.381,-3.4900,0.798,0.380),
c("GO:0051174","regulation of phosphorus metabolic process", 0.580, 4.326,-5.768, 4.872,-3.7644,0.710,0.390),
c("GO:0019538","protein metabolic process",18.489,-2.870,-7.088, 6.375,-8.4377,0.879,0.408),
c("GO:1901564","organonitrogen compound metabolic process",17.886,-5.675,-3.194, 6.361,-11.0501,0.877,0.415),
c("GO:0010467","gene expression",19.671,-2.751,-7.034, 6.402,-9.5186,0.882,0.417),
c("GO:0002181","cytoplasmic translation", 0.064,-3.786,-6.211, 3.915,-3.9617,0.832,0.429),
c("GO:0048584","positive regulation of response to stimulus", 0.461, 6.733,-2.871, 4.772,-7.7721,0.694,0.436),
c("GO:0032268","regulation of cellular protein metabolic process", 1.473, 2.302,-7.640, 5.277,-6.5452,0.666,0.437),
c("GO:0051246","regulation of protein metabolic process", 1.551, 2.710,-7.599, 5.299,-6.8633,0.724,0.453),
c("GO:0050789","regulation of biological process",19.373, 5.819,-5.913, 6.395,-12.4647,0.759,0.453),
c("GO:0050793","regulation of developmental process", 1.205, 5.921,-5.538, 5.189,-3.2623,0.797,0.457),
c("GO:0015833","peptide transport", 0.298, 0.850, 4.905, 4.582,-4.1186,0.920,0.460),
c("GO:0043933","macromolecular complex subunit organization", 2.371,-1.079, 5.726, 5.483,-7.1469,0.864,0.472),
c("GO:0007166","cell surface receptor signaling pathway", 0.920, 6.423,-3.115, 5.072,-7.7055,0.709,0.475),
c("GO:0006139","nucleobase-containing compound metabolic process",26.547,-6.507,-3.176, 6.532,-12.9355,0.782,0.484),
c("GO:0048583","regulation of response to stimulus", 1.120, 6.717,-3.026, 5.158,-7.7545,0.766,0.487),
c("GO:0006810","transport",17.616, 1.389, 5.772, 6.354,-3.6594,0.908,0.493),
c("GO:0019221","cytokine-mediated signaling pathway", 0.093, 6.679,-2.616, 4.078,-4.4750,0.743,0.495),
c("GO:0009605","response to external stimulus", 1.370, 6.205, 2.406, 5.245,-4.6655,0.888,0.501),
c("GO:0044271","cellular nitrogen compound biosynthetic process",22.502,-6.997,-3.026, 6.460,-7.7352,0.802,0.502),
c("GO:0009059","macromolecule biosynthetic process",19.548,-4.586,-6.843, 6.399,-6.4921,0.851,0.504),
c("GO:1901566","organonitrogen compound biosynthetic process",14.064,-6.622,-3.461, 6.256,-3.9782,0.833,0.506),
c("GO:0006468","protein phosphorylation", 4.137,-2.079,-6.690, 5.725,-5.8570,0.731,0.507),
c("GO:0010646","regulation of cell communication", 0.929, 6.120,-5.649, 5.076,-4.2924,0.767,0.521),
c("GO:0007005","mitochondrion organization", 0.418,-1.060, 5.285, 4.729,-4.2700,0.881,0.528),
c("GO:0034645","cellular macromolecule biosynthetic process",19.291,-4.362,-5.825, 6.394,-6.8153,0.798,0.536),
c("GO:0044267","cellular protein metabolic process",14.293,-3.769,-7.173, 6.263,-9.6326,0.800,0.536),
c("GO:0023051","regulation of signaling", 0.934, 6.275,-5.745, 5.079,-4.1568,0.793,0.536),
c("GO:0016070","RNA metabolic process",15.951,-5.485,-5.434, 6.311,-7.1325,0.757,0.541),
c("GO:0019693","ribose phosphate metabolic process", 3.032,-1.342,-2.339, 5.590,-5.3840,0.788,0.545),
c("GO:1901362","organic cyclic compound biosynthetic process",17.871,-5.908,-3.898, 6.360,-3.3623,0.837,0.545),
c("GO:0019438","aromatic compound biosynthetic process",16.954,-6.906,-3.086, 6.338,-3.1285,0.807,0.552),
c("GO:0018130","heterocycle biosynthetic process",17.388,-6.601,-2.972, 6.348,-3.1587,0.806,0.556),
c("GO:0006935","chemotaxis", 0.475, 5.711, 1.992, 4.785,-3.6799,0.872,0.562),
c("GO:0033554","cellular response to stress", 2.967, 6.041, 2.176, 5.581,-4.6383,0.824,0.570),
c("GO:0044249","cellular biosynthetic process",30.048,-6.880,-2.561, 6.586,-6.9101,0.843,0.585),
c("GO:0046700","heterocycle catabolic process", 1.044,-4.199,-1.267, 5.127,-4.8996,0.842,0.593),
c("GO:1904667","negative regulation of ubiquitin protein ligase activity", 0.004, 3.272,-6.931, 2.740,-3.8906,0.740,0.595),
c("GO:0034622","cellular macromolecular complex assembly", 1.211,-0.993, 5.211, 5.191,-7.8041,0.850,0.597),
c("GO:0090304","nucleic acid metabolic process",21.449,-5.467,-5.032, 6.440,-9.1141,0.755,0.597),
c("GO:0051170","nuclear import", 0.107, 0.516, 4.407, 4.138,-3.4354,0.890,0.610),
c("GO:0016310","phosphorylation", 7.764,-3.040,-2.045, 5.998,-8.5686,0.803,0.615),
c("GO:0032981","mitochondrial respiratory chain complex I assembly", 0.013,-1.120, 5.716, 3.207,-5.2984,0.891,0.618),
c("GO:0090316","positive regulation of intracellular protein transport", 0.054, 5.810,-3.341, 3.839,-3.1496,0.709,0.619),
c("GO:0010257","NADH dehydrogenase complex assembly", 0.014,-0.985, 5.076, 3.242,-5.2984,0.891,0.622),
c("GO:0051090","regulation of sequence-specific DNA binding transcription factor activity", 0.085, 1.223,-7.375, 4.035,-3.3310,0.710,0.629),
c("GO:0035556","intracellular signal transduction", 4.000, 6.585,-3.222, 5.710,-6.5391,0.663,0.641),
c("GO:1902255","positive regulation of intrinsic apoptotic signaling pathway by p53 class mediator", 0.001, 6.907,-1.943, 1.929,-4.8268,0.709,0.646),
c("GO:0019637","organophosphate metabolic process", 6.148,-2.374,-2.224, 5.897,-5.7932,0.802,0.648),
c("GO:0044085","cellular component biogenesis", 4.355,-1.045, 5.910, 5.747,-4.8210,0.902,0.653),
c("GO:1901576","organic substance biosynthetic process",30.365,-5.338,-4.126, 6.591,-6.8601,0.882,0.658),
c("GO:0006412","translation", 5.686,-4.313,-6.095, 5.863,-9.4260,0.733,0.661),
c("GO:0060255","regulation of macromolecule metabolic process",11.716, 2.561,-7.265, 6.177,-9.1433,0.670,0.666),
c("GO:0018212","peptidyl-tyrosine modification", 0.215,-0.978,-7.708, 4.441,-3.4576,0.859,0.667),
c("GO:0019222","regulation of metabolic process",11.942, 5.853,-5.944, 6.185,-9.1192,0.747,0.669),
c("GO:0006952","defense response", 0.568, 5.722, 2.058, 4.863,-3.9433,0.889,0.678),
c("GO:0009057","macromolecule catabolic process", 1.953,-2.007,-5.725, 5.399,-4.8861,0.870,0.679),
c("GO:0051716","cellular response to stimulus", 9.561, 6.236, 2.813, 6.089,-8.0395,0.818,0.680),
c("GO:0022900","electron transport chain", 0.564,-0.273, 1.479, 4.860,-6.8996,0.840,0.685),
c("GO:0034097","response to cytokine", 0.136, 5.909, 2.280, 4.242,-3.9286,0.894,0.688),
c("GO:0042886","amide transport", 0.337, 0.456, 4.831, 4.636,-3.9774,0.934,0.689),
c("GO:0070372","regulation of ERK1 and ERK2 cascade", 0.049, 3.854,-4.721, 3.796,-3.3604,0.587,0.693),
c("GO:1901361","organic cyclic compound catabolic process", 1.164,-3.791,-1.370, 5.174,-4.4145,0.868,0.700));

one.data <- data.frame(revigo.data);
names(one.data) <- revigo.names;
one.data <- one.data [(one.data$plot_X != "null" & one.data$plot_Y != "null"), ];
one.data$plot_X <- as.numeric( as.character(one.data$plot_X) );
one.data$plot_Y <- as.numeric( as.character(one.data$plot_Y) );
one.data$plot_size <- as.numeric( as.character(one.data$plot_size) );
one.data$log10_p_value <- as.numeric( as.character(one.data$log10_p_value) );
one.data$frequency <- as.numeric( as.character(one.data$frequency) );
one.data$uniqueness <- as.numeric( as.character(one.data$uniqueness) );
one.data$dispensability <- as.numeric( as.character(one.data$dispensability) );
#head(one.data);


# --------------------------------------------------------------------------
# Names of the axes, sizes of the numbers and letters, names of the columns,
# etc. can be changed below

p1 <- ggplot( data = one.data );
p1 <- p1 + geom_point( aes( plot_X, plot_Y, colour = log10_p_value, size = plot_size), alpha = I(0.6) ) + scale_size_area();
p1 <- p1 + scale_colour_gradientn( colours = c("blue", "green", "yellow", "red"), limits = c( min(one.data$log10_p_value), 0) );
p1 <- p1 + geom_point( aes(plot_X, plot_Y, size = plot_size), shape = 21, fill = "transparent", colour = I (alpha ("black", 0.6) )) + scale_size_area();
p1 <- p1 + scale_size( range=c(5, 30)) + theme_bw(); # + scale_fill_gradientn(colours = heat_hcl(7), limits = c(-300, 0) );
ex <- one.data [ one.data$dispensability < 0.40, ]; 
ex=ex[-grep("metabolic",ex$description),]
ex = ex[-grep("cellular",ex$description),]
p1 <- p1 + geom_text( data = ex, aes(plot_X, plot_Y, label = description), colour = I(alpha("black", 0.85)), size = 3 );
p1 <- p1 + labs (y = "semantic space x", x = "semantic space y");
p1 <- p1 + theme(legend.key = element_blank()) ;
one.x_range = max(one.data$plot_X) - min(one.data$plot_X);
one.y_range = max(one.data$plot_Y) - min(one.data$plot_Y);
p1 <- p1 + xlim(min(one.data$plot_X)-one.x_range/10,max(one.data$plot_X)+one.x_range/10);
p1 <- p1 + ylim(min(one.data$plot_Y)-one.y_range/10,max(one.data$plot_Y)+one.y_range/10);



# --------------------------------------------------------------------------
# Output the plot to screen

p1;

# Uncomment the line below to also save the plot to a file.
# The file type depends on the extension (default=pdf).

ggsave(paste0(outputdir,"revigo-plot-falciparum.pdf"))
