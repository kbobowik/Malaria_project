# Code developed by Katalina Bobowik, 11.03.2019

# Load dependencies and set input paths --------------------------

# Load dependencies:
library(edgeR)
library(plyr)
library(openxlsx)
library(RColorBrewer)
library(magrittr)
library(reshape2)
library(ggplot2)

# set up colour palette. The "wes" palette will be used for island and other statistical information, whereas NineteenEightyR will be used for batch information
wes=c("#3B9AB2", "#EBCC2A", "#F21A00", "#00A08A", "#ABDDDE", "#000000", "#FD6467","#5B1A18")
palette(c(wes, brewer.pal(8,"Dark2")))
dev.off()

# now set up coulor palette for plasmodium
plasmo=brewer.pal(3, "Set2")

# Set paths:
# inputdir <- "/data/cephfs/punim0586/kbobowik/Sumba/Output/DE_Analysis/123_combined/" # on server
inputdir = "/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/Output/Malaria/Yamagishi-Analysis/dataPreprocessing/"
FeatureCountsDir= "/Users/katalinabobowik/Documents/UniMelb_PhD/Projects/Sumba/FeatureCounts/"
refdir <- "/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/ReferenceFiles/"

# Set output directory and create it if it does not exist:
outputdir <- "/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/Output/Malaria/Plasmodium/Yamagishi/OtherInfectiousDiseases/"

if (file.exists(outputdir) == FALSE){
    dir.create(outputdir)
}

# Read in plasmodium count data --------------------------------------------------------------------

pfpx.yam.sick=list.files(path=paste0(FeatureCountsDir,"PFPX_Combined_Yamagishi/Sick/"), pattern="mapq0and255", full.names=T)
pfpx.yam.controls=list.files(path=paste0(FeatureCountsDir,"PFPX_Combined_Yamagishi/Controls/"), pattern="mapq0and255", full.names=T)
pfpx.yam.other.sick=list.files(path=paste0(FeatureCountsDir,"PFPX_Combined_Yamagishi/"), pattern="mapq0and255", full.names=T)

# read files into a list
featureCountsOut.yam.all <- lapply(c(pfpx.yam.sick,pfpx.yam.controls,pfpx.yam.other.sick), read.delim)

# transform list into a dataframe
plasmoReads.yam <- data.frame(t(ldply(featureCountsOut.yam.all, "[",,3)))

# set col and row names:
namesSick <- paste(sapply(strsplit(basename(pfpx.yam.sick), "[_.]"), `[`, 11), "Sick", sep="_") %>% gsub("Aligned", "", .)
namesControls <- paste(sapply(strsplit(basename(pfpx.yam.controls), "[_.]"), `[`, 11), "Controls", sep="_") %>% gsub("Aligned", "", .)
namesSickOther <- paste(sapply(strsplit(basename(pfpx.yam.other.sick), "[_.]"), `[`, 11), "Sick_Other", sep="_") %>% gsub("Aligned", "", .)

row.names(plasmoReads.yam) <- featureCountsOut.yam.all[[1]]$Geneid
names(plasmoReads.yam) <- c(namesSick, namesControls, namesSickOther) 

# Into DGEList:
pfpx.yam <- DGEList(plasmoReads.yam, genes=rownames(plasmoReads.yam), samples=colnames(plasmoReads.yam))
dim(pfpx.yam)
# [1] 32232   183
# create shortened names
pfpx.names=sapply(1:ncol(pfpx.yam), function(x) strsplit(colnames(pfpx.yam)[x],"_")[[1]][1])

rm(featureCountsOut.yam.all) # clean up, big object
# save unfiltered counts file
save(pfpx.yam, file=paste0(outputdir, "Yamagishi.unfiltered_counts.Rda"))

# read in unmapped reads file -----------------------------------------------------------------------

# read in unmapped reads file
number.unmapped.reads.df.yam=read.table(paste0(refdir,"unmappedReads_Counts_Yamagishi.txt"))
# set column names
colnames(number.unmapped.reads.df.yam)=c("Sample.ID","Unmapped.Reads")
pfpx.unmapped.names=sapply(1:ncol(pfpx.yam), function(x) strsplit(as.character(number.unmapped.reads.df.yam$Sample.ID[x]),"_")[[1]][1])
# Reorder the unmapped reads df
number.unmapped.reads.df.yam=number.unmapped.reads.df.yam[match(pfpx.names, pfpx.unmapped.names),]
#reset pfpx.unmapped.names after reordering
pfpx.unmapped.names=sapply(1:ncol(pfpx.yam), function(x) strsplit(as.character(number.unmapped.reads.df.yam$Sample.ID[x]),"_")[[1]][1])
identical(pfpx.names, pfpx.unmapped.names)
# [1] TRUE

# Where are the falciparum and vivax reads mapping to? ------------------------------------

# now do the same for Yamagishi data
for(species in c("Falciparum", "Vivax")){
  merged_PlasmoCounts=melt(pfpx.yam$counts)
  colnames(merged_PlasmoCounts)=c("gene","sample","value")
  merged_PlasmoCounts_filtered=merged_PlasmoCounts[which(merged_PlasmoCounts[,"value"] != 0),]
  rm(merged_PlasmoCounts)
  # insert species column
  merged_PlasmoCounts_filtered$species=rep(NA,nrow(merged_PlasmoCounts_filtered))
  merged_PlasmoCounts_filtered$species[grep("mal|PF3D7", merged_PlasmoCounts_filtered$gene)]="Falciparum"
  merged_PlasmoCounts_filtered$species[grep("PVP01", merged_PlasmoCounts_filtered$gene)]="Vivax"
  merged_PlasmoCounts_filtered=merged_PlasmoCounts_filtered[merged_PlasmoCounts_filtered$species==species,]
  # order plasmo counts by sample ID, then by the descending value of counts
  ordered_PlasmoCounts=merged_PlasmoCounts_filtered[with(merged_PlasmoCounts_filtered, order(merged_PlasmoCounts_filtered$sample, -merged_PlasmoCounts_filtered$value)), ]
  # add in total number of unmapped reads information in order to normalise
  ordered_PlasmoCounts$unmappedReads="NA"
  rownames(number.unmapped.reads.df.yam)=number.unmapped.reads.df.yam$Sample.ID
  for(sample in number.unmapped.reads.df.yam$Sample.ID){
   ordered_PlasmoCounts$unmappedReads[which(sample == ordered_PlasmoCounts$sample)] = number.unmapped.reads.df.yam[sample,"Unmapped.Reads"]
  }
  ordered_PlasmoCounts$normalisedReads=ordered_PlasmoCounts$value/as.numeric(ordered_PlasmoCounts$unmappedReads)
  # save table
  write.table(ordered_PlasmoCounts, file=paste0(outputdir,"orderedPlamosCounts_Yamagishi.txt"))
  # get top genes for each sample
  for(n in c(1,2,3,5,10)){
    topGenes_Plasmo <- by(ordered_PlasmoCounts, ordered_PlasmoCounts["sample"], head, n=n)
    # transform this back into a dataframe
    assign(species, Reduce(rbind, topGenes_Plasmo))
    pdf(paste0(outputdir,"topGenes_",species,"_Plasmo_Yamagishi",n,".pdf"), height=8, width=20)
    # fraction of reads
    print(ggplot(get(species),aes(x=factor(sample), y=normalisedReads)) + geom_col(aes(fill=factor(get(species)$gene))) + theme(axis.text.x=element_text(angle=90,hjust=1,vjust=0.5)) + xlab("Sample ID") + labs(fill = "gene ID") + ggtitle(paste0("Fraction of reads mapping to P. ", species)))
    # raw read values
    print(ggplot(get(species),aes(x=factor(sample), y=value)) + geom_col(aes(fill=factor(get(species)$gene))) + theme(axis.text.x=element_text(angle=90,hjust=1,vjust=0.5)) + xlab("Sample ID") + labs(fill = "gene ID") + ggtitle(paste0("Number of reads mapping to P. ", species)))
    dev.off()
  }
}

# take out PVP01_0010550, since this maps to a human gene which probably just has poor human annotation
pfpx.yam=pfpx.yam[grep("exon_PVP01_0010550-E1", rownames(pfpx.yam), invert=T),]

# add in the fraction of unmapped reads, which is the total library size for each samples divided by the total number of unmapped reads; i.e., out of all of the unmapped reads, what fraction of maps to plasmodium?
fract.unmapped.reads.pfpx=colSums(pfpx.yam$counts)/number.unmapped.reads.df.yam$Unmapped.Reads

# add this information into the DGE list and save for downstream use
pfpx.yam$samples$fract.unmapped.reads=fract.unmapped.reads.pfpx
# load in human data and get fraction of reads from mapping hg38 data
load(paste0(inputdir,"unfiltered_DGElistObject_allSickSamples.Rda"))
# make column names the same and ensure they're identical in both datasets
colnames(pfpx.yam)=sapply(strsplit(colnames(pfpx.yam), "[_.]"), `[`, 1)
identical(colnames(pfpx.yam), colnames(y))
# TRUE

# add in fraction of HG38 information and save DGE list object
pfpx.yam$samples$fract.hg38.libSize=(pfpx.yam$samples$lib.size/y$samples$lib.size)
save(pfpx.yam, file=paste0(outputdir,"PlasmodiumCounts_unfiltered_Yamagishi_fractUnmappedReads.Rds"))
fract.reads.pfpx.yam=pfpx.yam$samples$lib.size/number.unmapped.reads.df.yam$Unmapped.Reads
# save fraction of unmapped reads information for downstream use
fract.unmapped=data.frame(pfpx.yam$samples$fract.unmapped.reads, pfpx.yam$samples$fract.hg38.libSize)
rownames(fract.unmapped)=colnames(pfpx.yam)
colnames(fract.unmapped)=c("fract.unmapped.reads", "fract.hg38.unmapped")
write.table(fract.unmapped, file=paste0(outputdir, "Yamagishi_FractUnmappedReads.txt"), row.names=T)

# Data visualisation of healthy and sick samples ----------------------------------------------

# plot vivax vs falciparum #
a=sapply(1:ncol(pfpx.yam$counts), function(x)sum(pfpx.yam$counts[,x][grep("PF3D7|mal",names(pfpx.yam$counts[,x]))])/number.unmapped.reads.df.yam$Unmapped.Reads[x])
b=sapply(1:ncol(pfpx.yam$counts), function(x)sum(pfpx.yam$counts[,x][grep("PVP01",names(pfpx.yam$counts[,x]))])/number.unmapped.reads.df.yam$Unmapped.Reads[x])
data=data.frame(a,b)
data=t(data)
colnames(data)=colnames(pfpx.yam$counts)
rownames(data)=c("Falciparum","Vivax")

# Make a stacked barplot
pdf(paste0(outputdir,"FractionReadsMappingToPlasmodium_NormalisedByUnmappedReads_VivaxAndFalciparumSeparated_Yamagishi.pdf"), height=8, width=15)
par(oma=c(5,0,0,0))
barplot(data, col=plasmo , border="white", las=3, main="Fraction of Total Unmapped Reads", ylim=c(0,1))
legend(x="topright", col=plasmo[c(1:2)], legend=c("Falciparum","Vivax"), pch=15)
dev.off()

#Transform this data in %
pdf(paste0(outputdir,"FractionReadsMappingToPlasmodium_NormalisedByUnmappedReads_Percentage__VivaxAndFalciparumSeparated_Yamagishi.pdf"), height=10, width=15)
data_percentage=apply(data, 2, function(x){x*100/sum(x,na.rm=T)})
par(oma=c(5,0,0,0))
barplot(data_percentage, col=plasmo , border="white", las=3)
legend(x="topright", col=plasmo[c(1:2)], legend=c("Falciparum","Vivax"), pch=15, cex=0.8)
dev.off()

# now do this for fraction of reads mapping to Hg38
a=sapply(1:ncol(pfpx.yam$counts), function(x)sum(pfpx.yam$counts[,x][grep("PF3D7|mal",names(pfpx.yam$counts[,x]))])/y$samples$lib.size[x])
b=sapply(1:ncol(pfpx.yam$counts), function(x)sum(pfpx.yam$counts[,x][grep("PVP01",names(pfpx.yam$counts[,x]))])/y$samples$lib.size[x])
data=data.frame(a,b)
data=t(data)
colnames(data)=colnames(pfpx.yam$counts)
rownames(data)=c("Falciparum","Vivax")

# Make a stacked barplot
pdf(paste0(outputdir,"FractionReadsMappingToPlasmodium_NormalisedByHg38_VivaxAndFalciparumSeparated_Yamagishi.pdf"), height=8, width=15)
par(oma=c(5,0,0,0))
barplot(data, col=plasmo , border="white", las=3, main="Fraction of Total Hg38 Reads", ylim=c(0,1))
legend(x="topright", col=plasmo[c(1:2)], legend=c("Falciparum","Vivax"), pch=15)
dev.off()

#Transform this data in %
pdf(paste0(outputdir,"FractionReadsMappingToPlasmodium_NormalisedByHg38_Percentage__VivaxAndFalciparumSeparated_Yamagishi.pdf"), height=10, width=15)
data_percentage=apply(data, 2, function(x){x*100/sum(x,na.rm=T)})
par(oma=c(5,0,0,0))
barplot(data_percentage, col=plasmo , border="white", las=3)
legend(x="topright", col=plasmo[c(1:2)], legend=c("Falciparum","Vivax"), pch=15, cex=0.8)
dev.off()