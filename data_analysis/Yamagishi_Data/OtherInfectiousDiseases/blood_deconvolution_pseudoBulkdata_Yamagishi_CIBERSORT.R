# Script created 09.09.19
# load in Yamagishi count matrix and convert gene names to P. Berghei orthologs. Convert to CPM, then save for CIBERSORT output

# load packages
library(edgeR)

# Set paths:
inputdir="/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/Output/Malaria/Plasmodium/Yamagishi/"
outputdir="/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/Output/Malaria/Yamagishi-Analysis/bloodDeconvolution/"
referenceFile="/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/ReferenceFiles/bloodDeconvolution/MalariaCellAtalas/"

# Read in Yamagishi mixture data ------------------------------------------------------
load(paste0(inputdir, "Yamagishi.unfiltered_counts.Rda"))

# separate P.Falciparum and P.Vivax data
# Falciparum
falciparum=pfpx.yam[grep("PF3D7|mal", rownames(pfpx.yam)),]
rownames(falciparum)=gsub("exon_", "", rownames(falciparum))
rownames(falciparum)=make.unique(sapply(1:length(rownames(falciparum)), function(x) strsplit(rownames(falciparum)[x],"-")[[1]][1]))
# Vivax
vivax=pfpx.yam[grep("PVP01", rownames(pfpx.yam)),]
rownames(vivax)=gsub("exon_", "", rownames(vivax))
rownames(vivax)=make.unique(sapply(1:length(rownames(vivax)), function(x) strsplit(rownames(vivax)[x],"-")[[1]][1]))

# get orthologs ---------------------------------------------------------------

orthologs=read.csv(paste0(referenceFile,"Orthologs.csv"), row.names=1)
orthologs$berghei=rownames(orthologs)
# add in PVP01 data
PVP_Sal_orth=read.csv(paste0(referenceFile,"Orthologs_Sanger_PVP-Sal.csv"))
orthologs$P01=PVP_Sal_orth[match(orthologs$vivax, PVP_Sal_orth$sal1_name), "p01_id"]

# Falciparum - convert to CPM ----------------------------------------

# assign PB orthologs to falciparum
PB_orthologs_falciparum=orthologs[match(rownames(falciparum), as.character(orthologs$falciparum)),"berghei"]
falciparum$genes$PB_orthologs=PB_orthologs_falciparum
falciparum=falciparum[-which(is.na(falciparum$genes$PB_orthologs)),]
rownames(falciparum)=falciparum$genes$PB_orthologs
falciparum_counts=falciparum$counts
falciparum_cpm_counts=cpm(falciparum_counts)
# add in id name as column
falciparum_cpm_counts=cbind(id=row.names(falciparum_cpm_counts), falciparum_cpm_counts)

# save file
write.table(falciparum_cpm_counts, file=paste0(outputdir,"CPM_YamagishiCounts_BergheiOrthologs_Falciparum.txt"), sep="\t", quote=F, row.names=F)

# Vivax - convert to CPM --------------------------------------------

# assign PB orthologs to vivax
PB_orthologs_vivax=orthologs[match(rownames(vivax), as.character(orthologs$P01)),"berghei"]
vivax$genes$PB_orthologs=PB_orthologs_vivax
vivax=vivax[-which(is.na(vivax$genes$PB_orthologs)),]
rownames(vivax)=vivax$genes$PB_orthologs
vivax_counts=vivax$counts
vivax_cpm_counts=cpm(vivax_counts)
# add in id name as column
vivax_cpm_counts=cbind(id=row.names(vivax_cpm_counts), vivax_cpm_counts)

# save file
write.table(vivax_cpm_counts, file=paste0(outputdir,"CPM_YamagishiCounts_BergheiOrthologs_Vivax.txt"), sep="\t", quote=F, row.names=F)

