# by KSB, 26.08.19

# load in all covariates for the Yamagishi et al 2014 paper: Interactive transcriptome analysis of malaria patients and infecting Plasmodium falciparum. 

# load packages
library(RColorBrewer)
library(edgeR)
library(plyr)
library(ggsci)
library(scales)
library(openxlsx)
library(magrittr)
library(XML)

# Set paths:
inputdir = "/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/Output/"
outputdir="/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/Output/Malaria/Yamagishi-Analysis/dataPreprocessing/"
SRAdir="/Users/katalinabobowik/Documents/Singapore_StemCells/Projects/Sumba/Papers/SupplementaryMaterials/"
refdir="/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/ReferenceFiles/"
plasmoDir="/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/Output/Malaria/Outliers_PFPX/Yamagishi/"

# set up colour palette. The 'NPG' palette (Nature Publising Group) from ggsci will be used for the standard palette
standard_col=c(pal_npg()(10), pal_futurama()(12), pal_tron()(3))
plasmo=brewer.pal(3, "Set2")

# BEGIN ANALYSIS -----------------------------------------------------------

# load in count data (as DGE list object)
load(paste0(inputdir, "Malaria/Yamagishi-Analysis/dataPreprocessing/unfiltered_DGElistObject.Rda"))

# Assign covariates --------------------------------------

# Load in Yamagishi et al 2014 supplementary table 11 (with patient information)
sup11.sick=read.xlsx(paste0(refdir,"Yamagishi/Supplemental_Table_11.xlsx"), sheet=1)
sup11.controls=read.xlsx(paste0(refdir,"Yamagishi/Supplemental_Table_11.xlsx"), sheet=3)
sup11.all=rbind(sup11.sick, sup11.controls)

# load in SRA run table info for sick samples and controls
sra.sick=read.delim("/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/ReferenceFiles/SraRunTable.txt", as.is=T, strip.white=T)
sra.controls=read.delim("/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/ReferenceFiles/Yamagishi/SraRunTable_Controls.txt", as.is=T, strip.white=T)
sra.sick=sra.sick[,c("Library_Name_s","Run_s")]
sra.controls=sra.controls[,c("Library_Name","Run")]
colnames(sra.sick)=colnames(sra.controls)
sra.all=rbind(sra.sick,sra.controls)

# Create empty matrix and fill in with patient names, Age, Sex, and SRA Run ID
sample_summary=sup11.all[,-which(names(sup11.all) %in% "Patient_ID")]
# convert to data frame
sample_summary=as.data.frame(sample_summary)
# get rid of unnecessary characters whic may cause problems later
colnames(sample_summary)=gsub("%","",colnames(sample_summary))
# There is a discrepancy between the SRA table and Supplementary 11 table- one says "malaria7#09" and one says "malaria7#009". We need to change "malaria7#09" to "malaria7#009"
sample_summary$Patient_Name=gsub("malaria7#09", "malaria7#009", sample_summary$Patient_Name)
# Match patient names with the patient names in the SRA table, then grab the corresponding SRA run IDs
sra.all[match(sample_summary$Patient_Name, sra.all$Library_Name),"Run"]
# we seem to have NA values in the dataframe. Which ones are these?
sample_summary$Patient_Name[which(is.na(match(sample_summary$Patient_Name, sra.all$Library_Name)))]
# [1] "malaria11#1" "malaria11#2" "malaria11#3" "malaria11#4" "malaria11#5"
# [6] "malaria11#6" "malaria11#7" "malaria11#8" "malaria11#9"

# when checking the sra run table sheet, these samples have a 0 in front of them. Let's take this out to keep sample names the same.
sra.all$Library_Name=gsub("malaria11#0","malaria11#", sra.all$Library_Name)
sample_summary[,"SRA_ID"]=sra.all[match(sample_summary$Patient_Name, sra.all$Library_Name),"Run"]

# load in XML file with extra information on sequencing date
seqDate=matrix(nrow=ncol(y),ncol=2)
colnames(seqDate)=c("Sample","date")
xml.sick <- xmlParse(paste0(refdir,"Yamagishi/DRA000949.run.xml"))
xml.sick <- xmlToList(xml.sick)
xml.control = xmlParse(paste0(refdir,"Yamagishi/DRA001875.run.xml"))
xml.control = xmlToList(xml.control)
xml.allsamples=c(xml.sick, xml.control)
counter=0
for (sample in colnames(y)){
  counter=counter+1
  seqDate[counter,]=c(sample,unname(xml.allsamples[grep(sample,xml.allsamples)]$RUN$.attrs["run_date"]))
}
y$samples$seqDate=as.factor(seqDate[,"date"])
# take off trailing time, which is the same for each sample
y$samples$seqDate=as.factor(gsub("\\:","",y$samples$seqDate) %>% gsub("\\+","",.) %>% gsub("T1200000900","",.))

# also assign instrument information, created in the file 'SequenceBatch_plus_Flowcell_extraction_YAmagishi.sh'
instrument=read.table("/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/Output/Malaria/Yamagishi-Analysis/Yamagishi_allSamples_instrument.txt")
y$samples$instrument=instrument[match(rownames(y$samples), as.character(instrument[,1])),2]

# first, assign new sex information (file created in script "Yamagishi_sexIdentification.R"). Check to make sure column names match
updated_sex_info=read.table(paste0(inputdir,"Malaria/GenderClassification/updated_sexCovariate_Yamagishi.txt"))
identical(rownames(y$samples), rownames(updated_sex_info))
# TRUE
y$samples$sex <- updated_sex_info[match(rownames(y$samples), rownames(updated_sex_info)),]
y$samples$sex=as.factor(as.matrix(y$samples$sex))

# now the rest
y$samples$age <- as.numeric(sample_summary[match(colnames(y), sample_summary$SRA_ID),"Age"])
y$samples$location <- as.factor(sample_summary[match(colnames(y), sample_summary$SRA_ID),"From"])
y$samples$PFload <- as.numeric(as.character(sample_summary[match(colnames(y), sample_summary$SRA_ID),"Pf_tags"]))
y$samples$Temperature <- as.numeric(as.character(sample_summary[match(colnames(y), sample_summary$SRA_ID),"Temparature"]))
y$samples$Infection.Date <- as.numeric(as.character(sample_summary[match(colnames(y), sample_summary$SRA_ID),"Infection.Date"]))

# let's also add PF load information. Here, we're reading in a file created in the script "SickSamples_Identification.R".
# In order to get the fractio of PFPX reads, I divided the total plasmodium counts by the total unmapped read library size
alignedPFPXreads=read.table(paste0(inputdir,"Malaria/Plasmodium/Yamagishi/Yamagishi_FractUnmappedReads.txt"), header=T)
y$samples$fract.total.reads=alignedPFPXreads[match(rownames(y$samples), rownames(alignedPFPXreads)),"fract.total"]

# add in blood info
blood=read.table(paste0(inputdir, "Malaria/Yamagishi-Analysis/bloodDeconvolution/predictedCellCounts_DeconCell_Yamagishi.txt"), sep="\t", as.is=T, header=T)
colnames(blood)=c("Gran","Bcell","CD4T","CD8T","NK","Mono")
y$samples=cbind(y$samples, blood[match(rownames(y$samples), rownames(blood)),])

# add in parasite stage information 
# First falciparum
falciparum_stage=read.table(paste0(inputdir,"/Malaria/Yamagishi-Analysis/bloodDeconvolution/CIBERSORT_Output_Falciparum_Yamagishi.txt"), sep="\t", header=T)
# make a the falciparum row names a vector to match the column names of y
rownames(falciparum_stage)=sapply(1:length(falciparum_stage$Input.Sample), function(x) tail(strsplit(as.character(falciparum_stage$Input.Sample)[x],"_")[[1]][1]))
# assign 'NA' values to 
falciparum_stage[which(falciparum_stage$P.value>0.05),] = NA
# remove columns we're not interested in
falciparum_stage=falciparum_stage[,-which(colnames(falciparum_stage) %in% c("Input.Sample","P.value","Pearson.Correlation","RMSE"))]
falciparum_stage[which(is.na(falciparum_stage$bbSpz)),] = 0
# now add in a "falciparum" suffix to each of the column headers to differentiate between falciparum and vivax
colnames(falciparum_stage)=paste(colnames(falciparum_stage),"falciparum", sep="_")
y$samples=cbind(y$samples, falciparum_stage[match(rownames(y$samples), rownames(falciparum_stage)),])

# now vivax
vivax_stage=read.table(paste0(inputdir,"/Malaria/Yamagishi-Analysis/bloodDeconvolution/CIBERSORT_Output_Vivax_Yamagishi.txt"), sep="\t", header=T)
# make a the falciparum row names a vector to match the column names of y
rownames(vivax_stage)=sapply(1:length(vivax_stage$Input.Sample), function(x) tail(strsplit(as.character(vivax_stage$Input.Sample)[x],"_")[[1]][1]))
# assign 'NA' values to 
vivax_stage[which(vivax_stage$P.value>0.05),] = NA
# remove columns we're not interested in
vivax_stage=vivax_stage[,-which(colnames(vivax_stage) %in% c("Input.Sample","P.value","Pearson.Correlation","RMSE"))]
vivax_stage[which(is.na(vivax_stage$bbSpz)),] = 0
# now add in a "vivax" suffix to each of the column headers to differentiate between falciparum and vivax
colnames(vivax_stage)=paste(colnames(vivax_stage),"vivax", sep="_")
y$samples=cbind(y$samples, vivax_stage[match(rownames(y$samples), rownames(vivax_stage)),])
covariates = y$samples

# save as text file
write.table(covariates, file=paste0(outputdir, "covariates.txt"), row.names=T)