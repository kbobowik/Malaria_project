# by KSB, 26.08.19

# create an RNASeq pipeline for the Yamagishi et al 2014 reads from the paper: Interactive transcriptome analysis of malaria patients and infecting Plasmodium falciparum. 

# load packages
library(RColorBrewer)
library(edgeR)
library(plyr)
library(ggsci)
library(scales)
library(openxlsx)
library(magrittr)
library(XML)
library(ggplot2)
library(dplyr)
library(viridis)

# Set paths:
inputdir = "/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/Output/"
outputdir="/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/Output/Malaria/Yamagishi-Analysis/dataPreprocessing/"
SRAdir="/Users/katalinabobowik/Documents/Singapore_StemCells/Projects/Sumba/Papers/SupplementaryMaterials/"
refdir="/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/ReferenceFiles/"
plasmoDir="/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/Output/Malaria/Plasmodium/Yamagishi/"

# set up colour palette. The 'NPG' palette (Nature Publising Group) from ggsci will be used for the standard palette
plasmo=brewer.pal(3, "Set2")
standard_col=c(pal_npg()(10), pal_futurama()(12), pal_tron()(3))
species.col=c(viridis(5)[1],brewer.pal(3, "Set2")[1],"coral1")
study.col=viridis(n=2, alpha=0.8)

# BEGIN ANALYSIS -----------------------------------------------------------

# load in count data (as DGE list object)
load(paste0(inputdir, "Malaria/Yamagishi-Analysis/dataPreprocessing/unfiltered_DGElistObject.Rda"))
dim(y)
# [1] 27413   147

# Assign covariates --------------------------------------

# load in covariates table, created in Yamagishi_CovariateSetup.R
covariates=read.table(paste0(inputdir, "Malaria/Yamagishi-Analysis/dataPreprocessing/covariates.txt"))

# remove any duplicated columns in covariate DF
which(colnames(covariates) %in% colnames(y$samples))
# [1] 1 2 3 4 5
covariates=covariates[,-which(colnames(covariates) %in% colnames(y$samples))]
covariates=covariates[colnames(y)[which(colnames(y) %in% rownames(covariates))],]

# check to make sure samplenames are the same
identical(colnames(y), rownames(covariates))
# [1] TRUE

# add this to the DGE list
y$samples=cbind(y$samples, covariates)

# add in species info table, created and decided in plasmodium data exploration script "QC-outliers-Yamagishi"
species=read.table(paste0(plasmoDir,"onlySamplesInStudy/updatedSpeciesTable.txt"))

# rename column names of species file
rownames(species)=sapply(strsplit(as.character(species$samples), "[_.]"), `[`, 1)
y$samples$species=species[match(rownames(y$samples), rownames(species)),"species"]
# since species is confounded with disease status, let's also add in vivax and falciparum separately
y$samples$is.vivax=as.factor(y$samples$species=="vivax")
y$samples$is.falciparum=as.factor(y$samples$species=="falciparum")

# assign covariate names
# subtract variables we don't need
subtract=c("files", "group", "norm.factors")
# get index of unwanted variables
subtract=which(colnames(y$samples) %in% subtract)
covariate.names = colnames(y$samples)[-subtract]
for (name in covariate.names){
 assign(name, y$samples[[paste0(name)]])
}

# Age, RIN, and library size need to be broken up into chunks for easier visualisation of trends (for instance in Age, we want to see high age vs low age rather than the effect of every single age variable)
for (name in c("age","lib.size")){
  assign(name, cut(as.numeric(as.character(y$samples[[paste0(name)]])), breaks=5))
}

# assign factor variables
factorVariables=c(colnames(Filter(is.factor,y$samples))[which(colnames(Filter(is.factor,y$samples)) %in% covariate.names)], "age", "lib.size")
numericVariables=colnames(Filter(is.numeric,y$samples))[which(colnames(Filter(is.numeric,y$samples)) %in% covariate.names)] %>% subset(., !(. %in% factorVariables))

for (name in factorVariables) {
  if(name=="diseaseStatus"){
    col=plasmo
  }
  else{
    col=standard_col
  }
  png(paste0(outputdir,name,"_rarefactionCurves.png"), units="px", width=1600, height=1600, res=300)
  plot(1:length(y$counts[,1]), cumsum(sort(y$counts[,1], decreasing=T)/sum(y$counts[,1])), log="x", type="n", xlab="Number of genes", ylab="Fraction of reads pool", ylim=c(0,1), main=name) ## initialize the plot area
  counter=0
  for (sample in colnames(y)){
    counter=counter+1
    lines(1:length(y$counts[,sample]), cumsum(sort(y$counts[,sample], decreasing=T)/sum(y$counts[,sample])), lwd=2, col=col[as.numeric(get(name))[counter]])
  }
  levels=levels(get(name))
  levels[which(is.na(levels))] = "NA"
  legend(x="bottomright", bty="n", col=col[1:length(levels(get(name)))], legend=levels, lty=1, lwd=2)
  dev.off()
}

rank=apply(y$counts, 2, function(x) rank(x,ties.method = c("average")))
# save ranked file
write.table(rank, file=paste0(outputdir,"RankedGenes_Prefiltering_Yamagishi.txt"), sep="\t", quote=F)
genes=apply(rank, 2, function(x) y$genes$SYMBOL[which.max(x)])
topGenes.df=data.frame(genes, diseaseStatus)

pdf(paste0(outputdir,"mostHighlyExpressedGenes.pdf"))
ggplot(tally(group_by(topGenes.df, genes, diseaseStatus)),
aes(x = genes, y = n, fill = diseaseStatus)) + labs(x="Gene", y = "Gene count") +
geom_bar(stat="identity") + ggtitle("Most Highly Expressed Genes") + theme_minimal()
dev.off()

# get rid of globin genes, to see if this affects the rarefaction curves
globin_genes = c("HBA1", "HBA2", "HBB", "MB", "CYGB", "NGB")
remove_globin_index = grep(paste("^",globin_genes,"$",sep="",collapse="|"), y$genes$SYMBOL)
y=y[-c(remove_globin_index),, keep.lib.sizes=FALSE]
dim(y)
# [1] 27407   147

for (name in factorVariables) {
  if(name=="diseaseStatus"){
    col=plasmo
  } else if (name=="species") {
    col=species.col
  }
  else {
    col=standard_col
  }
  png(paste0(outputdir,name,"_rarefactionCurves_noGlobinGenes.png"), units="px", width=1600, height=1600, res=300)
  plot(1:length(y$counts[,1]), cumsum(sort(y$counts[,1], decreasing=T)/sum(y$counts[,1])), log="x", type="n", xlab="Number of genes", ylab="Fraction of reads pool", ylim=c(0,1), main=name) ## initialize the plot area
  counter=0
  for (sample in colnames(y)){
    counter=counter+1
    lines(1:length(y$counts[,sample]), cumsum(sort(y$counts[,sample], decreasing=T)/sum(y$counts[,sample])), lwd=2, col=col[as.numeric(get(name))[counter]])
  }
  levels=levels(get(name))
  levels[which(is.na(levels))] = "NA"
  legend(x="bottomright", bty="n", col=col[1:length(levels(get(name)))], legend=levels, lty=1, lwd=2)
  dev.off()
}

# Data pre-processing ------------------------------------------------------------------------

# Filter out samples with library size <9 million 
y=y[,which(y$samples$lib.size >= 9000000)]
dim(y)
# [1] 27407   95

# Visualise library size after filtering
pdf(paste0(outputdir,"librarysizeYamagishi_postFiltering.pdf"), height=10, width=15)
barplot(y$samples$lib.size*1e-6, ylab="Library size (millions)", cex.main=1.5, main="Library Size \nYamagishi", cex.names=0.75, col=study.col[2], names=colnames(y), las=3, ylim=c(0,max(y$samples$lib.size*1e-6)+10))
#legend(x="topright", col=plasmo[unique(as.numeric(factor(y$samples$diseaseStatus)))], legend=unique(y$samples$diseaseStatus), pch=15, cex=0.8)
dev.off()

# Total number of genes
pdf(paste0(outputdir,"totalGenesYamagishi_postFiltering.pdf"), height=10, width=15)
barplot(apply(y$counts, 2, function(c)sum(c!=0)), ylab="n Genes", cex.names=0.75, col=plasmo[as.numeric(factor(y$samples$diseaseStatus))], names=colnames(y), las=3, ylim=c(0,max(apply(y$counts, 2, function(c)sum(c!=0)))+2000))
legend(x="topright", col=plasmo[unique(as.numeric(factor(y$samples$diseaseStatus)))], legend=unique(y$samples$diseaseStatus), pch=15, cex=0.8)
dev.off()

# How does this look like for the PF load?
pdf(paste0(outputdir,"totalPFPXload_fractExpr.pdf"), height=10, width=15)
barplot(y$samples$fract.total, col=plasmo[as.numeric(factor(y$samples$diseaseStatus))], main="Fraction of Total Reads")
legend(x="topright", col=plasmo[unique(as.numeric(factor(y$samples$diseaseStatus)))], legend=unique(factor(y$samples$diseaseStatus)), pch=15, cex=0.8)
dev.off()

# Classify sick samples, as per script "diseaseStatus identification"
# remove sample identified as healthy
y=y[,-which((colnames(y) %in% "DRR006377"))]
dim(y)
# 27407 94

# Transformation from the raw scale --------------------------------------------------------------------

# Transform raw counts onto a scale that accounts for library size differences. Here, we transform to CPM and log-CPM values (prior count for logCPM = 0.25). 
cpm <- cpm(y) 
lcpm <- cpm(y, log=TRUE)

# Remove genes that are lowly expressed -----------------------------------------

# get histogram of number of genes expressed at log2 cpm > 0.5 and 1 (before filtering)
pdf(paste0(outputdir,"historgram_nGenesExpressed.pdf"), height=10, width=15)
hist(rowSums(cpm>0.5), main= "n Genes expressed at cpm > 0.5 \n pre-filtering", xlab="samples", col=4)
hist(rowSums(cpm>1), main= "n Genes expressed at cpm > 1 \n pre-filtering", xlab="samples", col=4)
dev.off()

# Extract CPM information for each village
cpm_control <- cpm[,grep("control",y$samples$diseaseStatus)]
cpm_sick <- cpm[,grep("malaria",y$samples$diseaseStatus)]

# set keep threshold to only retain genes that are expressed at or over a CPM of one in at least half of the group
keep.control <- rowSums(cpm_control>1) >= (ncol(cpm_control)*0.5)
keep.sick <- rowSums(cpm_sick>1) >= (ncol(cpm_sick)*0.5)

keep <- keep.control | keep.sick
y <- y[keep,, keep.lib.sizes=FALSE]
dim(y)
# 12895    94

# Compare library sizes before and after removing lowly-expressed genes
nsamples <- ncol(y)
col <- plasmo[as.numeric(factor(y$samples$diseaseStatus))]
pdf(paste0(outputdir,"libraryDensity_afterFiltering.pdf"), height=8, width=15)
par(mfrow=c(1,2), mar=c(5,7,4,4))
plot(density(lcpm[,1]), ylab='', col=col[1], lwd=2, cex.axis=1.5, cex.lab=1.5, ylim=c(0,max(density(lcpm)$y)), las=2, main="", xlab="")
title(main="All genes", xlab="Log-cpm", cex.main=2, cex.lab=1.5)
title(ylab="Density", line=5, cex.lab=1.5)
abline(v=0, lty=3)
for (i in 2:nsamples){
    den <- density(lcpm[,i])
    lines(den$x, den$y, col=col[i], lwd=2)
}
legend("topright", as.character(unique(y$samples$diseaseStatus)), ncol=1, cex=1.3, text.col=unique(col), bty="n")

lcpm <- cpm(y, log=TRUE)
plot(density(lcpm[,1]), ylab='', col=col[1], lwd=2, cex.axis=1.5, cex.lab=1.5, ylim=c(0,max(density(lcpm)$y)+0.2), las=2, main="", xlab="")
title(main="Filtered genes", xlab="Log-cpm", cex.main=2, cex.lab=1.5)
abline(v=0, lty=3)
for (i in 2:nsamples){
    den <- density(lcpm[,i])
    lines(den$x, den$y, col=col[i], lwd=2)
}
legend("topright", as.character(unique(y$samples$diseaseStatus)), ncol=1, cex=1.3, text.col=unique(col), bty="n")
dev.off()

# Normalise gene expression distributions (i.e., no bias introduced during sample preparation/sequencing)
y <- calcNormFactors(y, method = "TMM")

# Duplicate data, set normalisation back to 1, and plot difference between normalised and non-normalised data
y2 <- y
y2$samples$norm.factors <- 1
lcpm <- cpm(y2, log=TRUE)
pdf(paste0(outputdir,"NormalisedGeneExpressionDistribution_YamagishiHealthyvsControls.pdf"), height=15, width=15)
par(oma=c(2,0,0,0), mfrow=c(2,1), mar=c(5,6,4,4))
boxplot(lcpm, las=2, col=col, main="", cex.axis=1.5, cex.lab=1.5, xlab='', xaxt='n')
title(main="Unnormalised data",ylab="Log-cpm", cex.main=2, cex.lab=1.5)
y2 <- calcNormFactors(y2, method="TMM")
lcpm <- cpm(y2, log=TRUE)
boxplot(lcpm, las=2, col=col, main="", xlab='', cex.axis=1.5, cex.lab=1.5, xaxt='n')
title(main="Normalised data",ylab="Log-cpm", cex.main=2, cex.lab=1.5)
dev.off()

# get density plot after normalisation
pdf(paste0(outputdir,"libraryDensity_afterFilteringandNormalisation.pdf"), height=10, width=15)
plot(density(lcpm[,1]), col=col[1], lwd=2, ylim=c(0,max(density(lcpm)$y)+0.2), las=2, main="", xlab="")
title(main="A. Raw data", xlab="Log-cpm")
abline(v=0, lty=3)
for (i in 2:nsamples){
    den <- density(lcpm[,i])
    lines(den$x, den$y, col=col[i], lwd=2)
}
legend("topright", colnames(y), ncol=2, cex=0.6, text.col=col, bty="n")
dev.off()

# recalculate lcpm
lcpm=cpm(y, log=T)

# save data
save(lcpm, file=paste0(outputdir, "Yamagishi.logCPM.TMM.filtered.Rda"))
save(y, file=paste0(outputdir, "Yamagishi.read_counts.TMM.filtered.Rda"))
write.table(colnames(y), paste0(outputdir,"samplenames.txt"))
