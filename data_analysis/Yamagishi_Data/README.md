This folder contains files related to data analysis for the Yamagishi dataset collected by the SRA Run selector for the paper "Interactive transcriptome analysis of malaria patients and infecting Plasmodium falciparum" (study DRA000949). 

Files:

blood_deconvolution_DeconCell_Yamagishi.R is the script used to perform blood deconvolution on whole blood using Decon Cell. 

blood_deconvolution_pseudoBulkdata_Yamagishi_CIBERSORT.R is the script used to prepare files for input into CIBERSORT.

Yamagishi_countData_AllDiseases.R is the script used to read count files (created by FeatureCounts) into R.

Yamagishi_CovariateSetup_allDiseases.R is the script used to setup all covariates used in the study.

Yamagishi_dataExploration_allDiseases.R is the script used to perform data exploration of the preprocessed Yamagishi data

Yamagishi_dataPreprocessing_allDiseases.R is the script used to perform data preprocessing on the samples. 

Yamagishi_SexIdentification_allDiseases.R is the script used to identify sex based on expression profiles of marker genes from the paper "Robust and tissue-independent gender-specific transcript biomarkers".

Yamagishi_Sick_vs_Controls_DE_allCovariates_allDiseases.R is the script used to perform differential expression on healthy and sick samples within the Yamagishi dataset.

Yamagishi-PlasmoCountData-DiseaseStatusIdentification.R is the script used to read in count data for the plasmodium reads and get initial disease classification of samples. 

Yamagishi-plasmodiumData_onlySamplesInStudy.R is the script used for clustering analysis and data exploration of the plasmodium data. 