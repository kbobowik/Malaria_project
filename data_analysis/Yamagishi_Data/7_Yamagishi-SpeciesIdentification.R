# created by KSB, 09.08.2019

# Load dependencies and set input paths --------------------------

# Load dependencies:
library(edgeR)
library(plyr)
library(openxlsx)
library(RColorBrewer)
library(magrittr)
library(reshape2)
library(ggplot2)
library(NineteenEightyR)
library(pheatmap)
library(dendextend)
library(wesanderson)
library(ggsci)
library(scales)
library(XML)
library(ComplexHeatmap)
library(viridis)
library(transcripTools)

# colours
plasmo=brewer.pal(3, "Set2")
standard_col=c(pal_npg()(10), pal_futurama()(12), pal_tron()(3))
species.col=c(viridis(5)[1],brewer.pal(3, "Set2")[1],"coral1")

# Set paths:
inputdir = "/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/Output/"
FeatureCountsDir= "/Users/katalinabobowik/Documents/UniMelb_PhD/Projects/Sumba/FeatureCounts/"
refdir <- "/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/ReferenceFiles/"

# Set output directory and create it if it does not exist:
outputdir <- "/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/Output/Malaria/Plasmodium/Yamagishi/onlySamplesInStudy/"

if (file.exists(outputdir) == FALSE){
    dir.create(outputdir)
}

# Read in plasmodium count data 
load(paste0(inputdir,"Malaria/Plasmodium/Yamagishi/PlasmodiumCounts_unfiltered_Yamagishi_fractUnmappedReads.Rds"))

# covariate setup ------------------------------------------------------

# load in covariate information
covariates=read.table(paste0(inputdir, "Malaria/Yamagishi-Analysis/dataPreprocessing/covariates.txt"))

# remove duplicated columns
remove=which(colnames(covariates) %in% colnames(pfpx.yam$samples))
# Append human covariate data to plasmodium matrix
covariates=covariates[,-remove] %>% .[,-which(colnames(covariates) %in% "files")]
pfpx.yam$samples=cbind(pfpx.yam$samples,covariates[match(rownames(pfpx.yam$samples),rownames(covariates)),])

# read in unmapped reads file  -----------------------------------------------------------------------

# read in unmapped reads file
number.unmapped.reads.df.yam=read.table(paste0(refdir,"unmappedReads_Counts_Yamagishi.txt"))
# set column names
colnames(number.unmapped.reads.df.yam)=c("Sample.ID","Unmapped.Reads")
pfpx.unmapped.names=sapply(1:nrow(number.unmapped.reads.df.yam), function(x) strsplit(as.character(number.unmapped.reads.df.yam$Sample.ID[x]),"_")[[1]][1])
number.unmapped.reads.df.yam$Sample.ID = sapply(1:nrow(number.unmapped.reads.df.yam), function(x) strsplit(as.character(number.unmapped.reads.df.yam$Sample.ID[x]),"_")[[1]][1])
# Match the unmapped reads df with the pfpx.yam object
number.unmapped.reads.df.yam=number.unmapped.reads.df.yam[match(colnames(pfpx.yam), number.unmapped.reads.df.yam$Sample.ID),]
identical(colnames(pfpx.yam), number.unmapped.reads.df.yam$Sample.ID)
# [1] TRUE

# Get percentage of reads mapping to both Plasmodium species ------------------------

# load in human data
load(paste0(inputdir,"Malaria/Yamagishi-Analysis/dataPreprocessing/unfiltered_DGElistObject.Rda"))
y=y[,which(colnames(y) %in% colnames(pfpx.yam))]
dim(y)

# First, get all gene names with 'PF3D7' (the falciparum genome I'm using) and 'mal', which are mitochondrial chromosomes in the PF3d7 genome
fract_falciparum=sapply(1:ncol(pfpx.yam$counts), function(x)sum(pfpx.yam$counts[,x][grep("PF3D7|mal",names(pfpx.yam$counts[,x]))])/(number.unmapped.reads.df.yam$Unmapped.Reads + y$samples$lib.size)[x])
fract_vivax=sapply(1:ncol(pfpx.yam$counts), function(x)sum(pfpx.yam$counts[,x][grep("PVP01",names(pfpx.yam$counts[,x]))])/(number.unmapped.reads.df.yam$Unmapped.Reads + y$samples$lib.size)[x])
fract_species=data.frame(fract_falciparum,fract_vivax)
colnames(fract_species)=c("Falciparum","Vivax")
rownames(fract_species)=pfpx.yam$samples$samples

controls=c()
vivax=c()
falciparum=c()
for (row in 1:nrow(fract_species)){
  sample=rownames(fract_species)[row]
  if(fract_species[sample,"Vivax"] > fract_species[sample,"Falciparum"]){
    if(length(grep("Sick_Other|Controls",sample))>0){
      controls=c(controls, sample)
    } 
    else {
    vivax=c(vivax,sample)
    }
  }
  if(fract_species[sample,"Vivax"] < fract_species[sample,"Falciparum"]){
    falciparum=c(falciparum,sample)
  }
}

# now assign to DGE list
colnames(pfpx.yam) = as.character(pfpx.yam$samples$samples)
pfpx.yam$samples$species=NA
pfpx.yam$samples[vivax,"species"]="vivax"
pfpx.yam$samples[falciparum,"species"]="falciparum"
pfpx.yam$samples[controls,"species"]="control"
pfpx.yam$samples$species=as.factor(pfpx.yam$samples$species)

# save unfiltered counts file
save(pfpx.yam, file=paste0(outputdir, "Yamagishi.unfiltered_counts.Rda"))

# assign covariate names -------------------------------------

# subtract variables we don't need
subtract=c("group", "norm.factors", "samples", "files")
# get index of unwanted variables
subtract=which(colnames(pfpx.yam$samples) %in% subtract)
covariate.names = colnames(pfpx.yam$samples)[-subtract]
for (name in covariate.names){
 assign(name, pfpx.yam$samples[[paste0(name)]])
}

# Age, RIN, and library size need to be broken up into chunks for easier visualisation of trends (for instance in Age, we want to see high age vs low age rather than the effect of every single age variable)
for (name in c("age","lib.size")){
  assign(name, cut(as.numeric(as.character(pfpx.yam$samples[[paste0(name)]])), breaks=5))
}

# assign names to covariate names so you can grab individual elements by name
names(covariate.names)=covariate.names

# assign factor variables
factorVariables=c(colnames(Filter(is.factor,pfpx.yam$samples))[which(colnames(Filter(is.factor,pfpx.yam$samples)) %in% covariate.names)], "age", "lib.size")
numericVariables=colnames(Filter(is.numeric,pfpx.yam$samples))[which(colnames(Filter(is.numeric,pfpx.yam$samples)) %in% covariate.names)] %>% subset(., !(. %in% factorVariables))

# make rarefaction curves
for (name in factorVariables){
  png(paste0(outputdir,name,"_rarefactionCurves.png"), units="px", width=1600, height=1600, res=300)
  plot(1:length(pfpx.yam$counts[,1]), cumsum(sort(pfpx.yam$counts[,1], decreasing=T)/sum(pfpx.yam$counts[,1])), log="x", type="n", xlab="Number of genes", ylab="Fraction of reads pool", ylim=c(0,1), main=name) ## initialize the plot area
  counter=0
  for (sample in colnames(pfpx.yam)){
    counter=counter+1
    if (name == "species"){
      col=species.col    
    }
    else{
      col=standard_col    
    }
  lines(1:length(pfpx.yam$counts[,sample]), cumsum(sort(pfpx.yam$counts[,sample], decreasing=T)/sum(pfpx.yam$counts[,sample])), lwd=2, col=col[as.numeric(get(name))][counter])
  levels=levels(get(name))
  levels[which(is.na(levels))] = "NA"
  legend(x="bottomright", bty="n", col=col[1:length(levels(get(name)))], legend=levels, lty=1, lwd=2)
  }
  dev.off()
}

# Initial data exploration --------------------------------------------------------------------

# let's first see what the total plasmodium read count looks like for sick vs healthy samples
pdf(paste0(outputdir, "TotalPlasmodiumCounts.pdf"), height=15, width=15)
barplot(pfpx.yam$samples$lib.size, col=plasmo[as.numeric(diseaseStatus)], las=3, names=diseaseStatus, main="Total Plasmodium Reads")
legend(x="topright", col=plasmo[unique(as.numeric(diseaseStatus))], legend=c("malaria", "controls"), pch=15, cex=0.8)
dev.off()
# now let's plot the fraction of reads, so the total plasmodium count noralised by the total number of unmapped reads
pdf(paste0(outputdir, "FractionPlasmodiumCounts.pdf"), height=15, width=15)
barplot(pfpx.yam$samples$fract.unmapped.reads, col=plasmo[as.numeric(diseaseStatus)], las=3, names=diseaseStatus, main="Fraction of Plasmodium Reads")
legend(x="topright", col=plasmo[unique(as.numeric(diseaseStatus))], legend=c("malaria", "controls"), pch=15, cex=0.8)
dev.off()

# Transformation from the raw scale --------------------------------------------------------------------
cpm.pfpx.yam=cpm(pfpx.yam)
lcpm.pfpx.yam=cpm(pfpx.yam, log=TRUE)

# Removal of lowly-expressed genes -------------------------------------------------------------------------------

# get histogram of number of genes expressed at log2 cpm > 0.5 and 1 (before filtering)
pdf(paste0(outputdir, "lcpm_preFiltering_Histogram.pdf_cpm1.pdf"), height=10, width=15)
par(mfrow=c(1,2))
hist(rowSums(lcpm.pfpx.yam>0.5), main= "Genes expressed >= 0.5 log2 CPM \n pre-filtering", xlab="samples", col=standard_col[2], ylim=c(0,25000))
hist(rowSums(lcpm.pfpx.yam>1), main= "Genes expressed >= 1 log2 CPM \n pre-filtering", xlab="samples", col=standard_col[3], ylim=c(0,25000))
dev.off()

# Extract CPM information for each village
cpm_control <- cpm.pfpx.yam[,grep("Controls",colnames(cpm.pfpx.yam))]
cpm_sick <- cpm.pfpx.yam[,grep("Sick$",colnames(cpm.pfpx.yam))]

# set keep threshold to only retain genes that are expressed at or over a CPM of one in at least one individual
keep.control <- rowSums(cpm_control>1) >= 1
keep.sick <- rowSums(cpm_sick>1) >= 1

keep <- keep.control | keep.sick
pfpx.yam <- pfpx.yam[keep,, keep.lib.sizes=FALSE]
dim(pfpx.yam)
# [1] 28325    94

# eliminate composition bias between libraries by upper quartile normalisation
pfpx.yam <- calcNormFactors(pfpx.yam, method="TMM")
# save normalised data
save(pfpx.yam, file=paste0(outputdir, "Yamagishi.read_counts.TMM.filtered.Rda"))

# now just plot how well TMM normalisation worked (since we like that one the best)
pdf(paste0(outputdir, "NormalisedGeneExpressionDistribution_IndoRNA_TMM.pdf"), height=15, width=15)
  par(oma=c(2,0,0,0), mfrow=c(2,1), mar=c(5,6,4,4))
  pfpx.yam2 <- pfpx.yam
  pfpx.yam2$samples$norm.factors <- 1
  lcpm.pfpx.yam <- cpm(pfpx.yam2, log=TRUE)
  boxplot(lcpm.pfpx.yam, las=2, col=plasmo[as.numeric(diseaseStatus)], main="", cex.axis=1.5, cex.lab=1.5, names=diseaseStatus, xlab='', xaxt='n')
  title(main="A. Unnormalised data",ylab="Log-cpm", cex.main=2, cex.lab=1.5)
  pfpx.yam2 <- calcNormFactors(pfpx.yam2, method="TMM")
  lcpm.pfpx.yam <- cpm(pfpx.yam2, log=TRUE)
  boxplot(lcpm.pfpx.yam, las=2, col=plasmo[as.numeric(diseaseStatus)], main="", xlab='', cex.axis=1.5, cex.lab=1.5, names=diseaseStatus, xaxt='n')
  title(main="B. Normalised data, TMM",ylab="Log-cpm", cex.main=2, cex.lab=1.5)
dev.off()

# Species identification -------------------------------------------------------------

# delete? # 
# # hone in on these samples and only plot the library sizes of samples which are less than the highest control library size
# malaria.LibSize = pfpx.yam$samples$lib.size[which(pfpx.yam$samples$diseaseStatus=="malaria")]
# control.LibSize = pfpx.yam$samples$lib.size[which(pfpx.yam$samples$diseaseStatus=="control")]
# # get names of these individuals
# samplenames.plas[which(malaria.LibSize < max(control.LibSize))]
# # [1] "DRR006376" "DRR006378" "DRR006384" "DRR006407"

# # plot the library size again after removal of lowly expressed genes and removing sample that aren't in the study
# pdf(paste0(outputdir, "TotalPlasmodiumCounts.pdf"), height=15, width=15)
# barplot(pfpx.yam$samples$lib.size, col=plasmo[as.numeric(diseaseStatus)], las=3, names=samplenames.plas, main="Total Plasmodium Reads")
# legend(x="topright", col=plasmo[unique(as.numeric(diseaseStatus))], legend=c("malaria", "controls"), pch=15, cex=0.8)
# abline(h=max(control.LibSize))
# dev.off()

# malaria.fract.unmapped.reads = pfpx.yam$samples$fract.unmapped.reads[which(pfpx.yam$samples$diseaseStatus=="malaria")]
# control.fract.unmapped.reads = pfpx.yam$samples$fract.unmapped.reads[which(pfpx.yam$samples$diseaseStatus=="control")]
# # get names of these individuals
# samplenames.plas[which(malaria.fract.unmapped.reads < max(control.fract.unmapped.reads))]
# # [1] "DRR006377" "DRR006381" "DRR006392" "DRR006431" "DRR006433" "DRR006442"
# # [7] "DRR006457"

# # plot fraction of reads
# pdf(paste0(outputdir, "FractionPlasmodiumCounts.pdf"), height=15, width=15)
# barplot(pfpx.yam$samples$fract.unmapped.reads, ylim=c(0,1), col=plasmo[as.numeric(diseaseStatus)], las=3, names=samplenames.plas, main="Fraction of Plasmodium Reads")
# legend(x="topright", col=plasmo[unique(as.numeric(diseaseStatus))], legend=c("malaria", "controls"), pch=15, cex=0.8)
# abline(h=max(control.fract.unmapped.reads))
# dev.off()

# See if the expression of genes in these samples looks different ----------------------

# recalculate lcpm
lcpm.pfpx.yam=cpm(pfpx.yam, log=T)

# falciparum
mostVarFalc=mostVar(lcpm.pfpx.yam[grep("PF3D7|mal", rownames(lcpm.pfpx.yam)),], 1000, i_want_most_var = TRUE)
pdf(paste0(outputdir, "Heatmap_LogCPM_geneMatrix_Falciparum.pdf"), width=15)
Heatmap(mostVarFalc, show_row_names = FALSE, column_title = "Plasmodium falciparum", name = "log2-CPM")
dev.off()

# vivax
mostVarVivax=mostVar(lcpm.pfpx.yam[grep("PVP01", rownames(lcpm.pfpx.yam)),], 4000, i_want_most_var = TRUE)
pdf(paste0(outputdir, "Heatmap_LogCPM_geneMatrix_Vivax.pdf"), width=15)
Heatmap(mostVarVivax, show_row_names = FALSE, column_title = "Plasmodium vivax")
dev.off()

# PCA plotting function
plot.pca <- function(dataToPca, speciesCol, namesPch, sampleNames){
    pca <- prcomp(t(dataToPca), scale=T, center=T)
    pca.var <- pca$sdev^2/sum(pca$sdev^2)
    for (i in 1:9){
        pca_axis1=i
        pca_axis2=i+1
        plot(pca$x[,pca_axis1], pca$x[,pca_axis2], col=speciesCol, pch=namesPch, cex=0.8, xlab=paste0("PC", pca_axis1, " (", round(pca.var[pca_axis1]*100, digits=2), "% of variance)"), ylab=paste0("PC", pca_axis2, " (", round(pca.var[pca_axis2]*100, digits=2), "% of variance)", sep=""), main=name)
        #text(pca$x[,pca_axis1], pca$x[,pca_axis2], labels=colnames(pfpx.yam), pos=3, cex=0.6)
        legend(legend=unique(sampleNames), pch=16, x="topright", col=unique(speciesCol), cex=0.8, title=name, border=F, bty="n")
        }
    return(pca)
}

plot.pca.numeric <- function(dataToPca, speciesCol, namesPch, sampleNames){
    pca <- prcomp(t(dataToPca), scale=T, center=T)
    pca.var <- pca$sdev^2/sum(pca$sdev^2)
    for (i in 1:9){
        pca_axis1=i
        pca_axis2=i+1
        plot(pca$x[,pca_axis1], pca$x[,pca_axis2], col=speciesCol, pch=namesPch, cex=0.8, xlab=paste0("PC", pca_axis1, " (", round(pca.var[pca_axis1]*100, digits=2), "% of variance)"), ylab=paste0("PC", pca_axis2, " (", round(pca.var[pca_axis2]*100, digits=2), "% of variance)", sep=""), main=name)
        #text(pca$x[,pca_axis1], pca$x[,pca_axis2], labels=colnames(pfpx.yam), pos=3, cex=0.6)
        legend(legend=c("high","low"), pch=16, x="topright", col=c(speciesCol[which.max(sampleNames)],speciesCol[which.min(sampleNames)]), cex=0.8, title=name, border=F, bty="n")
        }
    return(pca)
}

# PCA association function
pc.assoc <- function(pca.data){
    all.pcs <- data.frame()
    for (i in 1:ncol(pca.data$x)){
        all.assoc <- vector()
        for (j in 1:ncol(all.covars.df)){
            test.assoc <- anova(lm(pca.data$x[,i] ~ all.covars.df[,j]))[1,5]
            all.assoc <- c(all.assoc, test.assoc)
        }
        single.pc <- c(i, all.assoc)
        all.pcs <- rbind(all.pcs, single.pc)
    }
    names(all.pcs) <- c("PC", colnames(all.covars.df))

    print ("Here are the relationships between PCs and some possible covariates")
    print (all.pcs)
    return (all.pcs)
}

# Prepare covariate matrix
all.covars.df <- pfpx.yam$samples[,covariate.names]

# Plot PCA
for (name in factorVariables){
    if (name=="diseaseStatus"){
      col=plasmo
    } else if (name=="species"){
    col=species.col
    }
    else{
      col = standard_col
    }
    pdf(paste0(outputdir,"pcaresults_",name,".pdf"))
    pcaresults <- plot.pca(dataToPca=lcpm.pfpx.yam, speciesCol=col[as.numeric(get(name))],namesPch=19,sampleNames=get(name))   
    dev.off()
}

# plot numeric variables
for (name in numericVariables){
    initial = .bincode(get(name), breaks=seq(min(get(name), na.rm=T), max(get(name), na.rm=T), len = 80),include.lowest = TRUE)
    bloodCol <- colorRampPalette(c("blue", "red"))(79)[initial]
    pdf(paste0(outputdir,"pcaresults_",name,".pdf"))
    pcaresults <- plot.pca.numeric(dataToPca=lcpm.pfpx.yam, speciesCol=bloodCol,namesPch=as.numeric(pfpx.yam$samples$diseaseStatus) + 14,sampleNames=get(name))
    dev.off()
}

# Get PCA associations
all.pcs <- pc.assoc(pcaresults)
all.pcs$Variance <- pcaresults$sdev^2/sum(pcaresults$sdev^2)

# shut down all devices before plotting heatmap
graphics.off()

# plot pca covariates association matrix to illustrate any potential confounding and evidence for batches
pdf(paste0(outputdir,"significantCovariates_AnovaHeatmap.pdf"))
pheatmap(log(all.pcs[1:5,covariate.names]), cluster_col=F, col= colorRampPalette(brewer.pal(11, "RdYlBu"))(100), cluster_rows=F, main="Significant Covariates \n Anova")
dev.off()

# Write out the covariates:
write.table(all.pcs, file=paste0(outputdir,"pca_covariates_significanceLevels.txt"), col.names=T, row.names=F, quote=F, sep="\t")

# look for sample outliers from PCA
pca.outliers.final=matrix(nrow=0, ncol=3)

for (i in 1:ncol(pcaresults$x)){
    pca.dim=c()
    outlier.sample=c()
    outlier.zscore=c()
    zscore=scale(pcaresults$x[,i])
    outliers=which(abs(zscore) >= 3)
    if (length(outliers) > 0){
        pca.dim=c(pca.dim, i)
        outlier.sample=c(outlier.sample, names(pcaresults$x[,i][outliers]))
        outlier.zscore=c(outlier.zscore, zscore[outliers])
        pca.outliers=matrix(c(rep(pca.dim,length(outlier.sample)), outlier.sample, outlier.zscore), nrow=length(outlier.sample), ncol=3)
        pca.outliers.final=rbind(pca.outliers.final, pca.outliers)
    }
}
colnames(pca.outliers.final)=c("Pca.dim", "Samples", "Z.score")
write.table(pca.outliers.final, file=paste0(outputdir,"sample_outliersInPCA.txt"), quote=F, row.names=F)

# Dissimilarity matrix with euclidean distances
pdf(paste0(outputdir,"SampleDistances_Euclidean.pdf"), height=8, width=15)
par(mar=c(11.1,4.1,4.1,2.1))
eucl.distance <- dist(t(lcpm.pfpx.yam), method = "euclidean")
eucl.cluster <- hclust(eucl.distance, method = "complete")
dend.eucl=as.dendrogram(eucl.cluster)
labels_colors(dend.eucl)=plasmo[as.numeric(diseaseStatus)[order.dendrogram(dend.eucl)]]
plot(dend.eucl, main="log2-CPM \n Euclidean Distances", cex.main=2, cex.axis=1.5)
# also plot the total fraction of reads
labels_colors(dend.eucl)=plasmo[as.numeric(species)[order.dendrogram(dend.eucl)]]
plot(dend.eucl, main="log2-CPM \n Euclidean Distances", cex.main=2, cex.axis=1.5)
dev.off()

# heatmap of lcpm distances
df1=data.frame(species = as.character(species))
ha1 = HeatmapAnnotation(df = df1, col = list(species=c("falciparum" = plasmo[2], "vivax" = plasmo[3], control = plasmo[1])))

# plot
pdf(paste0(outputdir,"lcpmCorrelationHeatmaps.pdf"), height=10, width=15)
Heatmap(cor(lcpm.pfpx.yam,method="pearson"), col=magma(100), column_title = "Pearson Correlation \n log2-CPM", name="Corr Coeff", top_annotation = ha1, show_row_names = FALSE, column_names_gp=gpar(fontsize = 8))
Heatmap(cor(lcpm.pfpx.yam,method="spearman"), col=magma(100), column_title = "Spearman Correlation \n log2-CPM", name="Corr Coeff", top_annotation = ha1, show_row_names = FALSE, column_names_gp=gpar(fontsize = 8))
dev.off()

# Reassigning species
dim1.outliers=colnames(pfpx.yam)[which(pcaresults$x[,1]>100)]
vivax=dim1.outliers

# assign to DF
pfpx.yam$samples[,"species"]=NA
pfpx.yam$samples[controls,"species"]="controls"
pfpx.yam$samples[vivax,"species"]="vivax"
pfpx.yam$samples[which(is.na(pfpx.yam$samples[,"species"])),"species"]="falciparum"
pfpx.yam$samples$species=as.factor(pfpx.yam$samples$species)
species=pfpx.yam$samples$species

# write out species table for downstream use
write.table(pfpx.yam$samples[,c("samples","species")], file=paste0(outputdir,"updatedSpeciesTable.txt"))

# Plot PCA
col=species.col
species=factor(species, levels = c("falciparum", "controls", "vivax"))
name="species"
pdf(paste0(outputdir,"pcaresults_adjustedSpecies_",name,".pdf"))
pcaresults <- plot.pca(dataToPca=lcpm.pfpx.yam, speciesCol=col[as.numeric(get(name))],namesPch=19,sampleNames=get(name))   
dev.off()

# Get PCA associations
all.pcs <- pc.assoc(pcaresults)
all.pcs$Variance <- pcaresults$sdev^2/sum(pcaresults$sdev^2)

# shut down all devices before plotting heatmap
graphics.off()

# plot pca covariates association matrix to illustrate any potential confounding and evidence for batches
pdf(paste0(outputdir,"significantCovariates_AnovaHeatmap_adjustedSpecies.pdf"))
pheatmap(log(all.pcs[1:5,covariate.names]), cluster_col=F, col= colorRampPalette(brewer.pal(11, "RdYlBu"))(100), cluster_rows=F, main="Significant Covariates \n Anova")
dev.off()

# Write out the covariates:
write.table(all.pcs, file=paste0(outputdir,"pca_covariates_significanceLevels.txt"), col.names=T, row.names=F, quote=F, sep="\t")

# Dissimilarity matrix with euclidean distances
pdf(paste0(outputdir,"SampleDistances_Euclidean_adjustedSpecies.pdf"), height=8, width=15)
par(mar=c(6.1,4.1,4.1,2.1))
eucl.distance <- dist(t(lcpm.pfpx.yam), method = "euclidean")
eucl.cluster <- hclust(eucl.distance, method = "complete")
dend.eucl=as.dendrogram(eucl.cluster)
for(name in numericVariables){
    initial = .bincode(get(name), breaks=seq(min(get(name), na.rm=T), max(get(name), na.rm=T), len = 80),include.lowest = TRUE)
    bloodCol <- colorRampPalette(c("blue", "red"))(79)[initial]
    labels_colors(dend.eucl)=bloodCol[order.dendrogram(dend.eucl)]
    plot(dend.eucl, main=name)
}
for(name in factorVariables){
    labels_colors(dend.eucl)=species.col[as.numeric(get(name))][order.dendrogram(dend.eucl)]
    plot(dend.eucl, main=name)
}
dev.off()

# heatmap of lcpm distances
# df1=data.frame(DiseaseStatus = as.character(diseaseStatus))
df1=data.frame(species = as.character(species))
#df2=data.frame(species = as.character(species))
ha1 = HeatmapAnnotation(df = df1, col = list(species=c("falciparum" = plasmo[2], "vivax" = plasmo[3], "controls" = plasmo[1])))
#ha2 = rowAnnotation(df = df2, col= list(species=c("falciparum" = plasmo[1], "vivax" = plasmo[2], control = plasmo[3])))

# plot
pdf(paste0(outputdir,"lcpmCorrelationHeatmaps_adjustedSpecies.pdf"), height=10, width=15)
Heatmap(cor(lcpm.pfpx.yam,method="pearson"), col=magma(100), column_title = "Pearson Correlation \n log2-CPM", name="Corr Coeff", top_annotation = ha1, show_row_names = FALSE, column_names_gp=gpar(fontsize = 8))
Heatmap(cor(lcpm.pfpx.yam,method="spearman"), col=magma(100), column_title = "Spearman Correlation \n log2-CPM", name="Corr Coeff", top_annotation = ha1, show_row_names = FALSE, column_names_gp=gpar(fontsize = 8))
dev.off()

