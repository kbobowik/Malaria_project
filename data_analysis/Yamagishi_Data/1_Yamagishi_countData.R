# by KSB, 26.08.19

# cread in data for the Yamagishi et al 2014 reads from the paper: Interactive transcriptome analysis of malaria patients and infecting Plasmodium falciparum. 

# load packages
library(RColorBrewer)
library(edgeR)
library(plyr)
library(Homo.sapiens)
library(ggsci)
library(scales)

# set up colour palette. The 'NPG' palette (Nature Publising Group) from ggsci will be used for the standard palette
standard_col=c(pal_npg()(10), pal_futurama()(12), pal_tron()(3))
plasmo=brewer.pal(3, "Set2")

# Set paths:
inputdir = "/Users/katalinabobowik/Documents/UniMelb_PhD/Projects/Yamagishi"
outputdir="/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/Output/Malaria/Yamagishi-Analysis/dataPreprocessing/"

# BEGIN ANALYSIS -----------------------------------------------------------

# read in count files for sick samples
files.sick=list.files(path=inputdir, pattern="mismatch3", full.names=T)
files.controls=list.files(path=paste0(inputdir ,"/HealthyControls"), pattern="mismatch3", full.names=T)

# set up DGE matrix combining healthy and sick samples
y <- readDGE(c(files.sick, files.controls), columns=c(1,3)) 
# Organising gene annotation using bioconductor's Homo.sapiens package, version 1.3.1 (Based on genome:  hg19)
geneid <- rownames(y)
genes <- select(Homo.sapiens, keys=geneid, columns=c("SYMBOL", "TXCHROM"), keytype="ENSEMBL")
# Check for and remove duplicated gene IDs, then add genes dataframe to DGEList object
genes <- genes[!duplicated(genes$ENSEMBL),]
y$genes <- genes
dim(y)
# [1] 27413   147

# assign healthy and control samples
y$samples$diseaseStatus="NA"
y$samples$diseaseStatus[grep("Controls", colnames(y))]="control"
y$samples$diseaseStatus[which(y$samples$diseaseStatus=="NA")]="malaria"
# make disease status into factor
y$samples$diseaseStatus=as.factor(y$samples$diseaseStatus)

# Trim file names into shorter sample names and apply to column names
samplenames <- sapply(1:length(colnames(y)), function(x) tail(strsplit(colnames(y)[x],"_")[[1]],1))
colnames(y) <- samplenames

# Get initial statistics before pre-processing -----------------------

# Visualise library size
pdf(paste0(outputdir,"librarysizeYamagishi_preFiltering.pdf"), height=10, width=15)
barplot(y$samples$lib.size*1e-6, ylab="Library size (millions)", cex.names=0.75, col=standard_col[as.numeric(factor(y$samples$diseaseStatus))], names=colnames(y), las=3, ylim=c(0,max(y$samples$lib.size*1e-6)+10))
legend(x="topright", col=standard_col[unique(as.numeric(factor(y$samples$diseaseStatus)))], legend=unique(y$samples$diseaseStatus), pch=15, cex=0.8)
dev.off()

# Total number of genes
pdf(paste0(outputdir,"totalGenesYamagishi_preFiltering.pdf"), height=10, width=15)
barplot(apply(y$counts, 2, function(c)sum(c!=0)), ylab="n Genes", cex.names=0.75, col=standard_col[as.numeric(factor(y$samples$diseaseStatus))], names=colnames(y), las=3, ylim=c(0,max(apply(y$counts, 2, function(c)sum(c!=0)))+2000))
legend(x="topright", col=standard_col[unique(as.numeric(factor(y$samples$diseaseStatus)))], legend=unique(y$samples$diseaseStatus), pch=15, cex=0.8)
dev.off()

# save unfiltered counts file
saveRDS(y$counts, file = paste0(outputdir, "unfiltered_counts.rds"))
# save whole DGE list object as output for downstream analysis
save(y, file = paste0(outputdir, "unfiltered_DGElistObject.Rda"))
