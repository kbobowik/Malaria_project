# Code developed by Katalina Bobowik, 11.03.2019

# Load dependencies and set input paths --------------------------

# Load dependencies:
library(edgeR)
library(plyr)
library(openxlsx)
library(RColorBrewer)
library(magrittr)
library(reshape2)
library(ggplot2)
library(ggsci)
library(viridis)

# set up colour palette. The "wes" palette will be used for island and other statistical information, whereas NineteenEightyR will be used for batch information
wes=c("#3B9AB2", "#EBCC2A", "#F21A00", "#00A08A", "#ABDDDE", "#000000", "#FD6467","#5B1A18")
palette(c(wes, brewer.pal(8,"Dark2")))
dev.off()

# now set up coulor palette for plasmodium
plasmo=brewer.pal(3, "Set2")
species.col=viridis(5)[c(1,3)]

# Set paths:
# inputdir <- "/data/cephfs/punim0586/kbobowik/Sumba/Output/DE_Analysis/123_combined/" # on server
inputdir = "/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/Output/Malaria/Indo-Analysis/dataPreprocessing/"
FeatureCountsDir= "/Users/katalinabobowik/Documents/UniMelb_PhD/Projects/Sumba/FeatureCounts/"
refdir <- "/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/ReferenceFiles/"

# Set output directory and create it if it does not exist:
outputdir <- "/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/Output/Malaria/Plasmodium/Indo/"

if (file.exists(outputdir) == FALSE){
    dir.create(outputdir)
}

# Read in plasmodium count data --------------------------------------------------------------------

# read in count files from featureCounts for plasmodium falciparum and vivax and assign to DGElist object
files.pfpx=list.files(path=paste0(FeatureCountsDir,"PFPX_Combined_IndoSamples"), pattern="Filter_GeneLengthCount_mapq0and255", full.names=T)

# read files into a list
featureCountsOut.plasmo <- lapply(files.pfpx, read.delim)
# transform list into a dataframe
plasmoReads.indo <- data.frame(t(ldply(featureCountsOut.plasmo, "[",,3)))

# set col and row names:
names1st <- paste(sapply(strsplit(basename(files.pfpx[grep("first",basename(files.pfpx))]), "[_.]"), `[`, 8), "firstBatch", sep="_")
names2nd <- paste(sapply(strsplit(basename(files.pfpx[grep("second",basename(files.pfpx))]), "[_.]"), `[`, 8), "secondBatch", sep="_")
names3rd <- paste(sapply(strsplit(basename(files.pfpx[grep("third",basename(files.pfpx))]), "[_.]"), `[`, 8), "thirdBatch", sep="_")

row.names(plasmoReads.indo) <- featureCountsOut.plasmo[[1]]$Geneid
names(plasmoReads.indo) <- c(names1st, names2nd, names3rd)

# rename MPI-336 to MPI-381 (mixup in naming when perfomring sequencing)
colnames(plasmoReads.indo) <- gsub("MPI-336_thirdBatch", "MPI-381_thirdBatch", colnames(plasmoReads.indo)) 

# Into DGEList:
pfpx <- DGEList(plasmoReads.indo, genes=rownames(plasmoReads.indo), samples=colnames(plasmoReads.indo))
dim(pfpx)
# [1] 32232   123

rm(featureCountsOut.plasmo) # clean up, big object

# MPI-296 was found to actually be a female, so we'll take it out of the analysis
pfpx=pfpx[,-which(colnames(pfpx) %in% "MPI-296_firstBatch")]

# Create shortened samplenames- insert hyphen and drop suffixes
samplenames.plas <- as.character(pfpx$samples$samples)
samplenames.plas <- sub("([A-Z]{3})([0-9]{3})", "\\1-\\2", samplenames.plas)
samplenames.plas <- sapply(strsplit(samplenames.plas, "[_.]"), `[`, 1)

# save unfiltered counts file
save(pfpx, file=paste0(outputdir, "Indo.unfiltered_counts.Rda"))

# read in unmapped reads file -----------------------------------------------------------------------

# add in unmapped sample information. This file was created with the script "unmappedReads_TotalReadCounts.sh", which extracts the total number of unmapped reads in each bam file, aligned by STAR. 
number.unmapped.reads.df=read.table(paste0(refdir,"unmappedReads_Counts.txt"))
# first 123 rows are duplicated. get rid of these
number.unmapped.reads.df=number.unmapped.reads.df[124:246,]
# set column names
colnames(number.unmapped.reads.df)=c("Sample.ID","Unmapped.Reads")
number.unmapped.reads.df$Sample.ID=gsub("first_batch", "firstBatch", number.unmapped.reads.df$Sample.ID) %>% gsub("second_batch", "secondBatch", .) %>% gsub("third_batch", "thirdBatch", .)
# rename MPI-336 to MPI-381 (mixup in naming when perfomring sequencing)
number.unmapped.reads.df$Sample.ID=gsub("MPI-336_thirdBatch", "MPI-381_thirdBatch", number.unmapped.reads.df$Sample.ID)
rownames(number.unmapped.reads.df) = number.unmapped.reads.df$Sample.ID
# MPI-296 was found to actually be a female, so we'll take it out of the analysis
number.unmapped.reads.df=number.unmapped.reads.df[-which(rownames(number.unmapped.reads.df) %in% "MPI-296_firstBatch"),]
# See if the total unmapped reads and PFPX file are the same
identical(number.unmapped.reads.df$Sample.ID, colnames(pfpx))
# [1] TRUE

# Read in file sent from Chelzie Crenna and the field sampling crew. All blood was checked for PCR positive Plasmodium Falciparum
samplesheet = read.xlsx(paste0(refdir,"/indoRNA_SequencingFiles/SampleList_120417_v2d.xlsx"),sheet=1, detectDates=T)
samplesheet$Sample.ID=gsub(" ", "-",samplesheet$Sample.ID)

# Remove MPI-296
samplesheet=samplesheet[-which(samplesheet$Sample.ID=="MPI-296"),]

# assign variables and make DF
sample.ID=samplenames.plas
microscopy=samplesheet$Malaria.microscopy[match(samplenames.plas,samplesheet$Sample.ID)]
PCR=samplesheet$Malaria.PCR[match(samplenames.plas,samplesheet$Sample.ID)]
# SMB-PTB-028 was confirmed to have malaria by PCR, however this data isn't in the samplesheet. Let's add it in.
PCR[which(sample.ID %in% "SMB-PTB-028")]="TRUE"
# There are also a lot of 'NA' values in the PCR dataframe. We need to replace these values with 'FALSE'
PCR[which(is.na(PCR))]="FALSE"

# Transform vivax and falciparum reads as fraction of total Hg38 + unmapped reads library size -----------------------------------

# add in the fraction of unmapped reads, which is the total library size for each samples divided by the total number of unmapped reads; i.e., out of all of the unmapped reads, what fraction of maps to plasmodium?
fract.unmapped.reads.pfpx=colSums(pfpx$counts)/number.unmapped.reads.df$Unmapped.Reads

# add this information into the DGE list and save for downstream use
pfpx$samples$fract.unmapped.reads=fract.unmapped.reads.pfpx

# load in human data and get fraction of reads from mapping hg38 data
load(paste0(inputdir,"unfiltered_DGElistObject.Rda"))
# remove MPI-296
y=y[,-which(colnames(y) %in% "MPI-296_firstBatch")]
identical(colnames(pfpx), colnames(y))
# TRUE
pfpx$samples$fract.hg38.libSize=(colSums(pfpx$counts)/y$samples$lib.size)
# add unmapped reads plus Hg38 library size for total library size
pfpx$samples$fract.total.libSize= (pfpx$sample$lib.size) / (number.unmapped.reads.df$Unmapped.Reads + y$samples$lib.size) 
# save malaria metadata information
malaria.summary=data.frame(sample.ID,microscopy,PCR,fract.unmapped.reads.pfpx,pfpx$samples$fract.hg38.libSize, pfpx$samples$fract.total.libSize)
write.table(malaria.summary, file=paste0(outputdir,"Malaria_summary_table.txt"), quote=F, row.names=F, col.names=T, sep="\t")

# Identify sick samples ---------------------------------------------------

# Get number of reads in samples already designated as having malaria
confirmed.PCR=rownames(pfpx$samples)[which(PCR=="TRUE")]
malaria.libSize=pfpx$samples[confirmed.PCR,"lib.size"]
range(malaria.libSize)
# 7874 868762
min(malaria.libSize)
# [1] 7874
malaria.fract=pfpx$samples[confirmed.PCR,"fract.total.libSize"]
range(malaria.fract)
# 0.0004013708 0.0433233797

# Get number of genes
malaria.geneSize=apply(pfpx$counts, 2, function(c)sum(c!=0))[confirmed.PCR]
range(malaria.geneSize)
# 807 4589
min(malaria.geneSize)
# 807

# which samples have the same fraction of Plasmodium reads as the minimum sample confirmed by PCR?
samples.fract=which(pfpx$samples$fract.total.libSize  >= min(malaria.fract))
samples.Genes=which(apply(pfpx$counts, 2, function(c)sum(c!=0)) >= min(malaria.geneSize))
malariaSamples=colnames(pfpx)[intersect(samples.fract,samples.Genes)]
malariaSamples
# [1] "MPI-025_firstBatch"    "MPI-061_firstBatch"    "MPI-376_firstBatch"   
# [4] "MPI-334_secondBatch"   "MPI-345_secondBatch"   "SMB-PTB028_thirdBatch"

# add in sick samples into plasmodium DGE list object
pfpx$samples$diseaseStatus=NA
pfpx$samples$diseaseStatus[which(colnames(pfpx) %in% malariaSamples)]="malaria"
pfpx$samples$diseaseStatus[which(is.na(pfpx$samples$diseaseStatus))]="control"

# plot total number of genes in whole library
pdf(paste0(outputdir,"TotalGenes_Indo.pdf"), height=8, width=15)
par(oma=c(5,0,0,0))
barplot(apply(pfpx$counts, 2, function(c)sum(c!=0)), las=3, main="Total Genes", col=plasmo[as.numeric(as.factor(pfpx$samples$diseaseStatus))])
legend(x="topright", col=plasmo[c(1:2)], legend=levels(as.factor(pfpx$samples$diseaseStatus)), pch=15)
dev.off()

# make table of disease status summary statistics and save
diseaseStatus_identification=data.frame(pfpx$samples$fract.total.libSize, apply(pfpx$counts, 2, function(c)sum(c!=0)), pfpx$samples$diseaseStatus)
colnames(diseaseStatus_identification) = c("totalLibSize","totalGenes","diseaseStatus")
# make library size into percentage values by multiplying by 100, then limit to two decomal places
diseaseStatus_identification$totalLibSize = round(diseaseStatus_identification$totalLibSize * 100,2)
write.table(diseaseStatus_identification, file=paste0(outputdir,"diseaseStatusReassignment_Indo.txt"), sep="\t",quote=F)

# save whole DGE list object
save(pfpx, file=paste0(outputdir,"PlasmodiumCounts_unfiltered_Indo_fractMappedReads.Rds"))

# Data visualisation of healthy and sick samples ----------------------------------------------

# plot vivax vs falciparum in a stacked barplot
a=sapply(1:ncol(pfpx$counts), function(x)sum(pfpx$counts[,x][grep("PF3D7|mal",names(pfpx$counts[,x]))])/(number.unmapped.reads.df$Unmapped.Reads + y$samples$lib.size)[x])
b=sapply(1:ncol(pfpx$counts), function(x)sum(pfpx$counts[,x][grep("PVP01",names(pfpx$counts[,x]))])/(number.unmapped.reads.df$Unmapped.Reads + y$samples$lib.size)[x])
data=data.frame(a,b)
data=t(data)
colnames(data)=colnames(pfpx$counts)
rownames(data)=c("Falciparum","Vivax")
malariaSamples=samplenames.plas[intersect(samples.fract,samples.Genes)]

# Make a stacked barplot of the fraction of unmapped reads
pdf(paste0(outputdir,"FractionReadsMappingToPlasmodium_NormalisedByTotalReads_VivaxAndFalciparumSeparated.pdf"), height=10, width=15)
par(oma=c(5,0,0,0))
b=barplot(data, col=species.col, border="white", las=3, main="Fraction of Total Reads")
text(b,c(data[1,] + data[2,]),ifelse(samplenames.plas %in% malariaSamples,"*",""),pos=3,cex=1.5,xpd=NA)
legend(x="topright", col=species.col, legend=c("Falciparum","Vivax"), pch=15, cex=0.8)
dev.off()

#Transform this data into %
pdf(paste0(outputdir,"FractionReadsMappingToPlasmodium_NormalisedByTotalReads_Percentage__VivaxAndFalciparumSeparated.pdf"), height=10, width=15)
data_percentage=apply(data, 2, function(x){x*100/sum(x,na.rm=T)})
par(oma=c(5,0,0,0))
b=barplot(data_percentage, col=species.col, border="white", las=3)
text(b,data_percentage[1,],ifelse(samplenames.plas %in% malariaSamples,"*",""),pos=3,cex=1.5,xpd=NA)
legend(x="topright", col=species.col, legend=c("Falciparum","Vivax"), pch=15, cex=0.8)
dev.off()

# make a barplot of just the malaria samples
pdf(paste0(outputdir,"FractionReadsMappingToPlasmodium_NormalisedByTotalReads_VivaxAndFalciparumSeparated_JustSickSamples.pdf"), height=10, width=15)
par(oma=c(5,0,0,0))
# rename malaria samples
malariaSamples=colnames(pfpx)[intersect(samples.fract,samples.Genes)]
b=barplot(data[,malariaSamples], col=species.col, border="white", las=3, main="Fraction of Total Reads")
legend(x="topright", col=species.col, legend=c("Falciparum","Vivax"), pch=15, cex=0.8)
dev.off()

# Where are the falciparum and vivax reads mapping to? ------------------------------------

for(species in c("Falciparum", "Vivax")){
  merged_PlasmoCounts=melt(pfpx$counts)
  colnames(merged_PlasmoCounts)=c("gene","sample","value")
  merged_PlasmoCounts_filtered=merged_PlasmoCounts[which(merged_PlasmoCounts[,"value"] != 0),]
  rm(merged_PlasmoCounts)
  # insert species column
  merged_PlasmoCounts_filtered$species=rep(NA,nrow(merged_PlasmoCounts_filtered))
  merged_PlasmoCounts_filtered$species[grep("mal|PF3D7", merged_PlasmoCounts_filtered$gene)]="Falciparum"
  merged_PlasmoCounts_filtered$species[grep("PVP01", merged_PlasmoCounts_filtered$gene)]="Vivax"
  merged_PlasmoCounts_filtered=merged_PlasmoCounts_filtered[merged_PlasmoCounts_filtered$species==species,]
  # order plasmo counts by sample ID, then by the descending value of counts
  ordered_PlasmoCounts=merged_PlasmoCounts_filtered[with(merged_PlasmoCounts_filtered, order(merged_PlasmoCounts_filtered$sample, -merged_PlasmoCounts_filtered$value)), ]
  # add in total number of unmapped reads information in order to normalise
  ordered_PlasmoCounts$unmappedReads="NA"
  rownames(number.unmapped.reads.df)=number.unmapped.reads.df$Sample.ID
  for(sample in number.unmapped.reads.df$Sample.ID){
   ordered_PlasmoCounts$unmappedReads[which(sample == ordered_PlasmoCounts$sample)] = number.unmapped.reads.df[sample,"Unmapped.Reads"]
  }
  ordered_PlasmoCounts$normalisedReads=ordered_PlasmoCounts$value/as.numeric(ordered_PlasmoCounts$unmappedReads)
  # save table information
  write.table(ordered_PlasmoCounts, file=paste0(outputdir,species,"_orderedPlasmoCounts_IndoSamples.txt"))
  # plot top genes for each sample
  for(n in c(1,2,3,5,10)){
    topGenes_Plasmo <- by(ordered_PlasmoCounts, ordered_PlasmoCounts["sample"], head, n=n)
    # transform this back into a dataframe
    assign(species, Reduce(rbind, topGenes_Plasmo))
    pdf(paste0(outputdir,"topGenes_",species,"_IndoSamples_Plasmo_",n,".pdf"), height=8, width=20)
    # look at fraction
    print(ggplot(get(species),aes(x=factor(sample), y=normalisedReads)) + geom_col(aes(fill=factor(get(species)$gene))) + theme(axis.text.x=element_text(angle=90,hjust=1,vjust=0.5)) + xlab("Sample ID") + labs(fill = "gene ID") + ggtitle(paste0("Fraction of reads mapping to P. ", species)))
    # also look at raw value of counts
    print(ggplot(get(species),aes(x=factor(sample), y=value)) + geom_col(aes(fill=factor(get(species)$gene))) + theme(axis.text.x=element_text(angle=90,hjust=1,vjust=0.5)) + xlab("Sample ID") + labs(fill = "gene ID") + ggtitle(paste0("Number of reads mapping to P. ", species)))
    dev.off()
  }
}

# Most reads are mapping to exon_PVP01_0010550-E1. Let's take this out and replot to see how it looks like
pfpx=pfpx[-which(rownames(pfpx) %in% "exon_PVP01_0010550-E1"),]
a=sapply(1:ncol(pfpx$counts), function(x)sum(pfpx$counts[,x][grep("PF3D7|mal",names(pfpx$counts[,x]))])/(number.unmapped.reads.df$Unmapped.Reads + y$samples$lib.size)[x])
b=sapply(1:ncol(pfpx$counts), function(x)sum(pfpx$counts[,x][grep("PVP01",names(pfpx$counts[,x]))])/(number.unmapped.reads.df$Unmapped.Reads + y$samples$lib.size)[x])
data=data.frame(a,b)
data=t(data)
colnames(data)=colnames(pfpx$counts)
rownames(data)=c("Falciparum","Vivax")
malariaSamples=samplenames.plas[intersect(samples.fract,samples.Genes)]

# Make a stacked barplot of the fraction of unmapped reads
pdf(paste0(outputdir,"FractionReadsMappingToPlasmodium_NormalisedByTotalReads_VivaxAndFalciparumSeparated_noExonPVP01_0010550.pdf"), height=10, width=15)
par(oma=c(5,0,0,0))
b=barplot(data, col=species.col, border="white", las=3, main="Fraction of Total Reads")
text(b,c(data[1,] + data[2,]),ifelse(samplenames.plas %in% malariaSamples,"*",""),pos=3,cex=1.5,xpd=NA)
legend(x="topright", col=species.col, legend=c("Falciparum","Vivax"), pch=15, cex=0.8)
dev.off()
