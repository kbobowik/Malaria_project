This folder contains files related to data analysis for the Indonesian dataset collected by the Eijkman Institute. 

Files:

blood_deconvolution_DeconCell.R is the script used to perform blood deconvolution on whole blood using Decon Cell. 

blood_deconvolution_pseudoBulkdata_Indo_CIBERSORT - script used to prepare data for CIBERSORT

countData_123_combined.R is the script used to read count files (created by FeatureCounts) into R.

dataExploration_123Combined.R is the script used to perform data exploration of the preprocessed Indonesian data

dataPreprocessing_123_combined.R is the script used to perform data preprocessing on the samples. 

DE_DIseaseStatus_123_combined_noTreat_batchCorrectionUsingLMandBloodAsCovar_withBlocking.R is the script used to perform differential expression on healthy and sick samples within the Indonesian dataset.

Indo_CovariateData.R - script used to obtain data for all covariates used in the study. 

Indonesian_SexIdentification.R is the script used to identify sex based on expression profiles of marker genes from the paper "Robust and tissue-independent gender-specific transcript biomarkers".

Indonesian-PlasmoCounts-DiseaseStatusIdentification.R is the script used to read in count data for the plasmodium reads and get initial disease classification of samples. 

Indonesian-PlasmodiumData.R is the script used for clustering analysis and plasmodium data exploration. 