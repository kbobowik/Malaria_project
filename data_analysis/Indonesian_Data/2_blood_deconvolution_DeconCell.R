# Test out DeconCell for deconvoluting blood using RNASeq data from 123 Indonesian samples
# Code developed by Katalina Bobowik, 26.02.2019
# following the vignette as per: http://htmlpreview.github.io/?https://github.com/molgenis/systemsgenetics/blob/master/Decon2/DeconCell/inst/doc/my-vignette.html
# and the vignette: https://github.com/molgenis/systemsgenetics/tree/master/Decon2

# load packages
library(devtools)
library(DeconCell)
library(edgeR)
library(tidyverse)
library(ghibli)
library(Rcmdr)
library(RColorBrewer)
library(stringr)
library(matrixStats)
library(NineteenEightyR)

# Set paths:
inputdir = "/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/Output/Malaria/Indo-Analysis/dataPreprocessing/"
outputdir= "/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/Output/Malaria/Indo-Analysis/Deconvolution/"

# set colours
batch.col=electronic_night(n=3)

# load count data
load(paste0(inputdir, "unfiltered_DGElistObject.Rda"))

# assign count table This is y$counts, which has not been filtered for lowly-expressed genes
count.table=y$counts
dim(y$counts)
# [1] 27413   123

# load the model. Models were built using an elastic net and training in 95 healthy dutch volunteers from the 500FG cohort with FACS quantification of 73 circulating cell subpopulations
data("dCell.models")

# Normalize gene expression: approximate the data to a normal-like distribution and account for library sizes by using the dCell.expProcessing function. This function will perform a TMM normalization, a log2(counts+1), and scale (z-transformation) per gene.
dCell.exp <- dCell.expProcessing(count.table, trim = TRUE)
# [INFO]	 Total of 61.13 % genes from dCell are found>

# predict cell counts
prediction <- dCell.predict(dCell.exp, dCell.models, res.type = "median")

# first select all cell types
all.predicted.cellcounts=prediction$dCell.prediction

# then just select relevant cell types
predicted.cellcounts <- prediction$dCell.prediction[,c('Granulocytes','B cells (CD19+)','CD4+ T cells','CD8+ T cells','NK cells (CD3- CD56+)','Monocytes (CD14+)')]

# scale to sum to 100
# predicted.cellcounts.scaled <- (predicted.cellcounts/rowSums(predicted.cellcounts))*100

# save both tables 
write.table(all.predicted.cellcounts, file=paste0(outputdir,"AllPredictedCellCounts_DeconCell.txt"), sep="\t")
write.table(predicted.cellcounts, file=paste0(outputdir,"predictedCellCounts_DeconCell.txt"), sep="\t")

# plot percentages of each cell type
axiscolors=batch.col[as.numeric(y$samples$batch[match(colnames(t(predicted.cellcounts)), rownames(y$samples))])]
pdf(paste0(outputdir,"DeconCell_RNASeqDeconvolution.pdf"), height=10,width=16)
par(mar=c(3,4.1,10.1,2.1),xpd=T)
x=barplot(t(predicted.cellcounts), cex.axis=1.5, axisnames = FALSE, col=c("#ffd92f","#e78ac3","#fc8d62","#66c2a5","#8da0cb","#a6d854"), las=3, ylim=c(0,110), cex.main=1.5, main="Indonesian Cell Proportion")
#text(labels=rep("°",length(colnames(y))), col=axiscolors, x=x, y=0, srt = 1, pos = 1, xpd = TRUE, cex=1.5)
legend(123,135,legend=colnames(predicted.cellcounts), col=c("#ffd92f","#e78ac3","#fc8d62","#66c2a5","#8da0cb","#a6d854"), pch=15, cex=1.2)
dev.off()

shortenedCellnames  <- paste(word(colnames(prediction$dCell.prediction), 1), word(colnames(prediction$dCell.prediction), 2),sep=" ")
shortenedCellnames = gsub("NA", "", shortenedCellnames)
col=brewer.pal(8,"Dark2")
pdf(paste0(outputdir,"allCelltypesBoxplot.pdf"), height=20,width=10)
par(mfrow=c(3,1))
for (island in c("SMB","MTW","MPI")){
	median=cbind(colnames(prediction$dCell.prediction),colMedians(prediction$dCell.prediction[grep(island,rownames(prediction$dCell.prediction)),]))
	write.table(median, file=paste0(outputdir,island,"_medianValues_allCellTypes.txt"))
	mean=colMeans(prediction$dCell.prediction[grep(island,rownames(prediction$dCell.prediction)),])
	write.table(mean, file=paste0(outputdir,island,"_meanValues_allCellTypes.txt"))
	boxplot(prediction$dCell.prediction[grep(island,rownames(prediction$dCell.prediction)),], las=3, col=col, main=island, cex.names=0.5, names=shortenedCellnames)
}
dev.off()
