# created by KSB, 29.03.2019
# use the genome aligner STAR to map all Yamagishi healthy samples and controls

# load modules
module load STAR

genomeDir=/data/cephfs/punim0586/kbobowik/genomes
mainDir=/data/cephfs/punim0586/kbobowik
star=/data/cephfs/punim0586/kbobowik/STAR/Hg38_SecondPass

# combine PFalciparum and PVivax genomes
cat ${genomeDir}/PlasmoDB_36_Pfalciparum3D7_Genome.fasta ${genomeDir}/PlasmoDB_36_PvivaxP01_Genome.fasta > ${genomeDir}/combined_PFalc3D7_PvivaxP01_Genome.fasta

# Generate genome indices
# Note: jobs will get killed of you don't ask for enough memory. I found asking for at least 12 CPUs and 200,000 MB was sufficient

# the '--sjdbGTFtagExonParentTranscript Parent' flag makes STAR work with gff files
STAR --runMode genomeGenerate --genomeDir ${mainDir}/STAR/Yamagishi_CombinedPFPX --genomeFastaFiles ${genomeDir}/combined_PFalc3D7_PvivaxP01_Genome.fasta --sjdbGTFfile ${genomeDir}/combined_PFalc3D7_PvivaxP01_GFF.gff --sjdbOverhang 35 --runThreadN 12 --sjdbGTFtagExonParentTranscript Parent

# First Pass- map all unmapped reads from yamagishi study
for file in ${star}/Yamagishi/{Controls,Sick}/unmapped*.fastq; do
  sample=`basename $file _trimmed.fastq.gz`
  healthStatus=`echo $file | cut -d/ -f 9`
  if [[ $healthStatus == "Controls" ]]; then
  	echo STAR --genomeDir ${mainDir}/STAR/Yamagishi_CombinedPFPX --readFilesIn $file --runThreadN 12 --sjdbOverhang 35 --outFileNamePrefix ${mainDir}/STAR/Yamagishi_CombinedPFPX/Controls/STAR_PXPFCombined_${healthStatus}_${sample} --outSAMtype BAM SortedByCoordinate --genomeSAindexNbases 11 --alignIntronMax 35000 --alignMatesGapMax 35000
  else
  	echo STAR --genomeDir ${mainDir}/STAR/Yamagishi_CombinedPFPX --readFilesIn $file --runThreadN 12 --sjdbOverhang 35 --outFileNamePrefix ${mainDir}/STAR/Yamagishi_CombinedPFPX/Sick/STAR_PXPFCombined_${healthStatus}_${sample} --outSAMtype BAM SortedByCoordinate --genomeSAindexNbases 11 --alignIntronMax 35000 --alignMatesGapMax 35000
  fi
done > ${mainDir}/Sumba/scripts/Array_Scripts/STARArrayTable_YamagishiSamples_PXPFCombined.txt

# combine all of the SJ.out.tab files from from all files created in first pass into one file 
cat ${mainDir}/STAR/Yamagishi_CombinedPFPX/{Controls,Sick}/*.out.tab > ${mainDir}/STAR/Yamagishi_CombinedPFPX/Sick/STAR_PFPX_allFilesSJ.out.tab

# generate genomice indices for the second time
STAR --runMode genomeGenerate --genomeDir ${mainDir}/STAR/Yamagishi_CombinedPFPX_SecondPass --genomeFastaFiles ${genomeDir}/combined_PFalc3D7_PvivaxP01_Genome.fasta --sjdbFileChrStartEnd ${mainDir}/STAR/Yamagishi_CombinedPFPX/Sick/STAR_PFPX_allFilesSJ.out.tab --sjdbGTFfile ${genomeDir}/combined_PFalc3D7_PvivaxP01_GFF.gff --sjdbOverhang 35 --runThreadN 12 --sjdbGTFtagExonParentTranscript Parent

# Map the reads for the second pass
for file in ${star}/Yamagishi/{Controls,Sick}/*.fastq; do
  sample=`basename $file _trimmed.fastq.gz`
  healthStatus=`echo $file | cut -d/ -f 9`
  if [[ $healthStatus == "Controls" ]]; then
  	echo STAR --genomeDir ${mainDir}/STAR/Yamagishi_CombinedPFPX_SecondPass --readFilesIn $file --runThreadN 12 --sjdbOverhang 35 --outFileNamePrefix ${mainDir}/STAR/Yamagishi_CombinedPFPX_SecondPass/Controls/STAR_PXPFCombined_${healthStatus}_${sample} --outSAMtype BAM SortedByCoordinate --genomeSAindexNbases 11 --alignIntronMax 35000 --alignMatesGapMax 35000
  else
  	echo STAR --genomeDir ${mainDir}/STAR/Yamagishi_CombinedPFPX_SecondPass --readFilesIn $file --runThreadN 12 --sjdbOverhang 35 --outFileNamePrefix ${mainDir}/STAR/Yamagishi_CombinedPFPX_SecondPass/Sick/STAR_PXPFCombined_${healthStatus}_${sample} --outSAMtype BAM SortedByCoordinate --genomeSAindexNbases 11 --alignIntronMax 35000 --alignMatesGapMax 35000
  fi
done > ${mainDir}/Sumba/scripts/Array_Scripts/STARArrayTable_YamagishiSamples_PXPFCombined_SecondPass.txt

# filter for rows in each file which have field five (MAPQ score) equal to 255 (uniquely mapped) or 0 (mapped equally well elsewhere)

input_dir="/data/cephfs/punim0586/kbobowik/STAR/Yamagishi_CombinedPFPX_SecondPass/Controls"

for file in ${input_dir}/STAR*.bam; do
  filename=`basename $file`
  pathname=`dirname $file`
  #samtools view $file | awk '{OFS="t"; if($15 == "nM:i:0" || $15 == "nM:i:1" || $15 == "nM:i:2" || $15 == "nM:i:3") {print}}' | awk '{OFS="t"; if($5 == 0 || $5 == 255){print}}' > ${pathname}/mapq0and255_${filename}
  echo samtools view $file \| awk \'{OFS=\"\t\"\; if\(\$15 == \"nM:i:0\" \|\| \$15 == \"nM:i:1\" \|\| \$15 == \"nM:i:2\" \|\| \$15 == \"nM:i:3\"\) {print}}\' \| awk \'{OFS=\"\t\"\; if\(\$5 == 0 \|\| \$5 == 255\){print}}\' \> ${pathname}/mapq0and255_${filename}
done > /data/cephfs/punim0586/kbobowik/Malaria/Array_Scripts/BAMArray_mpaq255and0.txt