# Created 14.02.19 by KSB
 
# first, laod the SRA-Toolkit module
module load SRA-Toolkit
module load web_proxy

# set outdir
outdir=/data/cephfs/punim0586/kbobowik/Yamagishi/data

# First, download accession list (SRR_Acc_List.txt under 'Download') from ncbi: https://www.ncbi.nlm.nih.gov/Traces/study/?acc=DRP001953

# download each sample by reading line by line from accession list file, then run as an array script (Yamagishi_Controls_Array.txt)
for i in `cat SRR_Acc_List.txt`; do
	fastq-dump --gzip --outdir $outdir $i
done
