# get number of reads at all stages

# First, number of raw reads

rawDir="/data/cephfs/punim0586/kbobowik/Yamagishi/data"
summaryDir="/data/cephfs/punim0586/kbobowik/Malaria/SummaryStats"
arrayDir="/data/cephfs/punim0586/kbobowik/Malaria/Array_Scripts"
trimmedDir="/data/cephfs/punim0586/kbobowik/processed_data/Yamagishi"
starDir="/data/cephfs/punim0586/kbobowik/STAR/Hg38_SecondPass/Yamagishi"
fcDir="/data/cephfs/punim0586/kbobowik/Sumba/FeatureCounts/Yamagishi"

# get raw reads
for file in ${rawDir}/{Controls,Sick}/*.fastq.gz; do
	shortenedFile=`basename $file .fastq.gz`
	diseaseStatus=`echo $file | awk -F'[/.]' '{print $8}'`
	echo "printf \"\`echo -e \$(zcat $file|wc -l)/4|bc\` $diseaseStatus $shortenedFile\n\"" '>' ${summaryDir}/${shortenedFile}_totalRawReads.txt
done > ${arrayDir}/rawReadsArray.txt
# RawReadsCounts_Combined.txt

# get trimmed reads
for file in ${trimmedDir}/{controls,Sick}/*.fastq.gz; do
	shortenedFile=`basename $file .fastq.gz`
	sampleID=${shortenedFile%_*}
	diseaseStatus=`echo $file | awk -F'[/.]' '{print $8}'`
	echo "printf \"\`echo -e \$(zcat $file|wc -l)/4|bc\` $diseaseStatus $sampleID\n\"" '>' ${summaryDir}/${sampleID}_totalTrimmedReads.txt
done > ${arrayDir}/trimmedReadsArray.txt
# trimmedReadsCounts_Combined.txt

# get number of reads after STAR
for file in ${starDir}/{Controls,Sick}/STAR*Log.final.out; do
	shortenedFile=`basename $file Log.final.out`
	sampleID=${shortenedFile##*_}
	diseaseStatus=`echo $file | awk -F'[/.]' '{print $9}'`
	grepNumber=`grep "Uniquely mapped reads number" $file`
	uniquelyMappedNumber=`echo $grepNumber | cut -f6 -d' '`
	printf "$uniquelyMappedNumber $diseaseStatus $sampleID \n"
done > ${summaryDir}/STAR_UniquelyMappedReadsNumbers.txt

# featureCounts Numbers
for file in ${fcDir}/{Controls,sample_counts}/Filter_GeneLengthCount_mismatch3*.txt; do
	shortenedFile=`basename $file .txt`
	sampleID=${shortenedFile##*_}
	diseaseStatus=`echo $file | cut -d/ -f 9`
	if [[ $diseaseStatus == "sample_counts" ]]; then
		diseaseStatus="Sick"
	fi
	nReads=`tail -n +2 $file | cut -f3 | paste -sd+ | bc`
	printf "$nReads $diseaseStatus $sampleID \n"
done > ${summaryDir}/FeatureCounts_AssignedReadsNumbers.txt

