# created 26.08.2019

# set directory names
dir="/data/cephfs/punim0586/kbobowik/Yamagishi/data"
outputdir="/data/cephfs/punim0586/kbobowik/Malaria/flowcell_batch_info"

# get instrument name and flowcell name from fastq files
for file in ${dir}/{Sick,Controls}/*.fastq.gz; do
  sample=`basename $file .fastq.gz`
  # this command takes roughly 2 minutes for each file
  echo "zcat $file | grep \"length\" | cut -d ' ' -f2 | cut -d ":" -f1,2 | sort | uniq > ${outputdir}/${sample}_instrumentPlusBatch.txt"
done > /data/cephfs/punim0586/kbobowik/Malaria/Array_Scripts/lanePlusinstrimentInfo_Array.txt

# Get sample name and merge all data together
for file in ${outputdir}/*.txt; do
  sample=`basename $file _instrumentPlusBatch.txt`
  echo -n $sample && echo -ne "\t" && column $file -s "\t"
done >> ${outputdir}/allSamples_flowcellBatch.txt

# Also make text file of just instrument information
for file in ${outputdir}/*instrumentPlusBatch.txt; do
  sample=`basename $file _instrumentPlusBatch.txt`
  instrument=`cat $file | head -n 1 | cut -d ":" -f1`
  echo -n $sample && echo -ne "\t" && echo -e $instrument
done >> ${outputdir}/Yamagishi_allSamples_instrument.txt

