# Script for creating barplot of number of reads at all different processing stages for Indonesian dataset
# Code developed by Katalina Bobowik, 10.12.2018

# load library and colour palette
library(viridis)
library(Rcmdr)
library(DescTools)
library(ggsci)
library(scales)
library(RColorBrewer)

# colour palette
plasmo=brewer.pal(3, "Set2")

# set working directory
inputdir="/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/Output/Malaria/"
outputdir="/Users/katalinabobowik/Documents/UniMelb_PhD/Analysis/UniMelb_Sumba/Output/Malaria/QC/"

# read in summary file and tidy up 
a=read.table(paste0(inputdir,"QC/TotalnReads_AllSamples_Yamagishi.txt"), header=T, sep="\t")
rownames(a)=as.character(a$Sample)
# clear Sample column, as it's not needed
a$Sample = NULL

# Percentages ------------------------------------------------

# divide all rows by the first column, which is the total number of reads
b=t(apply(a[,1:4], 1, function(x){x/x[1]}))
# now subtract each column percentage from the previous column in order to get the cumulative number of reads
for(column in c(1:3)){
	b[,column]=b[,column]-b[,column+1]
}

# plot both n reads and percentages onto one pdf
pdf(paste0(outputdir,"TotalandPercentageOfReads_allFilteringStages_Yamagishi.pdf"), height=15,width=18)
par(mar=c(6.1,4.1,10.1,2.1), xpd=T, mfrow=c(2,1))
# first plot total number of reads
x=barplot(as.matrix(t(a)*1e-6), col=viridis(4), cex.main=1.5, cex.lab=1.5, cex.axis=1.5, border="white", axisnames = FALSE, las=3, main="Number of Reads \nAll filtering stages", ylab="Number of Reads (millions)")
# add labels
# load in Yamagishi DGE list in order to get disease status information
load(paste0(inputdir, "Yamagishi-Analysis/dataPreprocessing/unfiltered_DGElistObject.Rda"))
axiscolors=plasmo[as.numeric(y$samples$diseaseStatus[match(rownames(a), rownames(y$samples))])]
text(labels=rep("°",nrow(a)), col=axiscolors, x=x, y=0, srt = 1, pos = 1, xpd = TRUE)
legend(length(x),350, col=viridis(4), legend=colnames(a), pch=15, cex=1.5)
# now plot reads as a percentage
x=barplot(Rev(t(b),margin=1),col=Rev(viridis(4)),cex.axis=1.5, cex.main=1.5, cex.lab=1.5,border="white", axisnames = FALSE, las=3,main="Percentage of Reads \nAll filtering stages", ylab="Percentage of Reads")
text(labels=rep("°",nrow(a)), col=axiscolors, x=x, y=0, srt = 1, pos = 1, xpd = TRUE)
dev.off()

# plot just n reads
pdf(paste0(outputdir,"TotalReads_allFilteringStages_Yamagishi.pdf"), height=10,width=18)
par(mar=c(8.1,6.1,10.1,2.1), xpd=T)
x=barplot(as.matrix(t(a)*1e-6), axisnames = FALSE, col=viridis(4), cex.main=2, cex.lab=2, cex.axis=2, border="white", las=3, main="Yamagishi Reads", ylab="Number of Reads (millions)")
#text(labels=rep("°",nrow(a)), col=axiscolors, x=x, y=0, srt = 0, pos = 1, xpd = TRUE, cex=2)
legend(length(x),350, col=viridis(4), legend=colnames(a), pch=15, cex=1.5)
dev.off()

