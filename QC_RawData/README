# Quality control steps

This folder contains scripts for perfomring some basic quality control steps on the raw RNA-seq data

# FastQC

For the first quality control step, FastQC was run in order to ensure there were no major problems in the data that might affect downstream analyses. 
FastQC v0.11.5 for Linux was downlaoded from Babraham Bioninformatics: https://www.bioinformatics.babraham.ac.uk/projects/download.html#fastqc. FastQC quality check was performed on both globin-depleted (primary data) and globin samples (free, test data).

On all samples, FastQC is run twice: once for an initial sample QC check and a second time after trimming. In this script, a summary file is also made from the FastQC output, however MultiQC is also used for summary information. 

# Trimmomatic

Trimmomatic, version 0.36, was downlaoded from binary at http://www.usadellab.org/cms/?page=trimmomatic. All information on parameters can be found in the Trimmomatic manual: http://www.usadellab.org/cms/uploads/supplementary/Trimmomatic/TrimmomaticManual_V0.32.pdf

Leading and trailing bases are removed if below a quality of 20 and the minimum length is kept at 90 bp (i.e., in the Trimmomatic terminology: LEADING:20 TRAILING:20 MINLEN:90). 

This script was run for all samples as an array script using sbatch. All shell scripts can be found on Spartan under the path '/data/cephfs/punim0586/kbobowik/Sumba/scripts/shell_scripts' with 'FastQC' and 'Trimmomatic' prefix.

Files:

numberOfReads_AllStages.R is the script used to get summary statistics on the number of reads at all stages

QC_Pipeline_indoPapuanSamples.sh is the QC pipeline used on the Indonesian dataset

QC_Pipeline_YamagishiControls.sh is the QC pipeline used on the Yamagishi control dataset

QC_Pipeline_YamagishiReads.sh is the QC pipeline used on the Yamagishi malaria dataset

SequenceBatch_plus_Flowcell_extraction_YAmagishi.sh is teh script used to get the batch plus flowcell information from the yamagishi reads