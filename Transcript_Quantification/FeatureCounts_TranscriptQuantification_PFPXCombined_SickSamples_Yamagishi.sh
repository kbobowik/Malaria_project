# created by KSB, 13.03.2018

# quantify the abundance of transcripts from the STAR alignment of plasmodium using FeatureCounts
# The Subread package was downlaoded from Sourceforge for Linux, version 1.5.3 (subread-1.5.3-Linux-x86_64.tar.gz)

# load modules
module load SAMtools
module load Subread

input_dir="/data/cephfs/punim0586/kbobowik/STAR/Yamagishi_CombinedPFPX_SecondPass/Sick"

# filter for rows in each file which have field five (MAPQ score) equal to 255 (uniquely mapped) or 0 (mapped equally well elsewhere)
for file in ${input_dir}/STAR*.bam; do
	filename=`basename $file`
	pathname=`dirname $file`
	#samtools view $file | awk '{OFS="t"; if($15 == "nM:i:0" || $15 == "nM:i:1" || $15 == "nM:i:2" || $15 == "nM:i:3") {print}}' | awk '{OFS="t"; if($5 == 0 || $5 == 255){print}}' > ${pathname}/mapq0and255_${filename}
	echo samtools view $file \| awk \'{OFS=\"\t\"\; if\(\$15 == \"nM:i:0\" \|\| \$15 == \"nM:i:1\" \|\| \$15 == \"nM:i:2\" \|\| \$15 == \"nM:i:3\"\) {print}}\' \| awk \'{OFS=\"\t\"\; if\(\$5 == 0 \|\| \$5 == 255\){print}}\' \> ${pathname}/mapq0and255_${filename}
done > /data/cephfs/punim0586/kbobowik/Malaria/Array_Scripts/BAMArray_mpaq255and0.txt

# quantify the abundance of transcripts from the STAR alignment of Yamagishi reads to the human genome (hg38) using featureCounts

# execute FeatureCounts 
# here are the following flags I used:
# -T: Number of the threads.  The value should be between 1 and 32.  1 by default.
# -s: Indicate if strand-specific read counting should be performed. Acceptable  values:  0  (unstranded),  1  (stranded)  and  2  (re-versely stranded).  0 by default.  For paired-end reads, strand of the first read is taken as the strand of the whole fragment. FLAG field is used to tell if a read is first or second read in a pair.
# -p: If specified, fragments (or templates) will be counted instead of reads.  This option is only applicable for paired-end reads.
# -a: Give the name of an annotation file
# - t: Specify the feature type.  Only rows which have the matched feature type in the provided GTF annotation file will be included for read counting.  ‘exon’ by default.
# -o: Give the name of the output file
# -g: Specify the attribute type used to group features (eg.  exons) into meta-features (eg.  genes)

# set directory name
bindir=/data/cephfs/punim0586/kbobowik/bin
genomeDir=/data/cephfs/punim0586/kbobowik/genomes
outputdir=/data/cephfs/punim0586/kbobowik/Sumba/FeatureCounts/PFPX_Combined_Yamagishi/Sick

# Execute featureCounts with an array script
for file in ${input_dir}/mapq0and255*.bam; do
  sample=`basename $file Aligned.sortedByCoord.out.bam`
  ID=${sample##*_}
  # -T = threads; -a = GTF file; -t = feature type; -g = attribute type used to group features; -o = output file
  ${bindir}/subread-1.5.3-Linux-x86_64/bin/featureCounts -T 12 -a ${genomeDir}/combined_PFalc3D7_PvivaxP01_GFF.gff -t exon -g gene_id -o ${outputdir}/${sample}.txt $file --verbose -B -C
done

for file in ${outputdir}/mapq0and255*.txt; do
	sample=`basename $file Aligned.sortedByCoord.out.fastq.txt`
	ID=${sample##*_}
	cat $file | awk '{ print $1 "\t" $6 "\t" $7 }' | tail -n +2 | sed -e "1s~${file}~Count~" > ${outputdir}/Filter_GeneLengthCount_${sample}.txt
done